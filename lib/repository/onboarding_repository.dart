import 'package:rupp_client_app/model/onboarding_model.dart';

class OnBoardingRepository{
  static List<OnBoardingModel> get all{
    return const [
      OnBoardingModel(image: 'assets/onboarding/first-min.png', title: 'Study smart and updated',
        subTitle: 'Check  your learning performances anywhere anytime.'),
      OnBoardingModel(image: 'assets/onboarding/second-min.png', title: 'Keep up to date',
          subTitle: 'explore all learning materials within this app')
    ];
  }
}