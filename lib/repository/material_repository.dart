import 'package:rupp_client_app/model/online_learning_model.dart';

abstract class MaterialRepository{
  static List<OnlineLearningMaterial> all(){
    return const [
      OnlineLearningMaterial(title: 'Video'),
      OnlineLearningMaterial(title: 'PDF'),
      OnlineLearningMaterial(title: 'Word'),
      OnlineLearningMaterial(title: 'Podcast'),
      OnlineLearningMaterial(title: 'Link'),
      OnlineLearningMaterial(title: 'Image'),
      OnlineLearningMaterial(title: 'Quiz')
    ];
  }
}