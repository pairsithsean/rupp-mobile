import 'package:rupp_client_app/model/menu_item.dart';
import 'package:rupp_client_app/widgets/menu_item_widget.dart';
import 'package:flutter/material.dart';

class MenuItemRepository{
  static List<MenuItemWidget> get all{
    return [
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('timetable');
        },
        item: MenuItemModel(
          label: 'Timetable',
          icon: Image.asset('assets/png/timetable.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('bookmark');
        },
        item: MenuItemModel(
          label: 'Bookmarks',
          icon: Image.asset('assets/png/bookmark.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('learning-result');
        },
        item: MenuItemModel(
          label: 'Learning Results',
          icon: Image.asset('assets/png/results-min.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('student-performance');
        },
        item: MenuItemModel(
          label: 'Student Performance',
          icon: Image.asset('assets/png/performance-min.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('online-learning');
        },
        item: MenuItemModel(
          label: 'Online Learning',
          icon: Image.asset('assets/png/lms-min.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('attendance-history');
        },
        item: MenuItemModel(
          label: 'Request Absence',
          icon: Image.asset('assets/png/absent-min.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('payment-history');
        },
        item: MenuItemModel(
          label: 'Payment History',
          icon: Image.asset('assets/png/payment-min.png', width: 24.0, height: 24.0)
        )
      ),
      ColumnMenuItemWidget(
        onPressed: (ctx){
          Navigator.of(ctx).pushNamed('settings');
        },
        item: MenuItemModel(
          label: 'Settings',
          icon: Image.asset('assets/png/settings-min.png', width: 24.0, height: 24.0)
        )
      )
    ];
  }
}