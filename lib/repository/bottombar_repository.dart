import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/custom_icon.dart';

abstract class BottomBarItemRepository{
  static List<BottomNavigationBarItem> get all{
    return const [
      BottomNavigationBarItem(
        icon: Icon(CustomIcon.home),
        label: 'Home'
      ),
      BottomNavigationBarItem(
        icon: Icon(CustomIcon.result),
        label: 'Result'
      ),
      BottomNavigationBarItem(
        icon: Icon(CustomIcon.bell),
        label: 'Notifications'
      ),
      BottomNavigationBarItem(
        icon: Icon(CustomIcon.more),
        label: 'More'
      )
    ];
  }
}