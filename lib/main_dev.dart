import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
  ]);
  bool isOnBoardingAlreadyViewed = false;
  final prefs = await SharedPreferences.getInstance();
  if(prefs.containsKey('onBoarding_done')){
    final bool? result = prefs.getBool('onBoarding_done');
    if(result!){
      isOnBoardingAlreadyViewed = result;
    }
  }
  runApp(MyApp(title: 'RUPP Client Dev', isOnBoardingAlreadyViewed: isOnBoardingAlreadyViewed));
}