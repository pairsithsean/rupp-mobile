import 'package:flutter/material.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';
import 'package:rupp_client_app/widgets/custom_dropdown.dart';

class LearningResultScreen extends StatefulWidget {
  final String? title;
  const LearningResultScreen({Key? key, this.title}) : super(key: key);

  @override
  _LearningResultScreenState createState() => _LearningResultScreenState();
}

class _LearningResultScreenState extends State<LearningResultScreen> {

  // final _data = const [
  //   LearningResultModel(title: 'Cyber Security', score: 100.0),
  //   LearningResultModel(title: 'Database', score: 100.0),
  //   LearningResultModel(title: 'Java', score: 100.0)
  // ];

  static const Color _expansionTileColor = Color(0xFF2278F8);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title!),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              sliver: SliverToBoxAdapter(
                child: CustomDropdown(
                  items: const [
                    'Computer Science - Year 1',
                    'English - Year 1'
                  ],
                  onSelected: (index){

                  }
                )
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              sliver: SliverToBoxAdapter(
                child: ContainerRounded(
                  padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                  child: Row(
                    children: const [
                      Expanded(
                        child: Text('Cumulative Grade Point Average', style: TextStyle(fontWeight: FontWeight.bold))
                      ),
                      Flexible(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Chip(
                            label: Text('2.64', style: TextStyle(color: Colors.white)),
                            backgroundColor: Color(0xFFEE3436)
                          )
                        )
                      )
                    ]
                  )
                )
              )
            ),
            // SliverPadding(
            //     padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            //     sliver: SliverToBoxAdapter(
            //         child: DropdownButton<String>(
            //           onChanged: (v){},
            //           items: ['aa', 'bb'].map((e){
            //             return DropdownMenuItem<String>(
            //               value: e,
            //               child: Text(e)
            //             );
            //           }).toList()
            //         )
            //     )
            // ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 16.0)
            ),
            // SliverPadding(
            //   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 2.0),
            //   sliver: SliverToBoxAdapter(
            //     child: _HeaderWidget()
            //   )
            // ),
            // SliverPadding(
            //   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 2.0),
            //   sliver: SliverToBoxAdapter(
            //     child: Column(
            //       children: _data.map((e){
            //         return Container(
            //           padding: const EdgeInsets.all(16.0),
            //           margin: const EdgeInsets.symmetric(vertical: 2.0),
            //           decoration: BoxDecoration(
            //             color: Colors.black.withAlpha(12)
            //           ),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Flexible(
            //                 child: Text(e.title, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(fontWeight: FontWeight.w600)),
            //               ),
            //               Flexible(
            //                 child: Text('${e.score!}', style: const TextStyle(fontWeight: FontWeight.w600))
            //               )
            //             ]
            //           )
            //         );
            //       }).toList()
            //     )
            //   )
            // ),
            // SliverPadding(
            //   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
            //   sliver: SliverToBoxAdapter(
            //     child: _FooterWidget()
            //   )
            // )
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              sliver: SliverToBoxAdapter(
                child: Column(
                  children: List.generate(2, (index) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: ContainerRounded(
                        child: ExpansionTile(
                          title: const Center(
                            child: Text('Year 4 - Semester 2', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0))
                          ),
                          textColor: _expansionTileColor,
                          collapsedTextColor: _expansionTileColor,
                          collapsedIconColor: _expansionTileColor,
                          iconColor: _expansionTileColor,
                          collapsedBackgroundColor: Colors.transparent,
                          children: [
                            ContainerRounded(
                              child: Column(
                                children: [
                                  const SizedBox(height: 16.0),
                                  const TableView(),
                                  const Divider(),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                    child: Row(
                                      children: [
                                        const Expanded(
                                          flex: 3,
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Text('Grade Point Average', style: TextStyle(color: Color(0xFF2278F8)))
                                          )
                                        ),
                                        const SizedBox(width: 16.0),
                                        Expanded(
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Chip(
                                              label: const Text('2.64', style: TextStyle(color: Color(0xFF2278F8))),
                                              backgroundColor: Colors.black.withAlpha(12)
                                            )
                                          )
                                        )
                                      ]
                                    )
                                  )
                                ]
                              )
                            )
                          ]
                        )
                      )
                    );
                  })
                )
              )
            ),
            // SliverPadding(
            //   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            //   sliver: SliverToBoxAdapter(
            //     child: Column(
            //       children: List.generate(2, (index) {
            //         return Padding(
            //           padding: const EdgeInsets.only(bottom: 16.0),
            //           child: ContainerRounded(
            //             child: Column(
            //               children: [
            //                 const Center(
            //                   child: Padding(
            //                     padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            //                     child: Text('Title', style: TextStyle(color: Color(0xFF2278F8)))
            //                   )
            //                 ),
            //                 const SizedBox(height: 16.0),
            //                 const TableView(),
            //                 const Divider(),
            //                 Padding(
            //                   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            //                   child: Row(
            //                     children: [
            //                       const Expanded(
            //                         flex: 3,
            //                         child: Align(
            //                           alignment: Alignment.centerRight,
            //                           child: Text('Grade Point Average', style: TextStyle(color: Color(0xFF2278F8)))
            //                         )
            //                       ),
            //                       const SizedBox(width: 16.0),
            //                       Expanded(
            //                         child: Align(
            //                           alignment: Alignment.centerRight,
            //                           child: Chip(
            //                             label: const Text('2.64', style: TextStyle(color: Color(0xFF2278F8))),
            //                             backgroundColor: Colors.black.withAlpha(12)
            //                           )
            //                         )
            //                       )
            //                     ]
            //                   )
            //                 )
            //               ]
            //             )
            //           )
            //         );
            //       })
            //     )
            //   )
            // )
          ]
        )
      )
    );
  }
}

class TableView extends StatelessWidget{
  const TableView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            decoration: const BoxDecoration(
              color: Colors.black12
            ),
            child: Row(
                children: const [
                  Expanded(
                      flex: 3,
                      child: Text('Subject')
                  ),
                  Expanded(
                      child: Center(
                          child: Text('Credit')
                      )
                  ),
                  Expanded(
                      child: Center(
                          child: Text('Score')
                      )
                  ),
                  Expanded(
                      child: Center(
                          child: Text('GPA')
                      )
                  )
                ]
            )
          ),
          const SizedBox(height: 16.0),
          Column(
            children: List.generate(5, (index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Expanded(
                            flex: 3,
                            child: Text('Row')
                        ),
                        Expanded(
                            child: Center(
                                child: Text('4')
                            )
                        ),
                        Expanded(
                            child: Center(
                                child: Text('80')
                            )
                        ),
                        Expanded(
                            child: Center(
                                child: Text('2.5(B)')
                            )
                        )
                      ]
                  ),
                )
              );
            })
          )
        ]
    );
  }
}
