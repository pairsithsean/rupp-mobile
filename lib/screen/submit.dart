import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../style/text_color.dart';
import '../widgets/custom_fill_button.dart';
import 'login.dart';

class SubmitScreen extends StatefulWidget {
  final String title;
  final String subTitle;
  final int statusCode;
  const SubmitScreen({Key? key, required this.title, this.subTitle = '', required this.statusCode}) : super(key: key);

  @override
  _SubmitScreenState createState() => _SubmitScreenState();
}

class _SubmitScreenState extends State<SubmitScreen> {

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        top: false,
        child: Column(
          children: [
            Expanded(
              child: Builder(
                builder: (ctx){
                  if(widget.statusCode == 200){
                    return SubmitSuccessView(
                      title: widget.title,
                      subTitle: widget.subTitle
                    ).build(context);
                  }else{
                    return const SubmitErrorView(
                      title: 'Request Failed',
                      subTitle: 'There was an error. Please try again.'
                    ).build(context);
                  }
                }
              )
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: CustomFillButton(
                label: 'Back to Login',
                onPressed: (){
                  Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(PageRouteBuilder(
                    pageBuilder: (ctx, anim1, anim2){
                      return const LoginScreen();
                    },
                    transitionsBuilder: (ctx, anim1, anim2, child){
                      return FadeTransition(
                        opacity: anim1,
                        child: child
                      );
                    }
                  ), (route) => false);
                }
              )
            )
          ]
        )
      )
    );
  }
}

abstract class SubmitView{
  final String title;
  final String subTitle;
  const SubmitView({this.title = '', this.subTitle = ''});

  static const double _iconSize = 128.0;

  Widget build(final BuildContext context);
}

class SubmitSuccessView extends SubmitView{

  const SubmitSuccessView({final String title = '',
    final String subTitle = ''}) : super(title: title, subTitle: subTitle);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset('assets/svg/ico-verified.svg', width: SubmitView._iconSize, height: SubmitView._iconSize),
        const SizedBox(height: 56.0),
        Text(title, style: const TextStyle(fontSize: 17.0, fontWeight: FontWeight.w600)),
        const SizedBox(height: 16.0),
        Text(subTitle, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
      ]
    );
  }
}

class SubmitErrorView extends SubmitView{

  const SubmitErrorView({final String title = '',
    final String subTitle = ''}) : super(title: title, subTitle: subTitle);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset('assets/svg/failed.svg', width: SubmitView._iconSize, height: SubmitView._iconSize),
        const SizedBox(height: 56.0),
        Text(title, style: const TextStyle(fontSize: 17.0, fontWeight: FontWeight.w600)),
        const SizedBox(height: 16.0),
        Text(subTitle, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
      ]
    );
  }
}
