import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_checkbox.dart';
import 'package:rupp_client_app/widgets/custom_flutterswitch.dart';

class SettingsScreen extends StatefulWidget {
  final String title;
  const SettingsScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  final List<String> _generalNotificationItems = [
    'New Updates Notifications', 'Push Notifications', 'SMS Notifications'
  ];

  final List<String> _inAppNotificationItems = [
    'In-App sounds', 'In-App Vibrate', 'In-Chat sounds', 'Importance'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title),
            const SliverPadding(
              padding: EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: Text('Notification Settings', style: TextStyle(color: TextColor.secondaryColor, fontWeight: FontWeight.bold)),
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: Text('General Notifications', style: TextStyle(color: TextColor.secondaryColor))
                      )
                    ),
                    ColumnComposite(
                      iComponent: _generalNotificationItems.map((e) {
                        return Component(
                          child: CustomCheckboxListTile(
                            title: e,
                            onChanged: (v){

                            }
                          )
                        );
                      }).toList()
                    )
                  ]
                ).build(context)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: Text('In-App Notifications', style: TextStyle(color: TextColor.secondaryColor))
                      )
                    ),
                    ColumnComposite(
                      iComponent: _inAppNotificationItems.map((e) {
                        return Component(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                            child: CustomFlutterSwitch(
                              title: e,
                              onToggle: (v){

                              }
                            )
                          )
                        );
                      }).toList()
                    )
                  ]
                ).build(context)
              )
            )
          ]
        )
      )
    );
  }
}

// class SettingItem extends StatelessWidget{
//   final Widget trailing;
//   const SettingItem({Key? key, required this.trailing}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return trailing;
//   }
// }
