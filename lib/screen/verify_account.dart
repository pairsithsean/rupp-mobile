import 'package:flutter/material.dart';
import 'package:rupp_client_app/screen/create_new_password.dart';
import 'package:rupp_client_app/utils/custom_bottomsheet.dart';
import 'package:rupp_client_app/utils/show_loading_indicator.dart';
import 'package:rupp_client_app/widgets/content_widget.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class VerifyAccountScreen extends StatefulWidget {
  const VerifyAccountScreen({Key? key}) : super(key: key);

  @override
  _VerifyAccountScreenState createState() => _VerifyAccountScreenState();
}

class _VerifyAccountScreenState extends State<VerifyAccountScreen> {

  TextEditingController? _firstDigitController;
  TextEditingController? _secondDigitController;
  TextEditingController? _thirdDigitController;
  TextEditingController? _fourthDigitController;

  FocusNode? _firstNode;
  FocusNode? _secondNode;
  FocusNode? _thirdNode;
  FocusNode? _fourthNode;

  // bool _isNotMatched = false;
  ValueNotifier<bool>? _isOTPNotMatchedNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isOTPNotMatchedNotifier = ValueNotifier<bool>(false);
    _firstDigitController = TextEditingController();
    _secondDigitController = TextEditingController();
    _thirdDigitController = TextEditingController();
    _fourthDigitController = TextEditingController();
    //
    _firstNode = FocusNode();
    _secondNode = FocusNode();
    _thirdNode = FocusNode();
    _fourthNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _firstDigitController?.dispose();
    _secondDigitController?.dispose();
    _thirdDigitController?.dispose();
    _fourthDigitController?.dispose();
    _firstNode?.dispose();
    _secondNode?.dispose();
    _thirdNode?.dispose();
    _fourthNode?.dispose();
    _isOTPNotMatchedNotifier?.dispose();
    super.dispose();
  }

  void _onFieldSubmitted(final String? text) async{
    if(_fieldIsNotEmpty){
      if(otp == '1234'){
        _isOTPNotMatchedNotifier!.value = false;
        debugPrint(otp);
        showLoading(context);
        await Future.delayed(const Duration(seconds: 1), () async{
          Navigator.of(context, rootNavigator: true).pop();
          await Future.delayed(const Duration(milliseconds: 500), (){
            Navigator.of(context, rootNavigator: true).pop();
            //
            // showCustomModalBottomSheet(
            //   context: context,
            //   content: const Padding(
            //     padding: EdgeInsets.symmetric(horizontal: 26.0),
            //     child: CreatePasswordScreen()
            //   )
            // );
            DraggableBottomSheetBuilder().show(context, const Padding(
              padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 56.0),
              child: CreatePasswordScreen()
            ));
          });
        });
      }else{
        _isOTPNotMatchedNotifier!.value = true;
      }
    }
  }

  bool get _fieldIsNotEmpty{
    String first = _firstDigitController!.text;
    String second = _secondDigitController!.text;
    String third = _thirdDigitController!.text;
    String fourth = _fourthDigitController!.text;
    bool isValid = first.isNotEmpty && second.isNotEmpty && third.isNotEmpty && fourth.isNotEmpty;
    return isValid;
  }

  String get otp{
    String first = _firstDigitController!.text;
    String second = _secondDigitController!.text;
    String third = _thirdDigitController!.text;
    String fourth = _fourthDigitController!.text;
    return '$first$second$third$fourth';
  }

  @override
  Widget build(BuildContext context) {
    return ContentWidget(
      title: 'Verify account',
      subTitle: 'Please fill verification code that have been sent to your mobile phone',
      body: ValueListenableBuilder<bool>(
        valueListenable: _isOTPNotMatchedNotifier!,
        builder: (ctx, value, child){
          return Row(
            children: [
              Expanded(
                child: OTPField(
                  controller: _firstDigitController!,
                  focusNode: _firstNode,
                  isNotMatched: value,
                  onFieldSubmitted: _onFieldSubmitted,
                  onChanged: (v){
                    if(v!.isEmpty) {
                      return;
                    } else if(_fieldIsNotEmpty) {
                      FocusScope.of(context).unfocus();
                      _onFieldSubmitted(v);
                      return;
                    }
                    _secondNode?.requestFocus();
                  }
                )
              ),
              const SizedBox(width: 36.0),
              Expanded(
                child: OTPField(
                  controller: _secondDigitController!,
                  focusNode: _secondNode,
                  isNotMatched: value,
                  onFieldSubmitted: _onFieldSubmitted,
                  onChanged: (v){
                    if(v!.isEmpty) {
                      return;
                    } else if(_fieldIsNotEmpty) {
                      FocusScope.of(context).unfocus();
                      _onFieldSubmitted(v);
                      return;
                    }
                    _thirdNode?.requestFocus();
                  }
                )
              ),
              const SizedBox(width: 36.0),
              Expanded(
                child: OTPField(
                  controller: _thirdDigitController!,
                  focusNode: _thirdNode,
                  isNotMatched: value,
                  onFieldSubmitted: _onFieldSubmitted,
                  onChanged: (v){
                    if(v!.isEmpty) {
                      return;
                    } else if(_fieldIsNotEmpty) {
                      FocusScope.of(context).unfocus();
                      _onFieldSubmitted(v);
                      return;
                    }
                    _fourthNode?.requestFocus();
                  }
                )
              ),
              const SizedBox(width: 36.0),
              Expanded(
                child: OTPField(
                  controller: _fourthDigitController!,
                  focusNode: _fourthNode,
                  isNotMatched: value,
                  onFieldSubmitted: _onFieldSubmitted,
                  onChanged: (v){
                    if(v!.isEmpty) {
                      return;
                    }
                    FocusScope.of(context).unfocus();
                    _onFieldSubmitted(v);
                  }
                )
              )
            ]
          );
        }
      ),
      bottom: Center(
        child: Column(
          children: [
            const SizedBox(height: 16.0),
            const Text('Did\'nt received a code?'),
            const SizedBox(height: 16.0),
            TextButton(
              child: const Text('Resend'),
              onPressed: (){},
            )
          ]
        )
      )
    );
  }
}
