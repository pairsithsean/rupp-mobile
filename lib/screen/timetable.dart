import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rupp_client_app/model/sticky_note_model.dart';
import 'package:rupp_client_app/model/timetable_model.dart';
import 'package:rupp_client_app/model/view/sticky_note_view.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/utils/custom_bottomsheet.dart';
import 'package:rupp_client_app/widgets/breakpoint_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/empty_widget.dart';

class TimetableScreen extends StatefulWidget {
  final String title;
  const TimetableScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _TimetableScreenState createState() => _TimetableScreenState();
}

class _TimetableScreenState extends State<TimetableScreen> {

  StickyNoteViewCreator? _creator;

  final twoDim = List<List<FactoryStickyNoteView?>>.generate(TimetableModel.times.length, (index) =>
      List<FactoryStickyNoteView?>.filled(DateTime.daysPerWeek, null), growable: true);

  // final twoDim1 = List<Map<String, List<FactoryStickyNoteView>>>.generate(TimetableModel.times.length, (index) => {});
  //final twoDim2 = List<Map<String, Map<String, FactoryStickyNoteView>>>.generate(TimetableModel.times.length, (index) => {});
  final Map<String, Map<String, FactoryStickyNoteView?>> dataMap = {};

  TimetableModel? _timetableModel;
  DraggableBottomSheetBuilder? _bottomSheetBuilder;

  DateTime? _mondayDate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _creator = const StickyNoteViewCreator();
    _bottomSheetBuilder = DraggableBottomSheetBuilder();
    _timetableModel = TimetableModel();

    //
    _timetableModel?.loadDate();

    for(int i = 0; i < TimetableModel.times.length; i++){
      for(int j = 0; j < _timetableModel!.dates.length; j++){
        if(i == 0 && i != 4){
          /// 7 AM
          twoDim[0][j] = _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(data: StickyNoteData(
            time: '7:15-9:15',
            subject: 'Java',
            lecturer: 'Lecturer Bona'
          )));
        }
        if(i == 1 && i != 4){
          /// 8 AM
          if(j == 2){
            twoDim[1][j] = _creator!.factoryMethod(StickyNoteType.presentation, const StickyNoteModel(data: StickyNoteData(
              time: '8:15-9:15',
              subject: 'Java',
              lecturer: 'Lecturer Bona'
            )));
          }
          else{
            twoDim[1][j] = _creator!.factoryMethod(StickyNoteType.exam, const StickyNoteModel(data: StickyNoteData(
              time: '8:15-9:15',
              subject: 'Java',
              lecturer: 'Lecturer Bona'
            )));
          }
        }
        if(i == 2 && i != 4){
          /// 9 AM
          // twoDim[2][j] = _creator!.factoryMethod(StickyNoteType.presentation, const StickyNoteModel(data: StickyNoteData(
          //   time: '10:15-11:15',
          //   subject: 'Java',
          //   lecturer: 'Lecturer Bona'
          // )));
        }
        if(i == 3 && i != 4){
          /// 10 AM
          // twoDim[3][j] = _creator!.factoryMethod(StickyNoteType.presentation, const StickyNoteModel(data: StickyNoteData(
          //   time: '10:15-11:15',
          //   subject: 'Java',
          //   lecturer: 'Lecturer Bona'
          // )));
        }
      }
    }
    // twoDim[3][6] = _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(data: StickyNoteData(
    //   time: '9:15-10:15',
    //   subject: 'Java 1',
    //   lecturer: 'Lecturer Bona'
    // )));

    _timetableModel!.addData({
      '7 am' : [
        _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(
          data: StickyNoteData(
          time: '7:15-9:15',
          subject: 'Database',
          lecturer: 'Lecturer Bona'
        ))),
        null,
        _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(data: StickyNoteData(
          time: '7:15-9:15',
          subject: 'Database 1',
          lecturer: 'Lecturer Bona'
        )))
      ]
    });
    _timetableModel!.addData({
      '8 am' : [
        _creator!.factoryMethod(StickyNoteType.presentation, const StickyNoteModel(data: StickyNoteData(
          time: '8:15-10:15',
          subject: 'Java',
          lecturer: 'Lecturer Bona'
        )))
      ]
    });
    _timetableModel!.addData({
      '9 am' : [
        _creator!.factoryMethod(StickyNoteType.exam, const StickyNoteModel(data: StickyNoteData(
          time: '9:15-10:15',
          subject: 'Java',
          lecturer: 'Lecturer Bona'
        )))
      ]
    });
    _timetableModel!.addData({
      '10 am' : [

      ]
    });
    _timetableModel!.addData({
      '11 am' : [

      ]
    });
    _timetableModel!.addData({
      '12 pm' : [

      ]
    });
    _timetableModel!.addData({
      '1 pm' : [

      ]
    });
    _timetableModel!.addData({
      '2 pm' : [

      ]
    });
    _timetableModel!.addData({
      '3 pm' : [

      ]
    });
    //
    // dataMap.addAll({
    //   '7 am' : {
    //     'Monday' : _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(
    //       data: StickyNoteData(
    //         time: '7:15-9:15',
    //         subject: 'Database',
    //         lecturer: 'Lecturer Bona'
    //       ))),
    //     'Tuesday' : null,
    //     'Wednesday' : _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(
    //       data: StickyNoteData(
    //         time: '7:15-9:15',
    //         subject: 'Database',
    //         lecturer: 'Lecturer Bona'
    //       )))
    //   },
    //   '8 am' : {
    //     'Monday' : _creator!.factoryMethod(StickyNoteType.classes, const StickyNoteModel(
    //       data: StickyNoteData(
    //         time: '7:15-9:15',
    //         subject: 'Database',
    //         lecturer: 'Lecturer Bona'
    //       )))
    //   }
    // });
  }

  void _showBottomSheet(final StickyNoteData data){
    final content = Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 16.0),
          Text('Subject - ${data.subject}', style: const TextStyle(fontSize: 17.0,
            fontWeight: FontWeight.bold)),
          const SizedBox(height: 16.0),
          Text(data.time, style: const TextStyle(color: TextColor.secondaryColor)),
          const SizedBox(height: 16.0),
          Text(data.lecturer, style: const TextStyle(color: TextColor.secondaryColor))
        ]
      )
    );
    _bottomSheetBuilder?.setInitialChildSize(0.2);
    _bottomSheetBuilder?.setMinChildSize(0.2);
    _bottomSheetBuilder?.show(context, content);
  }

  @override
  Widget build(BuildContext context) {
    final indexBreakPoint = TimetableModel.times.indexWhere((element) => element.contains('11 am'));
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 16.0, height: 16.0,
                                decoration: const BoxDecoration(
                                  color: Color(0xFF5B8DEF),
                                  shape: BoxShape.circle
                                )
                              ),
                              const SizedBox(width: 8.0),
                              const Text('Classes')
                            ]
                          ),
                          const SizedBox(width: 16.0),
                          Row(
                            children: [
                              Container(
                                width: 16.0, height: 16.0,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFFF7172),
                                  shape: BoxShape.circle
                                )
                              ),
                              const SizedBox(width: 8.0),
                              const Text('Exam')
                            ]
                          ),
                          const SizedBox(width: 16.0),
                          Row(
                            children: [
                              Container(
                                width: 16.0, height: 16.0,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFFDDD48),
                                  shape: BoxShape.circle
                                )
                              ),
                              const SizedBox(width: 8.0),
                              const Text('Presentation')
                            ]
                          )
                        ]
                      )
                    ]
                  )
                )
              )
            ),
            SliverToBoxAdapter(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 0,
                    child: Container(
                      width: 70,
                      color: Colors.white,
                      child: Column(
                        children: TimetableModel.times.map((e) {
                          return Padding(
                            padding: const EdgeInsets.only(top: 56.0),
                            child: SizedBox(
                              height: 100.0,
                              child: Text(e)
                            )
                          );
                        }).toList()
                      )
                    )
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Row(
                              children: _timetableModel!.dates.map((e) {
                                return SizedBox(
                                  width: 200.0,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(e.day, style: const TextStyle(fontWeight: FontWeight.bold)),
                                      Text(e.date, style: const TextStyle(color: TextColor.secondaryColor))
                                    ]
                                  )
                                );
                              }).toList()
                            )
                          ),
                          Stack(
                            children: [
                              Column(
                                children: twoDim.map((e){
                                  return Row(
                                    children: e.map((e1){
                                      return Padding(
                                        padding: const EdgeInsets.only(bottom: 0.0),
                                        child: SizedBox(
                                          width: 200.0, height: 150.0,
                                          child: e1 == null ? EmptyWidget.instance : GestureDetector(
                                            child: e1.build(context),
                                            onTap: (){
                                              _showBottomSheet(e1.model.data);
                                            }
                                          )
                                        )
                                      );
                                    }).toList()
                                  );
                                }).toList()
                              ),
                              Positioned(
                                top: 162.0 * indexBreakPoint,
                                child: SizedBox(
                                  width: (200 * 7), height: 150.0,
                                  child: BreakpointWidget.instance
                                )
                              )
                            ]
                          )
                        ]
                      )
                    )
                  )
                ]
              )
            ),

            // SliverFillRemaining(
            //   hasScrollBody: false,
            //   child: Row(
            //     children: [
            //       Column(
            //         children: _timetableModel!.getData().map((e){
            //           return Column(
            //             children: e.keys.map((e1) {
            //               // if(e1.contains('11 am')){
            //               //   return Row(
            //               //     children: [
            //               //       Container(
            //               //         width: 70, height: 150.0,
            //               //         color: Colors.white,
            //               //         child: Center(
            //               //           child: Text(e1)
            //               //         )
            //               //       ),
            //               //       Expanded(
            //               //         flex: 0,
            //               //         child: BreakpointWidget.instance
            //               //       )
            //               //     ]
            //               //   );
            //               // }
            //               return Container(
            //                 width: 70, height: 150.0,
            //                 color: Colors.white,
            //                 child: Center(
            //                   child: Text(e1)
            //                 )
            //               );
            //             }).toList()
            //           );
            //         }).toList()
            //       ),
            //       Expanded(
            //         child: SingleChildScrollView(
            //           scrollDirection: Axis.horizontal,
            //           child: Column(
            //             crossAxisAlignment: CrossAxisAlignment.start,
            //             children: [
            //               Row(
            //                 children: TimetableModel.dates.map((e) {
            //                   final index = TimetableModel.dates.indexWhere((element) => element.day == e.day);
            //                   return Padding(
            //                     padding: EdgeInsets.only(left: index == 0 ? 16.0 : 0.0),
            //                     child: SizedBox(
            //                       width: 200.0,
            //                       child: Column(
            //                         crossAxisAlignment: CrossAxisAlignment.start,
            //                         children: [
            //                           Text(e.day, style: const TextStyle(fontWeight: FontWeight.bold)),
            //                           Text(e.date, style: const TextStyle(color: TextColor.secondaryColor))
            //                         ]
            //                       )
            //                     )
            //                   );
            //                 }).toList()
            //               ),
            //               const SizedBox(height: 16.0),
            //               Column(
            //                 crossAxisAlignment: CrossAxisAlignment.start,
            //                 children: _timetableModel!.getData().map((e) {
            //                   if(e.keys.isNotEmpty){
            //                     if(e.keys.elementAt(0).contains('11 am')){
            //                       return Padding(
            //                         padding: const EdgeInsets.only(top: 16.0),
            //                         child: SizedBox(
            //                           width: (200 * 7), height: 150.0,
            //                           child: BreakpointWidget.instance
            //                         )
            //                       );
            //                     }
            //                   }
            //                   return Column(
            //                     crossAxisAlignment: CrossAxisAlignment.start,
            //                     children: e.keys.map((e1){
            //                       return SizedBox(
            //                         height: 150.0,
            //                         child: Row(
            //                           children: e.values.map((e2) {
            //                             return Row(
            //                               children: e2.map((e3) {
            //                                 return SizedBox(
            //                                   width: 200, height: 150.0,
            //                                   child: GestureDetector(
            //                                     onTap: (){
            //                                       final content = Padding(
            //                                         padding: const EdgeInsets.symmetric(horizontal: 16.0),
            //                                         child: Column(
            //                                           crossAxisAlignment: CrossAxisAlignment.stretch,
            //                                           children: [
            //                                             const SizedBox(height: 16.0),
            //                                             Text('Subject - ${e3!.model.data.subject}', style: const TextStyle(fontSize: 17.0,
            //                                                 fontWeight: FontWeight.bold)),
            //                                             const SizedBox(height: 16.0),
            //                                             Text(e3.model.data.time, style: const TextStyle(color: TextColor.secondaryColor)),
            //                                             const SizedBox(height: 16.0),
            //                                             Text(e3.model.data.lecturer, style: const TextStyle(color: TextColor.secondaryColor))
            //                                           ]
            //                                         )
            //                                       );
            //                                       DraggableBottomSheetBuilder().setInitialChildSize(0.2).setMinChildSize(0.2)
            //                                           .show(context, content);
            //                                     },
            //                                     child: e3 == null ? EmptyWidget.instance : e3.build(context)
            //                                   )
            //                                 );
            //                               }).toList()
            //                             );
            //                           }).toList()
            //                         )
            //                       );
            //                     }).toList()
            //                   );
            //                 }).toList()
            //               )
            //             ]
            //           )
            //         )
            //       )
            //     ]
            //   )
            // )

            // SliverFillRemaining(
            //   hasScrollBody: false,
            //   child: Stack(
            //     children: [
            //       Positioned(
            //         child: Column(
            //           crossAxisAlignment: CrossAxisAlignment.start,
            //           children: twoDim1.map((e){
            //             return Column(
            //               crossAxisAlignment: CrossAxisAlignment.start,
            //               children: e.keys.map((e1){
            //                 if(e1.contains('11 am')){
            //                   return Row(
            //                     children: [
            //                       Container(
            //                         width: 70, height: 150.0,
            //                         color: Colors.white,
            //                         child: Center(
            //                           child: Text(e1)
            //                         )
            //                       ),
            //                       Expanded(
            //                         child: BreakpointWidget.instance
            //                       )
            //                     ]
            //                   );
            //                 }
            //                 return Container(
            //                   width: 70, height: 150.0,
            //                   color: Colors.white,
            //                   child: Center(
            //                     child: Text(e1)
            //                   )
            //                 );
            //               }).toList()
            //             );
            //           }).toList()
            //         )
            //       ),
            //       Positioned(
            //         left: 70.0, top: 0, bottom: 0, right: 0,
            //         child: SingleChildScrollView(
            //           scrollDirection: Axis.horizontal,
            //           child: Stack(
            //               children: [
            //                 Positioned(
            //                   child: Row(
            //                       children: TimetableModel.dates.map((e) {
            //                         return SizedBox(
            //                             width: 200.0,
            //                             child: Column(
            //                                 crossAxisAlignment: CrossAxisAlignment.start,
            //                                 children: [
            //                                   Text(e.day, style: const TextStyle(fontWeight: FontWeight.bold)),
            //                                   Text(e.date, style: const TextStyle(color: TextColor.secondaryColor))
            //                                 ]
            //                             )
            //                         );
            //                       }).toList()
            //                   )
            //                 ),
            //                 Positioned(
            //                   child: Column(
            //                       crossAxisAlignment: CrossAxisAlignment.start,
            //                       children: twoDim1.map((e) {
            //                         return Column(
            //                             crossAxisAlignment: CrossAxisAlignment.start,
            //                             children: e.keys.map((e1){
            //                               return SizedBox(
            //                                 height: 150.0,
            //                                 child: Row(
            //                                     children: e.values.map((e2) {
            //                                       return Row(
            //                                           children: e2.map((e3) {
            //                                             return SizedBox(
            //                                                 width: 200, height: 150.0,
            //                                                 child: e3.build(context)
            //                                             );
            //                                           }).toList()
            //                                       );
            //                                     }).toList()
            //                                 ),
            //                               );
            //                             }).toList()
            //                         );
            //                       }).toList()
            //                   ),
            //                 )
            //               ]
            //           )
            //         )
            //       )
            //     ]
            //   )
            // )
          ]
        )
      )
    );
  }
}
