import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/menu_item.dart';
import 'package:rupp_client_app/repository/menu_item_repository.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/menu_item_widget.dart';
import 'package:rupp_client_app/widgets/profile_widget.dart';

class MoreScreen extends StatefulWidget {
  const MoreScreen({Key? key}) : super(key: key);

  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color.fromRGBO(242, 242, 245, 1.0),
        child: CustomScrollView(
          slivers: [
            const CustomSliverAppbar(title: 'Menu'),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 36.0),
              sliver: SliverToBoxAdapter(
                child: ProfileWidget(
                  name: 'Name',
                  subTitle: Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      child: const Text('See your profile', style: TextStyle(color: CustomMaterialColor.primary)),
                      onTap: (){
                        Navigator.of(context).pushNamed('profile');
                      }
                    )
                  )
                )
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 16.0, bottom: 16.0, right: 16.0),
              sliver: SliverGrid(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, crossAxisSpacing: 26.0, mainAxisSpacing: 16.0,
                  mainAxisExtent: 80
                ),
                delegate: SliverChildBuilderDelegate((ctx, index){
                  return MenuItemRepository.all.elementAt(index).build(context);
                }, childCount: MenuItemRepository.all.length)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 16.0, bottom: 16.0, right: 16.0),
              sliver: SliverToBoxAdapter(
                child: RowMenuItemWidget(
                  item: MenuItemModel(
                    label: 'Rate Us',
                    icon: Image.asset('assets/png/rate-min.png', width: 24.0, height: 24.0)
                  ),
                  onPressed: (ctx){}
                ).build(context)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 16.0, bottom: 16.0, right: 16.0),
              sliver: SliverToBoxAdapter(
                child: RowMenuItemWidget(
                  item: MenuItemModel(
                    label: 'Logout',
                    icon: Image.asset('assets/png/logout-min.png', width: 24.0, height: 24.0)
                  ),
                  onPressed: (ctx){}
                ).build(context)
              )
            )
          ]
        )
      )
    );
  }
}
