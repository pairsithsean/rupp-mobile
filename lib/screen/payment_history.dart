import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/payment_history_model.dart';
import 'package:rupp_client_app/model/view/payment_history_view.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

class PaymentHistoryScreen extends StatefulWidget {
  final String? title;
  const PaymentHistoryScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _PaymentHistoryScreenState createState() => _PaymentHistoryScreenState();
}

class _PaymentHistoryScreenState extends State<PaymentHistoryScreen> {

  final _initData = const [
    PaymentHistoryModel(
      date: '9/11/2021',
      date1: '1 jan 2022',
      majorName: 'Computer Science',
      semesterTitle: 'Semester 1',
      isPaid: false,
      pay: 1000.0
    ),
    PaymentHistoryModel(
      date: '9/11/2021',
      date1: '1 jan 2022',
      majorName: 'Computer Science',
      semesterTitle: 'Semester 1',
      isPaid: true,
      pay: 500.0
    )
  ];

  Future<List<PaymentHistoryModel>> _loadData() async{
    await Future.delayed(const Duration(seconds: 1));
    return _initData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title!),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverFillRemaining(
                hasScrollBody: false,
                child: FutureBuilder<List<PaymentHistoryModel>>(
                  future: _loadData(), initialData: null,
                  builder: (ctx, snapshot){
                    if(snapshot.hasData){
                      return Column(
                        children: _initData.map((e){
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 16.0),
                            child: PaymentHistoryView(data: e)
                          );
                        }).toList()
                      );
                    }else{
                      return LoadingWidget.instance;
                    }
                  }
                )
              )
            )
          ]
        )
      )
    );
  }
}
