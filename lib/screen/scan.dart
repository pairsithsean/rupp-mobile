import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/view/scan_view.dart';
import 'package:rupp_client_app/style/matrial_color.dart';

class ScanScreen extends StatefulWidget {
  const ScanScreen({Key? key}) : super(key: key);

  @override
  _ScanScreenState createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SizedBox(
          width: 24.0, height: 24.0,
          child: Hero(
            tag: 'LOGO',
            child: Image.asset('assets/png/LOGO-min.png', fit: BoxFit.contain)
          )
        ), elevation: 0.0,
        iconTheme: const IconThemeData(color: CustomMaterialColor.primary),
      ),
      body: SizedBox(
        width: double.infinity, height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('Scan QR', style: TextStyle(fontSize: 28.0,
                color: CustomMaterialColor.primary, fontWeight: FontWeight.bold)),
            const Text('Align frame with QR code', style: TextStyle(fontSize: 10.0)),
            const SizedBox(height: 16.0),
            ScanView(
              onDetect: (barcode){
                debugPrint(barcode.code);
              }
            )
          ]
        )
      )
    );
  }
}
