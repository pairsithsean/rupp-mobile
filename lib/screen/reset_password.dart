import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rupp_client_app/widgets/content_widget.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';
import 'package:rupp_client_app/widgets/custom_form.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({Key? key}) : super(key: key);

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {

  TextEditingController? _phoneController;
  FocusNode? _focusNode;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _phoneController = TextEditingController();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _phoneController?.dispose();
    _focusNode?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ContentWidget(
      title: 'Reset password',
      subTitle: 'Enter your phone number for the verification process, we will send 4 digits code to your phone number.',
      body: FormItem(
        label: 'Phone number',
        child: PhoneNumberField(
          controller: _phoneController!,
          hintText: '012-345-678'
        )
      ),
      bottom: CustomFillButton(
        label: 'Confirm',
        onPressed: () async{
          if(FocusScope.of(context).hasFocus){
            FocusScope.of(context).unfocus();
          }
          // showLoading(context);
          // await Future.delayed(const Duration(seconds: 1), () async{
          //   Navigator.of(context, rootNavigator: true).pop();
          //   await Future.delayed(const Duration(milliseconds: 500), (){
          //     Navigator.of(context, rootNavigator: true).pop();
          //     //
          //     // showCustomModalBottomSheet(
          //     //   context: context,
          //     //   content: const Padding(
          //     //     padding: EdgeInsets.symmetric(horizontal: 26.0),
          //     //     child: VerifyAccountScreen()
          //     //   )
          //     // );
          //     DraggableBottomSheetBuilder().show(context, const Padding(
          //       padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 56.0),
          //       child: VerifyAccountScreen()
          //     ));
          //   });
          // });
        }
      )
    );
  }
}
