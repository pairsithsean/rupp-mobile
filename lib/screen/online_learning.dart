import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rupp_client_app/model/online_learning_model.dart';
import 'package:rupp_client_app/model/view/filter_view.dart';
import 'package:rupp_client_app/model/view/online_learning_view.dart';
import 'package:rupp_client_app/repository/material_repository.dart';
import 'package:rupp_client_app/screen/online_learning_detail.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/utils/custom_bottomsheet.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';
import 'package:rupp_client_app/widgets/filter_result_widget.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

class OnlineLearningScreen extends StatefulWidget {
  final String? title;
  const OnlineLearningScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _OnlineLearningScreenState createState() => _OnlineLearningScreenState();
}

class _OnlineLearningScreenState extends State<OnlineLearningScreen> {

  TextEditingController? _searchController;

  final _initData = [
    OnlineLearningModel(
      title: 'Upcoming Course',
      data: [
        OnlineLearningData(title: 'Steam', date: 'Dec 30, 2021', time: '9:00 AM - 12:00 PM', material: MaterialRepository.all().first),
        OnlineLearningData(title: 'YouTube lesson', date: 'cc', material: MaterialRepository.all().elementAt(3)),
        OnlineLearningData(title: 'Physical Lesson', date: 'dd', material: MaterialRepository.all().elementAt(1))
      ]
    ),
    OnlineLearningModel(
      title: 'Recent',
      data: [
        OnlineLearningData(title: 'Test Live', date: 'bb', material: MaterialRepository.all().first),
        OnlineLearningData(title: 'YouTube lesson 1', date: 'cc', material: MaterialRepository.all().elementAt(2)),
        OnlineLearningData(title: 'Physical Lesson 2', date: 'dd', material: MaterialRepository.all().elementAt(4))
      ]
    )
  ];

  List<OnlineLearningData> _queryData = [];
  StreamController<List<OnlineLearningModel>?>? _searchDataController;

  final _noDataWidget = SvgPicture.asset('assets/svg/no-data.svg');

  ValueNotifier<bool>? _isFilteredNotifier;

  DraggableBottomSheetBuilder? _sheetBuilder;

  CustomFillButton? _applyButton ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isFilteredNotifier = ValueNotifier<bool>(false);
    _searchController = TextEditingController();
    _searchDataController = StreamController<List<OnlineLearningModel>?>.broadcast();
    _sheetBuilder = DraggableBottomSheetBuilder();

    _applyButton = CustomFillButton(label: 'Apply', onPressed: _applyPressed);
  }

  Future<List<OnlineLearningModel>> _loadData() async{
    await Future.delayed(const Duration(seconds: 1));
    return _initData;
  }

  void _applyPressed(){
    Navigator.of(context, rootNavigator: true).pop();
    _isFilteredNotifier!.value = false;
    _queryData.clear();
    if(selectedFilterItem.isNotEmpty){
      final queryItem = selectedFilterItem.where((element) => element.isSelected == true);
      if(queryItem.isNotEmpty){
        _isFilteredNotifier!.value = true;
        _searchController?.clear();
      }
    }else{
      setState(() {
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _searchController?.dispose();
    _searchDataController?.close();
    _isFilteredNotifier?.dispose();
    selectedFilterItem.clear();
    super.dispose();
  }

  List<OnlineLearningModel> _search({required final String inputText, required final List<OnlineLearningModel> list}){
    final List<OnlineLearningModel> searchListData = [];
    for(OnlineLearningModel element in list){
      final result = element.data.where((element) => element.title.toLowerCase().contains(inputText.toLowerCase()));
      if(result.isNotEmpty){
        searchListData.add(element);
        for(OnlineLearningData data in result){
          _queryData.add(data);
        }
      }else{
        final result2 = element.data.where((element) => element.material.title.toLowerCase().contains(inputText.toLowerCase()));
        if(result2.isNotEmpty){
          searchListData.add(element);
          for(OnlineLearningData data in result2){
            _queryData.add(data);
          }
        }else{
          if(_isFilteredNotifier!.value){
            searchListData.add(element);
          }
        }
      }
    }
    return searchListData;
  }

  void _showFilter(final BuildContext context, final List<OnlineLearningModel> data){
    _sheetBuilder?.setInitialChildSize(0.95);
    _sheetBuilder?.show(context, FilterView(
      data: data
    ),
    bottom: _applyButton);
  }

  void _navigateToDetail(final OnlineLearningData data){
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (ctx) => OnlineLearningDetail(data: data)
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: CustomMaterialColor.backgroundColor,
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title!),
            // SliverPadding(
            //   padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            //   sliver: SliverToBoxAdapter(
            //     child: SearchField(
            //       controller: _searchController,
            //       hintText: 'Search',
            //       onChanged: (v){
            //         if(v!.isEmpty){
            //           _dataController!.sink.add(_initData);
            //           return;
            //         }
            //         for(OnlineLearningModel element in _initData){
            //           final searchData = element.data.where((element) => element.title.toLowerCase().contains(v.toLowerCase()));
            //           for(OnlineLearningData elementData in element.data){
            //             if(searchData.contains(elementData)){
            //               // _dataController!.sink.add(searchData.toList());
            //               if(element.title == 'Upcoming Course'){
            //                 _dataController!.sink.add(_initData);
            //               }else{
            //
            //               }
            //             }
            //           }
            //         }
            //         // final searchData = _initData.first.data.where((element) => element.title.toLowerCase().contains(v.toLowerCase()));
            //         // _dataController!.sink.add(searchData.toList());
            //         // debugPrint('${searchData.length}');
            //       },
            //       onSuffixIconTap: (){
            //         final content = Column(
            //           children: [
            //             Row(
            //               children: [
            //                 TextButton(
            //                   child: const Text('Cancel'),
            //                   onPressed: () => Navigator.of(context, rootNavigator: true).pop()
            //                 ),
            //                 const Expanded(
            //                   child: Center(
            //                     child: Text('Filter')
            //                   )
            //                 ),
            //                 TextButton(
            //                   child: const Text('Reset'),
            //                   onPressed: (){}
            //                 )
            //               ]
            //             )
            //           ]
            //         );
            //         DraggableBottomSheetBuilder().setInitialChildSize(0.95).show(context, content);
            //       }
            //     )
            //   )
            // ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 0.0),
              sliver: SliverFillRemaining(
                hasScrollBody: false,
                child: FutureBuilder<List<OnlineLearningModel>>(
                  future: _loadData(), initialData: null,
                  builder: (ctx, snapshotModel){
                    if(snapshotModel.hasData){
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                            child: ValueListenableBuilder<bool>(
                              valueListenable: _isFilteredNotifier!,
                              child: SearchField(
                                controller: _searchController,
                                hintText: 'Search',
                                onChanged: (v){
                                  final result = _search(inputText: v!, list: snapshotModel.data!);
                                  _searchDataController!.sink.add(result);
                                },
                                onSuffixIconTap: (){
                                  _showFilter(context, snapshotModel.data!);
                                }
                              ),
                              builder: (ctx, value, child){
                                if(value){
                                  // return Row(
                                  //   children: [
                                  //     Expanded(
                                  //       child: Wrap(
                                  //         children: selectedItemData.map((e) {
                                  //           final result = _search(inputText: e.data.title, list: snapshotModel.data!);
                                  //           _searchDataController!.sink.add(result);
                                  //           return Padding(
                                  //             padding: const EdgeInsets.only(right: 8.0),
                                  //             child: Chip(
                                  //               side: const BorderSide(color: Color(0xFF6B7588)),
                                  //               backgroundColor: const Color(0xFFEBEBF0),
                                  //               deleteIcon: const Icon(CupertinoIcons.clear_circled, size: 18.0,
                                  //                   color: Color(0xFF6B7588), semanticLabel: 'clear_circled'),
                                  //               onDeleted: (){
                                  //                 setState(() {
                                  //                   selectedItemData.remove(e);
                                  //                 });
                                  //                 if(selectedItemData.isEmpty){
                                  //                   _isFilteredNotifier!.value = false;
                                  //                 }
                                  //                 final result = _search(inputText: e.data.title, list: snapshotModel.data!);
                                  //                 _searchDataController!.sink.add(result);
                                  //               },
                                  //               label: Text(e.data.title, style: const TextStyle(color: TextColor.secondaryColor))
                                  //             )
                                  //           );
                                  //         }).toList()
                                  //       )
                                  //     ),
                                  //     GestureDetector(
                                  //       child: SvgPicture.asset('assets/svg/filter.svg', width: 18.0, height: 18.0),
                                  //       onTap: (){
                                  //       }
                                  //     )
                                  //   ]
                                  // );
                                  return FilterResultWidget(
                                    onTrailingTap: (){
                                      _showFilter(context, snapshotModel.data!);
                                    },
                                    onResultData: (data){
                                      final result = _search(inputText: data.label, list: snapshotModel.data!);
                                      if(result.isNotEmpty){
                                        //_filterData.add(data);
                                        _searchDataController!.sink.add(result);
                                      }
                                    },
                                    onDeleted: (data){
                                      //_filterData.clear();
                                      _queryData.clear();
                                      setState(() {
                                        selectedFilterItem.remove(data);
                                      });
                                      if(selectedFilterItem.isEmpty){
                                        _searchController?.clear();
                                        _isFilteredNotifier!.value = false;
                                      }
                                    }
                                  );
                                }else{
                                  return child!;
                                }
                              }
                            )
                          ),
                          Expanded(
                            child: StreamBuilder<List<OnlineLearningModel>?>(
                              stream: _searchDataController!.stream, initialData: const [],
                              builder: (ctx, snapshotSearch){
                                if(snapshotSearch.hasData){
                                  if(snapshotSearch.data!.isEmpty && _searchController!.text.isNotEmpty){
                                    return Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Flexible(
                                          flex: 0,
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                                            child: Text('No Result Found', style: TextStyle(fontSize: 17.0, color: TextColor.secondaryColor, fontWeight: FontWeight.w600))
                                          )
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Center(
                                            child: _noDataWidget
                                          )
                                        )
                                      ]
                                    );
                                  }
                                  if(snapshotSearch.data!.isNotEmpty && _searchController!.text.isNotEmpty || _isFilteredNotifier!.value){
                                    final isFiltered = _isFilteredNotifier!.value;
                                    if(isFiltered){
                                      Set<OnlineLearningData> filterQueryData = _queryData.toSet();
                                      if(filterQueryData.isEmpty){
                                        return _noDataWidget;
                                      }
                                      /// DISPLAY FILTER VIEW
                                      return ColumnComposite(
                                          iComponent: [
                                            Component(
                                              child: Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                                child: Align(
                                                  child: Text('Result(${filterQueryData.length})',
                                                    style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                                                  alignment: Alignment.centerLeft
                                                )
                                              )
                                            ),
                                            ColumnComposite(
                                              iComponent: filterQueryData.map((data){
                                                return Component(
                                                  child: OnlineLearningView(
                                                    data: data,
                                                    viewType: OnlineLearningViewType.column,
                                                    onTap: (){
                                                      _navigateToDetail(data);
                                                    }
                                                  )
                                                );
                                              }).toList()
                                            )
                                          ]
                                      ).build(context);
                                    }

                                    /// DISPLAY BY SEARCH VIEW
                                    return ColumnComposite(
                                      iComponent: snapshotSearch.data!.map((e) {
                                        _queryData = e.data.where((element) => element.title.toLowerCase().contains(_searchController!.text.toLowerCase())).toList();
                                        return ColumnComposite(
                                          iComponent: [
                                            Component(
                                              child: Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                                child: Align(
                                                  child: Text(e.title, style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                                                  alignment: Alignment.centerLeft
                                                )
                                              )
                                            ),
                                            e.title.contains('Upcoming') && !isFiltered ? RowComposite(
                                              iComponent: _queryData.map((searchData){
                                                return Component(
                                                  child: OnlineLearningView(
                                                    data: searchData,
                                                    viewType: OnlineLearningViewType.row,
                                                    onTap: (){
                                                      _navigateToDetail(searchData);
                                                    }
                                                  )
                                                );
                                              }).toList()
                                            ) : ColumnComposite(
                                              iComponent: _queryData.map((searchData){
                                                return Component(
                                                  child: OnlineLearningView(
                                                    data: searchData,
                                                    viewType: OnlineLearningViewType.column,
                                                    onTap: (){
                                                      _navigateToDetail(searchData);
                                                    }
                                                  )
                                                );
                                              }).toList()
                                            )
                                          ]
                                        );
                                      }).toList()
                                    ).build(context);
                                  }
                                }
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: snapshotModel.data!.map((e) {
                                    final index = snapshotModel.data!.indexWhere((element) => element.title == e.title);
                                    if(index == 0){
                                      return ColumnComposite(
                                        iComponent: [
                                          Component(
                                            child: Padding(
                                              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                              child: Text(e.title, style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
                                            )
                                          ),
                                          RowComposite(
                                            iComponent: e.data.map((e1){
                                              return Component(
                                                child: OnlineLearningView(
                                                  data: e1,
                                                  viewType: OnlineLearningViewType.row,
                                                  onTap: (){
                                                    _navigateToDetail(e1);
                                                  }
                                                )
                                              );
                                            }).toList()
                                          )
                                        ]
                                      ).build(context);
                                    }
                                    return ColumnComposite(
                                      iComponent: [
                                        Component(
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                            child: Text(e.title, style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
                                          )
                                        ),
                                         ColumnComposite(
                                          iComponent: e.data.map((e1){
                                            return Component(
                                              child: OnlineLearningView(
                                                data: e1,
                                                viewType: OnlineLearningViewType.column,
                                                onTap: (){
                                                  _navigateToDetail(e1);
                                                }
                                              )
                                            );
                                          }).toList()
                                        )
                                      ]
                                    ).build(context);
                                  }).toList()
                                );
                              }
                            )
                          )
                        ]
                      );
                    }else{
                      return LoadingWidget.instance;
                    }
                  }
                )
              )
            )
          ]
        )
      )
    );
  }
}
