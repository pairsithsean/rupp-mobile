import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';
import 'package:rupp_client_app/widgets/custom_dropdown.dart';
import 'package:rupp_client_app/widgets/draggable_container.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: DraggableContainer(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomDropdown(
                    position: 330.0,
                    items: const [
                      'Computer Science',
                      'English'
                    ],
                    onSelected: (i){
                    }
                  ),
                  const SizedBox(height: 16.0),
                  ColumnComposite(
                    iComponent: [
                      Component(
                        child: const Padding(
                          padding: EdgeInsets.only(bottom: 16.0),
                          child: Text('Timetable', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
                        )
                      ),
                      ColumnComposite(
                        iComponent: List.generate(5, (index) {
                          return Component(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 16.0),
                              child: ContainerRounded(
                                width: double.infinity, height: 75.0,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: const [
                                            Flexible(
                                              child: Text('7:30 am - 8:25', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                                            ),
                                            SizedBox(height: 4.0),
                                            Flexible(
                                              child: Text('Information Science', style: TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
                                            )
                                          ]
                                        )
                                      )
                                    ),
                                    Flexible(
                                      flex: 0,
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Container(
                                          width: 75.0,
                                          padding: const EdgeInsets.all(16.0),
                                          decoration: const BoxDecoration(
                                            color: Color(0xFFFFCC00)
                                          ),
                                          child: Column(
                                            children: const [
                                              Flexible(
                                                child: Text('Room', style: TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                                              ),
                                              SizedBox(height: 4.0),
                                              Flexible(
                                                child: Text('32 F', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
                                              )
                                            ]
                                          )
                                        )
                                      )
                                    )
                                  ]
                                )
                              )
                            )
                          );
                        }).toList()
                      )
                    ]
                  ).build(context)
                ]
              )
            )
          )
        )
      )
    );
  }
}
