import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/notification_model.dart';
import 'package:rupp_client_app/model/view/notification_item_view.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';

class NotificationScreen extends StatefulWidget {
  final String? title;
  const NotificationScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {

  final _data = const [
    NotificationModel(
      title: 'Latest',
      data: [
        NotificationData(
          title: 'Payment',
          subTitle: 'You have successfully made payment for new semester.',
          time: '11:40'
        ),
        NotificationData(
          title: 'New Course',
          subTitle: 'New course will begin at Jan 12, 2022.',
          time: '11:40'
        )
      ]
    ),
    NotificationModel(
      title: 'Older',
      data: [
        NotificationData(
          title: 'Exam Date',
          subTitle: 'Your final exam will be on Dec 14, 2021.',
          time: '16:00'
        )
      ]
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title!),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 0.0),
              sliver: SliverToBoxAdapter(
                // child: Column(
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: _data.map((e){
                //     return Column(
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //         Padding(
                //           padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                //           child: Text(e.title, style: const TextStyle(color: TextColor.secondaryColor))
                //         ),
                //         Column(
                //           children: e.data.map((e1){
                //             return Padding(
                //               padding: const EdgeInsets.only(bottom: 16.0),
                //               child: NotificationItemView(
                //                 title: e.title,
                //                 data: NotificationData(
                //                   title: e1.title,
                //                   subTitle: e1.subTitle,
                //                   time: e1.time
                //                 )
                //               )
                //             );
                //           }).toList()
                //         )
                //       ]
                //     );
                //   }).toList()
                // )
                child: ColumnComposite(
                  iComponent: _data.map((e){
                    return ColumnComposite(
                      iComponent: [
                        Component(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text(e.title, style: const TextStyle(color: TextColor.secondaryColor))
                          )
                        ),
                        ColumnComposite(
                          iComponent: e.data.map((e1){
                            return Component(
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 16.0),
                                child: NotificationItemView(
                                  title: e.title,
                                  data: NotificationData(
                                    title: e1.title,
                                    subTitle: e1.subTitle,
                                    time: e1.time
                                  )
                                )
                              )
                            );
                          }).toList()
                        )
                      ]
                    );
                  }).toList()
                ).build(context)
              )
            )
          ]
        )
      )
    );
  }
}