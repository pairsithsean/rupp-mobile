import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/view/credit_card_view.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_dropdown.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';
import 'package:rupp_client_app/widgets/custom_form.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class PaymentScreen extends StatefulWidget {
  final String? title;
  const PaymentScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {


  TextEditingController? _studentNameController;
  TextEditingController? _yearController;
  TextEditingController? _phoneController;

  TextEditingController? _cardHolderController;
  TextEditingController? _cardNumberController;
  TextEditingController? _expDateController;
  TextEditingController? _ccvController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _studentNameController = TextEditingController();
    _yearController = TextEditingController();
    _phoneController = TextEditingController();

    _cardHolderController = TextEditingController();
    _cardNumberController = TextEditingController();
    _expDateController = TextEditingController();
    _ccvController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _studentNameController?.dispose();
    _yearController?.dispose();
    _phoneController?.dispose();
    _cardNumberController?.dispose();
    _cardHolderController?.dispose();
    _ccvController?.dispose();
    _expDateController?.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(FocusScope.of(context).hasFocus){
          FocusScope.of(context).unfocus();
        }
      },
      child: Scaffold(
        body: SizedBox(
          child: Column(
            children: [
              Expanded(
                child: CustomScrollView(
                  slivers: [
                    CustomSliverAppbar(title: widget.title!),
                    SliverPadding(
                      padding: const EdgeInsets.all(16.0),
                      sliver: SliverToBoxAdapter(
                        child: FormItem(
                          label: 'Student name',
                          child: CustomTextField(
                            controller: _studentNameController
                          )
                        )
                      )
                    ),
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      sliver: SliverToBoxAdapter(
                        child: FormItem(
                          label: 'Major',
                          child: CustomDropdown(
                            items: const [
                              'Computer Science - Year 1',
                              'English - Year 1'
                            ],
                            onSelected: (index){

                            }
                          )
                        )
                      )
                    ),
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                      sliver: SliverToBoxAdapter(
                        child: FormItem(
                          label: 'Semester',
                          child: CustomDropdown(
                            items: const [
                              'Semester 1',
                              'Semester 2'
                            ],
                            onSelected: (index){

                            }
                          )
                        )
                      )
                    ),
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      sliver: SliverToBoxAdapter(
                        child: FormItem(
                          label: 'Year',
                          child: CustomTextField(
                            controller: _yearController,
                            textInputType: TextInputType.number
                          )
                        )
                      )
                    ),
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                      sliver: SliverToBoxAdapter(
                        child: FormItem(
                          label: 'Phone number',
                          child: PhoneNumberField(
                            controller: _phoneController
                          )
                        )
                      )
                    ),
                    const SliverPadding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      sliver: SliverToBoxAdapter(
                        child: Divider()
                      )
                    ),
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                      sliver: SliverToBoxAdapter(
                        child: CreditCardView(
                          cardHolderController: _cardHolderController!,
                          cardNumberController: _cardNumberController!,
                          ccvController: _ccvController!,
                          expDateController: _expDateController!
                        )
                      )
                    )
                  ]
                )
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SizedBox(
                  child: CustomFillButton(
                    label: 'Checkout',
                    onPressed: (){

                    }
                  )
                )
              )
            ]
          )
        )
      )
    );
  }
}
