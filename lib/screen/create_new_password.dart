import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/state_model.dart';
import 'package:rupp_client_app/screen/submit.dart';
import 'package:rupp_client_app/utils/show_loading_indicator.dart';
import 'package:rupp_client_app/widgets/content_widget.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';
import 'package:rupp_client_app/widgets/custom_form.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class CreatePasswordScreen extends StatefulWidget {
  const CreatePasswordScreen({Key? key}) : super(key: key);

  @override
  _CreatePasswordScreenState createState() => _CreatePasswordScreenState();
}

class _CreatePasswordScreenState extends State<CreatePasswordScreen> {

  static const int _requiredPasswordLength = 7;

  TextEditingController? _newPassController;
  TextEditingController? _confirmPassController;

  ValueNotifier<StateContext>? _passwordContextNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _newPassController = TextEditingController();
    _confirmPassController = TextEditingController();
    _passwordContextNotifier = ValueNotifier<StateContext>(const StateContext());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _newPassController?.dispose();
    _confirmPassController?.dispose();
    _passwordContextNotifier?.dispose();
    super.dispose();
  }

  void _onChanged(value){
    if(_newPassController!.text.length < _requiredPasswordLength || _confirmPassController!.text.length < _requiredPasswordLength){
      _passwordContextNotifier!.value = StateContext(state: ErrorState());
    }else{
      if(_newPassController!.text.length >= _requiredPasswordLength
          && _confirmPassController!.text.length >= _requiredPasswordLength){
        if(_newPassController!.text == _confirmPassController!.text){
          _passwordContextNotifier!.value = StateContext(state: SuccessState());
        }
        else{
          _passwordContextNotifier!.value = const StateContext();
        }
      }else{
        _passwordContextNotifier!.value = const StateContext();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ContentWidget(
      title: 'Create new password',
      subTitle: 'Set the password for your account so you can login and access all the features.',
      body: Form(
        child: ValueListenableBuilder<StateContext>(
          valueListenable: _passwordContextNotifier!,
          builder: (ctx, value, child){
            bool? isMatched = false;
            if(value.state != null){
              if(value.state is SuccessState){
                isMatched = value.state?.handle();
              }
            }
            // return Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     Visibility(
            //       visible: value.state != null ? value.state! is PasswordRequired ?
            //         (value.state! as PasswordRequired).handle(_newPassController!.text, _confirmPassController!.text) : false : false,
            //       child: const Padding(
            //         padding: EdgeInsets.symmetric(vertical: 16.0),
            //         child: FormErrorMessage(
            //           message: 'Password should be at least 7 characters'
            //         )
            //       )
            //     ),
            //     const Text('New Password'),
            //     const SizedBox(height: 8.0),
            //     PasswordField(
            //       controller: _newPassController!,
            //       onChanged: (v){
            //         _passwordContextNotifier!.value = PasswordContext(state: PasswordRequired());
            //         if(_newPassController!.text.length >= _requiredPasswordLength
            //             && _confirmPassController!.text.length >= _requiredPasswordLength){
            //           if(_newPassController!.text == _confirmPassController!.text){
            //             _passwordContextNotifier!.value = PasswordContext(state: PasswordMatched(PasswordRequired()));
            //           }else{
            //             _passwordContextNotifier!.value = PasswordContext(state: PasswordNotMatched(PasswordRequired()));
            //           }
            //         }
            //       },
            //       isPasswordMatched: value.state != null ? value.state! is PasswordMatched
            //           ? (value.state! as PasswordMatched).handle(_newPassController!.text, _confirmPassController!.text) : false : false
            //     ),
            //     const SizedBox(height: 16.0),
            //     const Text('Confirm new Password'),
            //     const SizedBox(height: 8.0),
            //     PasswordField(
            //       controller: _confirmPassController!,
            //       onChanged: (v){
            //         _passwordContextNotifier!.value = PasswordContext(state: PasswordRequired());
            //         if(_newPassController!.text.length >= _requiredPasswordLength
            //             && _confirmPassController!.text.length >= _requiredPasswordLength){
            //           if(_newPassController!.text == _confirmPassController!.text){
            //             _passwordContextNotifier!.value = PasswordContext(state: PasswordMatched(PasswordRequired()));
            //           } else{
            //             _passwordContextNotifier!.value = PasswordContext(state: PasswordNotMatched(PasswordRequired()));
            //           }
            //         }
            //       },
            //       isPasswordMatched: value.state != null ? value.state! is PasswordMatched
            //           ? (value.state! as PasswordMatched).handle(_newPassController!.text, _confirmPassController!.text) : false : false
            //     )
            //   ]
            // );
            return CustomForm(
              state: value,
              errorMessage: 'Password should be at least 7 characters',
              items: [
                FormItem(
                  label: 'New Password',
                  child: PasswordField(
                    controller: _newPassController!,
                    // onChanged: (v){
                    //   if(_newPassController!.text.length < _requiredPasswordLength && _confirmPassController!.text.length < _requiredPasswordLength){
                    //     _passwordContextNotifier!.value = StateContext(state: ErrorState());
                    //   }else{
                    //     if(_newPassController!.text.length >= _requiredPasswordLength
                    //         && _confirmPassController!.text.length >= _requiredPasswordLength){
                    //       if(_newPassController!.text == _confirmPassController!.text){
                    //         _passwordContextNotifier!.value = StateContext(state: SuccessState());
                    //       }
                    //       else{
                    //         _passwordContextNotifier!.value = const StateContext();
                    //       }
                    //     }else{
                    //       _passwordContextNotifier!.value = const StateContext();
                    //     }
                    //   }
                    // },
                    onChanged: _onChanged,
                    isPasswordMatched: isMatched
                  )
                ),
                const SizedBox(height: 16.0),
                FormItem(
                  label: 'Confirm Password',
                  child: PasswordField(
                    controller: _confirmPassController!,
                    // onChanged: (v){
                    //   if(_newPassController!.text.length < _requiredPasswordLength && _confirmPassController!.text.length < _requiredPasswordLength){
                    //     _passwordContextNotifier!.value = StateContext(state: ErrorState());
                    //   }else{
                    //     if(_newPassController!.text.length >= _requiredPasswordLength
                    //         && _confirmPassController!.text.length >= _requiredPasswordLength){
                    //       if(_newPassController!.text == _confirmPassController!.text){
                    //         _passwordContextNotifier!.value = StateContext(state: SuccessState());
                    //       }
                    //       else{
                    //         _passwordContextNotifier!.value = const StateContext();
                    //       }
                    //     }else{
                    //       _passwordContextNotifier!.value = const StateContext();
                    //     }
                    //   }
                    // },
                    onChanged: _onChanged,
                    isPasswordMatched: isMatched
                  )
                )
              ]
            );
          }
        )
      ),
      bottom: CustomFillButton(
        label: 'Create Password',
        onPressed: () async{
          if(FocusScope.of(context).hasFocus){
            FocusScope?.of(context).unfocus();
          }
          if(_passwordContextNotifier!.value.state != null){
            if(_passwordContextNotifier!.value.state is SuccessState){
              showLoading(context);
              await Future.delayed(const Duration(seconds: 1), (){
                Navigator.of(context, rootNavigator: true).pop();
                Navigator.of(context, rootNavigator: true).pop();
                Navigator.of(context).pushAndRemoveUntil(PageRouteBuilder(
                  pageBuilder: (ctx, anim1, anim2){
                    return const SubmitScreen(
                      title: 'Reset Successful',
                      subTitle: 'Your password is successfully reset.',
                      statusCode: 200
                    );
                  },
                  transitionsBuilder: (ctx, anim1, anim2, child){
                    return FadeTransition(
                      opacity: anim1,
                      child: child
                    );
                  }
                ), (routes) => false);
              });
            }
          }
          // if(_passwordContextNotifier!.value.state is PasswordMatched){
          //   if(_passwordContextNotifier!.value.state!.handle(_newPassController!.text, _confirmPassController!.text)){
          //     debugPrint('${_passwordContextNotifier!.value.state!.handle(_newPassController!.text, _confirmPassController!.text)}');
          //     showLoading(context);
          //     await Future.delayed(const Duration(seconds: 1), (){
          //       Navigator.of(context, rootNavigator: true).pop();
          //       Navigator.of(context, rootNavigator: true).pop();
          //       Navigator.of(context).pushAndRemoveUntil(PageRouteBuilder(
          //         pageBuilder: (ctx, anim1, anim2){
          //           return const ResetPasswordSuccessScreen();
          //         },
          //         transitionsBuilder: (ctx, anim1, anim2, child){
          //           return FadeTransition(
          //             opacity: anim1,
          //             child: child
          //           );
          //         }
          //       ), (routes) => false);
          //     });
          //   }
          // }
        }
      )
    );
  }
}

// abstract class PasswordState{
//   bool handle(final String newPassword, String confirmPassword);
// }
//
// class PasswordMatched implements PasswordState{
//   final PasswordRequired passwordRequired;
//
//   const PasswordMatched(this.passwordRequired);
//
//   @override
//   bool handle(String newPassword, String confirmPassword) {
//     // TODO: implement handle
//     return !passwordRequired.handle(newPassword, confirmPassword) && newPassword == confirmPassword;
//   }
//
// }
//
// class PasswordNotMatched implements PasswordState{
//   final PasswordRequired passwordRequired;
//
//   const PasswordNotMatched(this.passwordRequired);
//
//   @override
//   bool handle(String newPassword, String confirmPassword) {
//     // TODO: implement handle
//     return !passwordRequired.handle(newPassword, confirmPassword) && newPassword != confirmPassword;
//   }
//
// }
//
// class PasswordRequired implements PasswordState{
//   @override
//   bool handle(String newPassword, String confirmPassword) {
//     // TODO: implement handle
//     return newPassword.length < 7 || confirmPassword.length < 7;
//   }
// }
//
// class PasswordContext{
//   final PasswordState? state;
//   const PasswordContext({this.state});
// }
