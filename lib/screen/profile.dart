import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/widgets/avatar_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';
import 'package:rupp_client_app/widgets/custom_form.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

enum Gender{
  male, female
}

class ProfileScreen extends StatefulWidget {
  final String title;
  const ProfileScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  Gender? _gender;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _gender = Gender.male;
  }

  Future<String> _loadData() async{
    await Future.delayed(const Duration(seconds: 1));
    return 'Data';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: CustomMaterialColor.backgroundColor,
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverFillRemaining(
                hasScrollBody: false,
                child: FutureBuilder<String>(
                  future: _loadData(), initialData: null,
                  builder: (ctx, snapshot){
                    if(snapshot.hasData){
                      return Column(
                        children: [
                          const AvatarWidget(url: 'https://images.pexels.com/photos/10718776/pexels-photo-10718776.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
                          const SizedBox(height: 8.0),
                          const Text('Name', style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w600)),
                          const SizedBox(height: 16.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              const FormItem(
                                label: 'First Name',
                                child: ContainerRounded(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(16.0),
                                  child: Text('aa')
                                )
                              ),
                              const SizedBox(height: 16.0),
                              const FormItem(
                                label: 'Last Name',
                                child: ContainerRounded(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(16.0),
                                  child: Text('aa')
                                )
                              ),
                              const SizedBox(height: 16.0),
                              FormItem(
                                label: 'Gender',
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: RadioListTile<Gender>(
                                        title: const Text('Male'),
                                        value: Gender.male,
                                        groupValue: _gender,
                                        onChanged: (v){}
                                      )
                                    ),
                                    Expanded(
                                      child: RadioListTile<Gender>(
                                        title: const Text('Female'),
                                        value: Gender.female,
                                        groupValue: _gender,
                                        onChanged: (v){}
                                      )
                                    )
                                  ]
                                )
                              ),
                              const SizedBox(height: 16.0),
                              FormItem(
                                label: 'Date of birth',
                                child: Row(
                                  children: const [
                                    Flexible(
                                      child: ContainerRounded(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(16.0),
                                        child: Center(
                                          child: Text('04')
                                        )
                                      )
                                    ),
                                    SizedBox(width: 16.0),
                                    Flexible(
                                      flex: 2,
                                      child: ContainerRounded(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(16.0),
                                        child: Center(
                                          child: Text('February')
                                        )
                                      )
                                    ),
                                    SizedBox(width: 16.0),
                                    Flexible(
                                      child: ContainerRounded(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(16.0),
                                        child: Center(
                                          child: Text('2001')
                                        )
                                      )
                                    )
                                  ]
                                )
                              ),
                              const SizedBox(height: 16.0),
                              const FormItem(
                                label: 'Email',
                                child: ContainerRounded(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(16.0),
                                  child: Text('aa')
                                )
                              ),
                              const SizedBox(height: 16.0),
                              const FormItem(
                                label: 'Phone Number',
                                child: ContainerRounded(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(16.0),
                                  child: Text('aa')
                                )
                              ),
                              const SizedBox(height: 16.0),
                              const FormItem(
                                label: 'Address',
                                child: ContainerRounded(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(16.0),
                                  child: Text('aa')
                                )
                              )
                            ]
                          )
                        ]
                      );
                    }else{
                      return LoadingWidget.instance;
                    }
                  }
                )
              )
            )
          ]
        )
      )
    );
  }
}
