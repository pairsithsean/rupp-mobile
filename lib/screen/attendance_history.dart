import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/attendance_history_model.dart';
import 'package:rupp_client_app/model/view/attendance_history_view.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';

class AttendanceHistoryScreen extends StatefulWidget {
  final String? title;
  const AttendanceHistoryScreen({Key? key, this.title}) : super(key: key);

  @override
  _AttendanceHistoryScreenState createState() => _AttendanceHistoryScreenState();
}

class _AttendanceHistoryScreenState extends State<AttendanceHistoryScreen> {

  final List<AttendanceHistoryModel> _data = [
    AttendanceHistoryModel(
      date: 'December 2021',
      data: const [
        AttendanceHistoryData(title: 'Full-Day', date: 'Fri, 10', reason: 'Busy', status: 'Awaiting'),
        AttendanceHistoryData(title: 'Half-Day', date: 'Fri, 10', reason: 'Busy', status: 'Approved'),
        AttendanceHistoryData(title: '3 Days', date: 'Fri, 10', reason: 'Busy', status: 'Declined'),
        AttendanceHistoryData(title: '4 Days', date: 'Fri, 10', reason: 'Busy', status: 'Declined')
      ]
    ),
    AttendanceHistoryModel(
      date: 'January 2022',
      data: const [
        AttendanceHistoryData(title: 'Full-Day', date: 'Fri, 10', reason: 'Busy', status: 'Awaiting'),
        AttendanceHistoryData(title: 'Half-Day', date: 'Fri, 10', reason: 'Busy', status: 'Approved'),
        AttendanceHistoryData(title: '3 Days', date: 'Fri, 10', reason: 'Busy', status: 'Declined'),
        AttendanceHistoryData(title: '4 Days', date: 'Fri, 10', reason: 'Busy', status: 'Declined')
      ]
    )
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: Column(
          children: [
            CustomAppbar(title: 'Attendance History').build(context),
            Expanded(
              // child: SingleChildScrollView(
              //   child: SizedBox(
              //     width: double.infinity,
              //     child: Padding(
              //       padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              //       child: Column(
              //         children: _data.map((e){
              //           return Column(
              //             crossAxisAlignment: CrossAxisAlignment.start,
              //             children: [
              //               const SizedBox(height: 16.0),
              //               Text(e.date, style: const TextStyle(color: TextColor.secondaryColor)),
              //               const SizedBox(height: 16.0),
              //               Column(
              //                 children: e.data.map((data){
              //                   return AttendanceHistoryView(data: data);
              //                 }).toList()
              //               )
              //             ]
              //           );
              //         }).toList()
              //       )
              //     )
              //   )
              // )
              child: SingleChildScrollView(
                child: SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                    child: ColumnComposite(
                      iComponent: _data.map((e){
                        return ColumnComposite(
                          iComponent: [
                            Component(
                              child: Text(e.date, style: const TextStyle(color: TextColor.secondaryColor))
                            ),
                            ColumnComposite(
                              iComponent: e.data.map((e1){
                                return Component(
                                  child: AttendanceHistoryView(data: e1)
                                );
                              }).toList()
                            )
                          ]
                        );
                      }).toList()
                    ).build(context)
                  )
                )
              )
            )
          ]
        )
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: (){
          Navigator.of(context).pushNamed('request-absence');
        }
      ),
    );
  }
}

abstract class FactoryStatus{
  static const double horizontalPadding = 12.0;
  Widget build(final BuildContext context);
}

class FactoryAwaitingStatus extends FactoryStatus{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const _StatusWidget(label: 'Awaiting', backgroundColor: Color.fromRGBO(250, 197, 38, 1.0));
  }
}

class FactoryApprovedStatus extends FactoryStatus{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const _StatusWidget(label: 'Approved', backgroundColor: Color.fromRGBO(35, 193, 113, 1.0));
  }
}

class FactoryDeclinedStatus extends FactoryStatus{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const _StatusWidget(label: 'Declined', backgroundColor: Color.fromRGBO(238, 95, 80, 1.0));
  }
}

class FactoryStatusCreator{

  const FactoryStatusCreator();

  FactoryStatus factoryMethod(final AttendanceHistoryData historyModel){
    switch(historyModel.status){
      case 'Awaiting': return FactoryAwaitingStatus();
      case 'Approved': return FactoryApprovedStatus();
      default: return FactoryDeclinedStatus();
    }
  }
}

class _StatusWidget extends StatelessWidget{
  final String label;
  final Color? backgroundColor;
  const _StatusWidget({final Key? key, required this.label,
    this.backgroundColor = Colors.transparent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: FactoryStatus.horizontalPadding, vertical: 4.0),
      decoration: BoxDecoration(
        color: backgroundColor!,
        borderRadius: BorderRadius.circular(50.0)
      ),
      child: Text(label, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(color: Colors.white, fontSize: 12.0))
    );
  }
}
