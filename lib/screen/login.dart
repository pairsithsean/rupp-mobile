import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rupp_client_app/model/login_model.dart';
import 'package:rupp_client_app/model/state_model.dart';
import 'package:rupp_client_app/screen/main.dart';
import 'package:rupp_client_app/screen/reset_password.dart';
import 'package:rupp_client_app/screen/scan.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/utils/custom_bottomsheet.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';
import 'package:rupp_client_app/widgets/custom_form.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController? _enterCodeController;
  TextEditingController? _passController;

  ValueNotifier<bool>? _fieldNotifier;

  ValueNotifier<StateContext?>? _stateContextNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    //
    _stateContextNotifier = ValueNotifier<StateContext?>(const StateContext());
    _enterCodeController = TextEditingController();
    _passController = TextEditingController();
    _fieldNotifier = ValueNotifier<bool>(false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _enterCodeController?.dispose();
    _passController?.dispose();
    _fieldNotifier?.dispose();
    _stateContextNotifier?.dispose();
    super.dispose();
  }

  bool get _fieldIsNotEmpty{
    String phone = _enterCodeController!.text;
    String pass = _passController!.text;
    bool isValid = phone.isNotEmpty && pass.isNotEmpty;
    return isValid;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: const Color.fromRGBO(242, 242, 245, 1.0),
      child: SafeArea(
        top: false, bottom: false,
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 128.0),
                      child: ClipPath(
                        clipper: _CustomClipperPath(),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20.0)
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  flex: 0,
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 150 / 1.5),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                        child: Text('Welcome', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600))
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                        child: ValueListenableBuilder<StateContext?>(
                                          valueListenable: _stateContextNotifier!,
                                          builder: (ctx, value, child){
                                            bool? errorState = false;
                                            if(value!.state != null){
                                              if(value.state is ErrorState){
                                                errorState = value.state?.handle();
                                              }
                                            }
                                            return CustomForm(
                                              state: value,
                                              errorMessage: 'Incorrect code or password',
                                              items: [
                                                FormItem(
                                                  label: '',
                                                  child: CustomTextField(
                                                    controller: _enterCodeController!,
                                                    errorState: errorState!,
                                                    hintText: 'Enter code',
                                                    onFieldSubmitted: (v){},
                                                    onChanged: (v){
                                                      _stateContextNotifier!.value = const StateContext();
                                                      _fieldNotifier!.value = _fieldIsNotEmpty;
                                                    }
                                                  )
                                                ),
                                                FormItem(
                                                  label: '',
                                                  child: PasswordField(
                                                    controller: _passController!,
                                                    hintText: 'Enter Password',
                                                    errorState: errorState,
                                                    onFieldSubmitted: (v){},
                                                    onChanged: (v){
                                                      _stateContextNotifier!.value = const StateContext();
                                                      _fieldNotifier!.value = _fieldIsNotEmpty;
                                                    }
                                                  )
                                                )
                                              ]
                                            );
                                          },
                                        )
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                        child: Align(
                                          alignment: Alignment.bottomRight,
                                          child: Material(
                                            type: MaterialType.transparency,
                                            child: InkWell(
                                              child: const Text('Forget password?', style: TextStyle(color: CustomMaterialColor.primary)),
                                              onTap: (){
                                                const content = Padding(
                                                  padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 56.0),
                                                  child: ResetPasswordScreen()
                                                );
                                                DraggableBottomSheetBuilder().show(context, content);
                                              }
                                            )
                                          )
                                        )
                                      ),
                                      Flexible(
                                        flex: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 26.0),
                                          child: Align(
                                            alignment: Alignment.bottomCenter,
                                            child: ValueListenableBuilder<bool>(
                                              valueListenable: _fieldNotifier!,
                                              child: const CustomFillButton(
                                                label: 'Login',
                                                onPressed: null
                                              ),
                                              builder: (ctx, value, child){
                                                if(value){
                                                  return CustomFillButton(
                                                    label: 'Login',
                                                    onPressed: (){
                                                      if(FocusScope.of(context).hasFocus){
                                                        FocusScope.of(context).unfocus();
                                                      }
                                                      // if(_phoneController!.text == '081428264' && _passController!.text == '1234'){
                                                      //   Navigator.of(context).pushReplacement(
                                                      //     PageRouteBuilder(
                                                      //       pageBuilder: (ctx, anim1, anim2){
                                                      //         return const MainScreen();
                                                      //       },
                                                      //       transitionsBuilder: (ctx, anim1, anim2, child){
                                                      //         return FadeTransition(
                                                      //           child: child,
                                                      //           opacity: anim1
                                                      //         );
                                                      //       }
                                                      //     )
                                                      //   );
                                                      // }else{
                                                      //   _stateContextNotifier!.value = StateContext(state: ErrorState());
                                                      // }s
                                                      Navigator.of(context).pushReplacement(
                                                        PageRouteBuilder(
                                                          pageBuilder: (ctx, anim1, anim2){
                                                            return const MainScreen();
                                                          },
                                                          transitionsBuilder: (ctx, anim1, anim2, child){
                                                            return FadeTransition(
                                                              child: child,
                                                              opacity: anim1
                                                            );
                                                          }
                                                        )
                                                      );
                                                    }
                                                  );
                                                }else{
                                                  return child!;
                                                }
                                              }
                                            )
                                          )
                                        )
                                      )
                                    ]
                                  )
                                )
                              ]
                            )
                          )
                        )
                      )
                    )
                  ),
                  Positioned(
                    top: 56.0, left: 16.0, right: 16.0,
                    child: Center(
                      child: SizedBox(
                        width: 150.0, height: 150.0,
                        child: Hero(
                          tag: 'LOGO',
                          child: Image.asset('assets/png/LOGO-min.png', fit: BoxFit.contain)
                        )
                      )
                    )
                  )
                ]
              )
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 36.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('New User?'),
                  const SizedBox(width: 8.0),
                  InkWell(
                    child: const Text('Register', style: TextStyle(color: CustomMaterialColor.primary)),
                    onTap: (){}
                  )
                ]
              )
            ),
            Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                color: CustomMaterialColor.primary,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50.0), topRight: Radius.circular(50.0)
                )
              ),
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: TextButton.icon(
                  icon: const Icon(Icons.camera_alt_outlined),
                  label: const Text('Scan QR Code'),
                  style: TextButton.styleFrom(
                    primary: Colors.white
                  ),
                  onPressed: (){
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (ctx, anim1, anim2){
                          return const ScanScreen();
                        },
                        transitionsBuilder: (ctx, anim1, anim2, child){
                          return FadeTransition(
                            opacity: anim1,
                            child: child
                          );
                        }
                      )
                    );
                  }
                )
              )
            )
          ]
        )
      )
    );
  }
}

class _CustomClipperPath extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {
    // TODO: implement getClip
    final width = size.width;
    final height = size.height;
    const midHeight = 150.0 / 1.5;
    const gap = 145.0;
    final center = width / 2.0;

    final to1 = Offset(center, midHeight);
    final c1 = Offset(center - gap + 56, 0);
    final c2 = Offset(center - gap + 56, midHeight);

    final to2 = Offset(center + gap, 0);
    final c3 = Offset(center + gap - 56, midHeight);
    final c4 = Offset(center + gap - 56 + 4, 0);

    final path = Path();
    path.moveTo(0, 0);
    path.lineTo(width, 0);
    path.lineTo(width, height);
    path.lineTo(0, height);

    path.moveTo(center - gap, 0);
    path.cubicTo(c1.dx, c1.dy, c2.dx, c2.dy, to1.dx, to1.dy);
    path.cubicTo(c3.dx, c3.dy, c4.dx, c4.dy, to2.dx, to2.dy);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }

}