import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rupp_client_app/model/bookmark_model.dart';
import 'package:rupp_client_app/model/view/bookmark_view.dart';
import 'package:rupp_client_app/repository/material_repository.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

class BookmarkScreen extends StatefulWidget {
  const BookmarkScreen({Key? key}) : super(key: key);

  @override
  _BookmarkScreenState createState() => _BookmarkScreenState();
}

class _BookmarkScreenState extends State<BookmarkScreen> {

  Future<List<BookmarkModel>> _loadData() async{
    await Future.delayed(const Duration(milliseconds: 500));
    return List.generate(2, (index) => BookmarkModel(title: 'aaa', date: 'bbbb',
        material: MaterialRepository.all().first,
        bookmarkContext: const BookmarkContext(state: BookmarkSaveState())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            const CustomSliverAppbar(title: 'Bookmarks'),
            SliverFillRemaining(
              hasScrollBody: false,
              child: FutureBuilder<List<BookmarkModel>>(
                future: _loadData(), initialData: null,
                builder: (ctx, snapshot){
                  if(snapshot.hasData){
                    if(snapshot.data!.isEmpty){
                      return Center(
                        child: SvgPicture.asset('assets/svg/no-data.svg')
                      );
                    }else{
                      return Column(
                        children: snapshot.data!.map((e) {
                          return Padding(
                            padding: const EdgeInsets.only(top: 16.0),
                            child: BookmarkView(data: e)
                          );
                        }).toList()
                      );
                    }
                  }else{
                    return LoadingWidget.instance;
                  }
                }
              )
            )
          ]
        )
      )
    );
  }
}
