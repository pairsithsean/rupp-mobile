import 'package:flutter/material.dart';
import 'package:rupp_client_app/repository/bottombar_repository.dart';
import 'package:rupp_client_app/screen/attendance_history.dart';
import 'package:rupp_client_app/screen/bookmark.dart';
import 'package:rupp_client_app/screen/home.dart';
import 'package:rupp_client_app/screen/learning_result.dart';
import 'package:rupp_client_app/screen/more.dart';
import 'package:rupp_client_app/screen/notification.dart';
import 'package:rupp_client_app/screen/online_learning.dart';
import 'package:rupp_client_app/screen/payment.dart';
import 'package:rupp_client_app/screen/payment_history.dart';
import 'package:rupp_client_app/screen/profile.dart';
import 'package:rupp_client_app/screen/request_absence.dart';
import 'package:rupp_client_app/screen/settings.dart';
import 'package:rupp_client_app/screen/student_performance.dart';
import 'package:rupp_client_app/screen/timetable.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  ValueNotifier<int>? _selectedBottomBarNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedBottomBarNotifier = ValueNotifier<int>(0);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _selectedBottomBarNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ValueListenableBuilder<int>(
        valueListenable: _selectedBottomBarNotifier!,
        builder: (ctx, value, child){
          if(value == 3){
            return Navigator(
              onGenerateRoute: (settings){
                switch(settings.name){
                  case 'bookmark': return MaterialPageRoute(
                    builder: (ctx) => const BookmarkScreen()
                  );
                  case 'student-performance': return MaterialPageRoute(
                    builder: (ctx) => const StudentPerformanceScreen()
                  );
                  case 'learning-result': return MaterialPageRoute(
                    builder: (ctx) => const LearningResultScreen(title: 'Learning Result')
                  );
                  case 'attendance-history': return MaterialPageRoute(
                    builder: (ctx) => const AttendanceHistoryScreen(title: 'Attendance History')
                  );
                  case 'request-absence': return MaterialPageRoute(
                    builder: (ctx) => const RequestAbsenceScreen(title: 'Request Absence')
                  );
                  case 'payment-history': return MaterialPageRoute(
                    builder: (ctx) => const PaymentHistoryScreen(title: 'Payment History')
                  );
                  case 'online-learning': return MaterialPageRoute(
                    builder: (ctx) => const OnlineLearningScreen(title: 'Online Learning')
                  );
                  case 'payment': return MaterialPageRoute(
                    builder: (ctx) => const PaymentScreen(title: 'Payment')
                  );
                  case 'settings': return MaterialPageRoute(
                    builder: (ctx) => const SettingsScreen(title: 'Settings')
                  );
                  case 'timetable': return MaterialPageRoute(
                    builder: (ctx) => const TimetableScreen(title: 'Timetable')
                  );
                  case 'profile': return MaterialPageRoute(
                    builder: (ctx) => const ProfileScreen(title: 'Profile')
                  );
                  default: return MaterialPageRoute(
                    builder: (ctx) => const MoreScreen()
                  );
                }
              }
            );
          }else if(value == 1){
            return const LearningResultScreen(title: 'Learning Result');
          }
          else if(value == 2){
            return const NotificationScreen(title: 'Notifications');
          }
          return const HomeScreen();
        }
      ),
      bottomNavigationBar: ValueListenableBuilder<int>(
        valueListenable: _selectedBottomBarNotifier!,
        builder: (ctx, value, child){
          return BottomNavigationBar(
            selectedFontSize: 12.0, unselectedFontSize: 12.0,
            onTap: (index){
              _selectedBottomBarNotifier!.value = index;
            }, currentIndex: value,
            type: BottomNavigationBarType.fixed,
            items: BottomBarItemRepository.all
          );
        }
      )
    );
  }
}

