import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/utils/custom_bottomsheet.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_dropdown.dart';
import 'package:rupp_client_app/widgets/profile_widget.dart';

class StudentPerformanceScreen extends StatefulWidget {
  const StudentPerformanceScreen({Key? key}) : super(key: key);

  @override
  _StudentPerformanceScreenState createState() => _StudentPerformanceScreenState();
}

class _StudentPerformanceScreenState extends State<StudentPerformanceScreen> {

  final _dataMap = {
    'Project' : const _DonutChartModel(score: 90.0, color: Colors.red),
    'Project 1' : const _DonutChartModel(score: 75.0, color: Colors.blue),
    'Project 2' : const _DonutChartModel(score: 85.0, color: Colors.indigoAccent),
    'Project 3' : const _DonutChartModel(score: 50.0, color: Colors.amberAccent)
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            const CustomSliverAppbar(title: 'Student Performance'),
            // const SliverPadding(
            //   padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            //   sliver: SliverToBoxAdapter(
            //     child: ProfileWidget(
            //       name: 'Name',
            //       subTitle: Text('')
            //     )
            //   )
            // ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              sliver: SliverToBoxAdapter(
                child: CustomDropdown(
                  items: const [
                    'aa', 'gg'
                  ],
                  onSelected: (index){}
                )
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0, top: 56.0),
              sliver: SliverToBoxAdapter(
                // child: Container(
                //   decoration: BoxDecoration(
                //     color: Colors.white,
                //     borderRadius: BorderRadius.circular(10.0),
                //     boxShadow: [
                //       BoxShadow(color: Colors.black.withAlpha(12), blurRadius: 12.0)
                //     ]
                //   ),
                //   child: Column(
                //     children: [
                //       // const SizedBox(height: 16.0),
                //       // const Center(
                //       //   child: Text('Performance', style: TextStyle(
                //       //     fontSize: 20.0, fontWeight: FontWeight.w600
                //       //   ))
                //       // ),
                //       // const SizedBox(height: 150.0 / 2.0),
                //       // Center(
                //       //   child: CustomPaint(
                //       //     size: const Size(150, 150),
                //       //     painter: _DonutChart(dataMap: _dataMap)
                //       //   )
                //       // ),
                //       // const SizedBox(height: 150.0 / 4.0),
                //       Column(
                //         children: _dataMap.keys.map((key) {
                //           final progressScore = (_dataMap[key]!.score / 100.0);
                //           return Padding(
                //             padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                //             child: Row(
                //               crossAxisAlignment: CrossAxisAlignment.end,
                //               children: [
                //                 Expanded(
                //                   child: Column(
                //                     crossAxisAlignment: CrossAxisAlignment.start,
                //                     children: [
                //                       Row(
                //                         children: [
                //                           Expanded(
                //                             child: Text(key, style: const TextStyle(color: TextColor.secondaryColor))
                //                           ),
                //                           Flexible(
                //                             child: Align(
                //                               alignment: Alignment.topRight,
                //                               child: Text('${_dataMap[key]!.score.toInt()}%', style: const TextStyle(
                //                                 fontWeight: FontWeight.w300, color: TextColor.secondaryColor
                //                               ))
                //                             )
                //                           )
                //                         ]
                //                       ),
                //                       // Stack(
                //                       //   children: [
                //                       //     Container(
                //                       //       width: size.width, height: 8.0,
                //                       //       decoration: BoxDecoration(
                //                       //         color: Colors.black.withAlpha(12),
                //                       //         borderRadius: BorderRadius.circular(10.0)
                //                       //       )
                //                       //     ),
                //                       //     Container(
                //                       //       width: (size.width / 2) - (16.0 + 16.0), height: 8.0,
                //                       //       decoration: BoxDecoration(
                //                       //         color: Colors.red,
                //                       //         borderRadius: BorderRadius.circular(10.0)
                //                       //       )
                //                       //     )
                //                       //   ]
                //                       // )
                //                       const SizedBox(height: 8.0),
                //                       Row(
                //                         children: [
                //                           Flexible(
                //                             child: SizedBox(
                //                               height: 8.0,
                //                               child: ClipRRect(
                //                                 borderRadius: const BorderRadius.all(Radius.circular(10)),
                //                                 child: LinearProgressIndicator(
                //                                   value: progressScore,
                //                                   valueColor: AlwaysStoppedAnimation<Color>(_dataMap[key]!.color),
                //                                   backgroundColor: const Color(0xffD6D6D6)
                //                                 )
                //                               )
                //                             )
                //                           )
                //                         ]
                //                       )
                //                     ]
                //                   )
                //                 ),
                //                 const SizedBox(width: 16.0),
                //                 Align(
                //                   child: GestureDetector(
                //                     child: SvgPicture.asset('assets/svg/info.svg', width: 12.0, height: 12.0),
                //                     onTap: (){
                //                       final Widget content = Padding(
                //                         padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                //                         child: Row(
                //                           crossAxisAlignment: CrossAxisAlignment.start,
                //                           children: [
                //                             SvgPicture.asset('assets/svg/info.svg', width: 14.0, height: 14.0),
                //                             const SizedBox(width: 16.0),
                //                             const Flexible(
                //                               child: Text('This is your performance in School Projects in this semester.', style: TextStyle(
                //                                 color: TextColor.secondaryColor, fontSize: 16.0
                //                               ))
                //                             )
                //                           ]
                //                         )
                //                       );
                //                       DraggableBottomSheetBuilder().setInitialChildSize(0.2).setMinChildSize(0.2)
                //                           .show(context, content);
                //                     }
                //                   ),
                //                   alignment: Alignment.bottomCenter,
                //                 )
                //               ]
                //             )
                //           );
                //         }).toList()
                //       )
                //     ]
                //   )
                // )
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Text('Subject:', style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 17.0
                      ))
                    ),
                    ColumnComposite(
                      iComponent: _dataMap.keys.map((key) {
                        final progressScore = (_dataMap[key]!.score / 100.0);
                        return Component(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(key, style: const TextStyle(color: TextColor.secondaryColor))
                                          ),
                                          Flexible(
                                            child: Align(
                                              alignment: Alignment.topRight,
                                              child: Text('${_dataMap[key]!.score.toInt()}%', style: const TextStyle(
                                                fontWeight: FontWeight.w300, color: TextColor.secondaryColor
                                              ))
                                            )
                                          )
                                        ]
                                      ),
                                      const SizedBox(height: 8.0),
                                      Row(
                                        children: [
                                          Flexible(
                                            child: SizedBox(
                                              height: 8.0,
                                              child: ClipRRect(
                                                borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                child: LinearProgressIndicator(
                                                  value: progressScore,
                                                  valueColor: AlwaysStoppedAnimation<Color>(_dataMap[key]!.color),
                                                  backgroundColor: const Color(0xffD6D6D6)
                                                )
                                              )
                                            )
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ),
                                const SizedBox(width: 16.0),
                                Align(
                                  child: GestureDetector(
                                    child: SvgPicture.asset('assets/svg/info.svg', width: 12.0, height: 12.0),
                                    onTap: (){
                                      final Widget content = Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                SvgPicture.asset('assets/svg/info.svg', width: 14.0, height: 14.0),
                                                const SizedBox(width: 16.0),
                                                Flexible(
                                                  child: Text(key)
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      );
                                      DraggableBottomSheetBuilder().setInitialChildSize(0.2).setMinChildSize(0.2)
                                          .show(context, content);
                                    }
                                  ),
                                  alignment: Alignment.bottomCenter,
                                )
                              ]
                            )
                          )
                        );
                      }).toList()
                    )
                  ]
                ).build(context)
              )
            )
          ]
        )
      )
    );
  }
}

class _DonutChart extends CustomPainter{

  final Map<String, _DonutChartModel> dataMap;
  const _DonutChart({required this.dataMap});

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    final rect = Rect.fromLTWH(0, 0, size.width, size.height);
    const double strokeWidth = 45.0;
    const double startAngle = 4.71;

    double total = 0;
    for(_DonutChartModel chart in dataMap.values){
      total += chart.score;
    }
    //
    double angle = 0.0;
    //Random rand = Random();
    for(_DonutChartModel chart in dataMap.values){
      // final int r = rand.nextInt(256);
      // final int g = rand.nextInt(256);
      // final int b = rand.nextInt(256);
      final scoreRadian = (chart.score / total) * 6.28;
      canvas.drawArc(rect, angle + startAngle, scoreRadian, false,
          Paint()..color = chart.color..style = PaintingStyle.stroke..strokeWidth = strokeWidth);
      angle += scoreRadian;
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}

class _DonutChartModel{
  final double score;
  final Color color;

  const _DonutChartModel({required this.score, required this.color});
}
