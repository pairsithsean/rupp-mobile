import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/online_learning_model.dart';
import 'package:rupp_client_app/model/view/audio_player_view.dart';
import 'package:rupp_client_app/model/view/pdf_view.dart';
import 'package:rupp_client_app/model/view/video_view.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/empty_widget.dart';

class OnlineLearningDetail extends StatefulWidget {
  final OnlineLearningData data;
  const OnlineLearningDetail({Key? key, required this.data}) : super(key: key);

  @override
  _OnlineLearningDetailState createState() => _OnlineLearningDetailState();
}

class _OnlineLearningDetailState extends State<OnlineLearningDetail> {

  Widget? _viewBuilder;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    switch(widget.data.material.title){
      case 'Video': _viewBuilder = VideoView(data: widget.data); break;
      case 'Podcast': _viewBuilder = AudioPlayerView(data: widget.data); break;
      case 'PDF': _viewBuilder = PdfView(data: widget.data); break;
      default: _viewBuilder = EmptyWidget.instance;
    }
  }

  @override
  Widget build(BuildContext context) {
    // switch(widget.title){
    //   case 'Video': return const VideoView();
    //   case 'PDF': return _viewBuilder!.pdfView(context: context);
    //   default: return EmptyWidget.instance;
    // }
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.data.material.title),
            SliverFillRemaining(
              hasScrollBody: false,
              child: _viewBuilder
            )
          ]
        )
      )
    );
  }
}
