import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rupp_client_app/screen/onboarding.dart';
import 'package:rupp_client_app/style/text_color.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(const Duration(seconds: 3),
            () => Navigator.of(context).pushReplacement(
      PageRouteBuilder(
        pageBuilder: (ctx, anim1, anim2){
          return const OnBoardingScreen();
        },
        transitionsBuilder: (ctx, anim1, anim2, child){
          return FadeTransition(
            opacity: anim1,
            child: child
          );
        })
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFFEE3436),
              Color(0xFFFAFAFC)
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter
          )
        ),
        child: Stack(
          children: [
            Positioned.fill(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Image.asset('assets/png/LOGO-min.png', width: 150.0, height: 150.0)
                  ),
                  const SizedBox(height: 56.0),
                  const Flexible(
                    child: Text('Welcome', style: TextStyle(
                      fontSize: 20.0, fontWeight: FontWeight.bold
                    ))
                  )
                ]
              )
            ),
            const Positioned(
              bottom: 36.0, left: 0, right: 0,
              child: Center(
                child: Text('Powered by CAMIS', style: TextStyle(
                  color: TextColor.secondaryColor, fontWeight: FontWeight.bold
                ))
              )
            )
          ]
        )
      )
    );
  }
}
