import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rupp_client_app/model/view/onboarding_view.dart';
import 'package:rupp_client_app/repository/onboarding_repository.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'login.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {

  PageController? _pageController;
  StreamController<int>? _pageIndicatorController;

  final _duration = const Duration(milliseconds: 500);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    _pageController = PageController(initialPage: 0);
    _pageIndicatorController = StreamController<int>.broadcast();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController?.dispose();
    _pageIndicatorController?.close();
    super.dispose();
  }


  void _navigateToLogin() async{
    final prefs = await SharedPreferences.getInstance();
    final result = await prefs.setBool('onBoarding_done', true);
    if(result){
      Navigator.of(context).pushReplacement(
        PageRouteBuilder(
          pageBuilder: (ctx, anim1, anim2){
            return const LoginScreen();
          },
          transitionsBuilder: (ctx, anim1, anim2, child){
            return FadeTransition(
              opacity: anim1,
              child: child
            );
          }
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Positioned(
            top: 0.0, left: 0, right: 0, bottom: 0,
            child: PageView(
              controller: _pageController,
              onPageChanged: (pageIndex){
                _pageIndicatorController?.sink.add(pageIndex);
              },
              children: OnBoardingRepository.all.map((e){
                return OnBoardingView(data: e);
              }).toList()
            )
          ),
          Positioned(
            top: 56.0, right: 16.0,
            child: TextButton(
              onPressed: _navigateToLogin,
              child: const Text('SKIP'),
              style: TextButton.styleFrom(primary: TextColor.secondaryColor),
            )
          ),
          Positioned(
            bottom: 36.0, right: 16.0, left: 16.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: StreamBuilder<int>(
                    stream: _pageIndicatorController!.stream, initialData: 0,
                    builder: (ctx, snapshot){
                      return AnimatedSmoothIndicator(
                        activeIndex: snapshot.data!,
                        effect: const ExpandingDotsEffect(
                          activeDotColor: CustomMaterialColor.primary,
                          dotColor: Color(0xFFC7C9D9),
                          dotWidth: 12,
                          dotHeight: 12
                        ),
                        count: OnBoardingRepository.all.length
                      );
                    }
                  )
                ),
                Flexible(
                  child: FloatingActionButton(
                    elevation: 0.0,
                    onPressed: () async{
                      if(OnBoardingRepository.all.isNotEmpty){
                        if(_pageController!.page == OnBoardingRepository.all.length - 1){
                          _navigateToLogin();
                          return;
                        }
                        await _pageController?.nextPage(duration: _duration, curve: Curves.ease);
                      }
                    },
                    child: const Icon(Icons.arrow_forward_ios_outlined)
                  )
                )
              ]
            )
          )
        ]
      )
    );
  }
}
