import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:rupp_client_app/screen/submit.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/utils/custom_bottomsheet.dart';
import 'package:rupp_client_app/utils/show_loading_indicator.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_appbar.dart';
import 'package:rupp_client_app/widgets/custom_calendar_carousel.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';
import 'package:rupp_client_app/widgets/custom_sliding_control.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class RequestAbsenceScreen extends StatefulWidget {
  final String? title;
  const RequestAbsenceScreen({Key? key, this.title = ''}) : super(key: key);

  @override
  _RequestAbsenceScreenState createState() => _RequestAbsenceScreenState();
}

class _RequestAbsenceScreenState extends State<RequestAbsenceScreen> {

  TextEditingController? _reasonController;

  ValueNotifier<int>? _slidingNotifier;
  ValueNotifier<int>? _selectedTimeIndexNotifier;
  ValueNotifier<String?>? _selectedTimeValueNotifier;
  //FactoryTimeCreator? _timeCreator;
  
  final List<String> _timeData = [
    '7:00 AM', '7:15 AM', '7:30 AM', '1:30 PM', '2:00 PM'
  ];

  DraggableBottomSheetBuilder? _bottomSheetBuilder;
  ValueNotifier<DateTime>? _selectedFromDateNotifier;
  ValueNotifier<DateTime>? _selectedToDateNotifier;

  final DateTime _todayDate = DateTime.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //_timeCreator = const FactoryTimeCreator();
    _bottomSheetBuilder = DraggableBottomSheetBuilder();
    _reasonController = TextEditingController();
    _slidingNotifier = ValueNotifier<int>(0);
    _selectedTimeIndexNotifier = ValueNotifier<int>(-1);
    _selectedTimeValueNotifier = ValueNotifier<String?>(null);
    _selectedFromDateNotifier = ValueNotifier<DateTime>(_todayDate);
    _selectedToDateNotifier = ValueNotifier<DateTime>(_todayDate);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _reasonController?.dispose();
    _slidingNotifier?.dispose();
    _selectedTimeIndexNotifier?.dispose();
    _selectedTimeValueNotifier?.dispose();
    _selectedFromDateNotifier?.dispose();
    _selectedToDateNotifier?.dispose();
    super.dispose();
  }

  List<String> _getQuery({final int sliderIndex = 0}){
    List<String> queryTimeData = [];
    if(sliderIndex == 0){
      queryTimeData = _timeData.where((element) => element.toLowerCase().contains('am'.toLowerCase())).toList();
    }else{
      queryTimeData = _timeData.where((element) => element.toLowerCase().contains('pm'.toLowerCase())).toList();
    }
    return queryTimeData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: CustomScrollView(
          slivers: [
            CustomSliverAppbar(title: widget.title!),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Text('From Date', style: TextStyle(color: TextColor.secondaryColor)),
                    ),
                    Component(
                      child: GestureDetector(
                        onTap: (){
                          _bottomSheetBuilder?.show(context, CustomCalendar(
                            currentValue: _selectedFromDateNotifier!.value,
                            onDateSelect: (dateTime){
                              _selectedFromDateNotifier!.value = dateTime;
                              Navigator.of(context, rootNavigator: true).pop();
                              debugPrint('$dateTime');
                            }
                          ));
                        },
                        child: Container(
                          padding: const EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black12),
                            borderRadius: BorderRadius.circular(10.0)
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: ValueListenableBuilder<DateTime>(
                                  valueListenable: _selectedFromDateNotifier!,
                                  child: Text(DateFormat('dd/MMM/yyyy').format(_todayDate), maxLines: 1, overflow: TextOverflow.ellipsis),
                                  builder: (ctx, value, child){
                                    if(value != _todayDate){
                                      final dateFormat = DateFormat('dd/MMM/yyyy').format(value);
                                      return Text(dateFormat);
                                    }else{
                                      return child!;
                                    }
                                  }
                                )
                              ),
                              SvgPicture.asset('assets/svg/date.svg')
                            ]
                          )
                        )
                      )
                    )
                  ]
                ).build(context)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              sliver: SliverToBoxAdapter(
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Text('To Date', style: TextStyle(color: TextColor.secondaryColor)),
                    ),
                    Component(
                      child: GestureDetector(
                        onTap: (){
                          _bottomSheetBuilder?.show(context, CustomCalendar(
                            currentValue: _selectedToDateNotifier!.value,
                            onDateSelect: (dateTime){
                              _selectedToDateNotifier!.value = dateTime;
                              Navigator.of(context, rootNavigator: true).pop();
                              debugPrint('$dateTime');
                            }
                          ));
                        },
                        child: Container(
                          padding: const EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black12),
                            borderRadius: BorderRadius.circular(10.0)
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: ValueListenableBuilder<DateTime>(
                                  valueListenable: _selectedToDateNotifier!,
                                  child: Text(DateFormat('dd/MMM/yyyy').format(_todayDate), maxLines: 1, overflow: TextOverflow.ellipsis),
                                  builder: (ctx, value, child){
                                    if(value != _todayDate){
                                      final dateFormat = DateFormat('dd/MMM/yyyy').format(value);
                                      return Text(dateFormat);
                                    }else{
                                      return child!;
                                    }
                                  }
                                )
                              ),
                              SvgPicture.asset('assets/svg/date.svg')
                            ]
                          )
                        )
                      )
                    )
                  ]
                ).build(context)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverToBoxAdapter(
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Text('Select Time (Optional)', style: TextStyle(color: TextColor.secondaryColor)),
                    ),
                    Component(
                      child: GestureDetector(
                        onTap: (){
                          final content = Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    const Expanded(
                                      child: Text('Select Time', style: TextStyle(
                                        fontWeight: FontWeight.w600, color: CustomMaterialColor.primary
                                      ))
                                    ),
                                    Expanded(
                                      child: ValueListenableBuilder<int>(
                                        valueListenable: _slidingNotifier!,
                                        builder: (ctx, value, child){
                                          return CustomSlidingControl(
                                            groupValue: value,
                                            onValueChanged: (int? value) {
                                              _slidingNotifier!.value = value!;
                                              _selectedTimeIndexNotifier!.value = -1;
                                            },
                                            children: {
                                              0 : Text('AM', style: TextStyle(color: value == 0 ? Colors.white : Colors.black38)),
                                              1 : Text('PM', style: TextStyle(color: value == 1 ? Colors.white : Colors.black38))
                                            }
                                          );
                                        }
                                      )
                                    )
                                  ]
                                ),
                                const SizedBox(height: 16.0),
                                ValueListenableBuilder<int>(
                                  valueListenable: _slidingNotifier!,
                                  builder: (ctx, value, child){
                                    final queryData = _getQuery(sliderIndex: value);
                                    return GridView.builder(
                                      shrinkWrap: true,
                                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4, mainAxisSpacing: 26.0, crossAxisSpacing: 16.0,
                                        childAspectRatio: 2.0
                                      ),
                                      itemBuilder: (ctx, index){
                                        return GestureDetector(
                                          onTap: (){
                                            _selectedTimeIndexNotifier!.value = index;
                                          },
                                          child: ValueListenableBuilder<int>(
                                            valueListenable: _selectedTimeIndexNotifier!,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.circular(10.0),
                                                border: Border.all(color: Colors.black12)
                                              ),
                                              child: Center(
                                                child: Text(queryData.elementAt(index))
                                              )
                                            ),
                                            builder: (ctx, value, child){
                                              if(value == index){
                                                return Container(
                                                  decoration: BoxDecoration(
                                                    color: CustomMaterialColor.primary,
                                                    borderRadius: BorderRadius.circular(10.0)
                                                  ),
                                                  child: Center(
                                                    child: Text(queryData.elementAt(index), style: const TextStyle(color: Colors.white))
                                                  )
                                                );
                                              }else{
                                                return child!;
                                              }
                                            }
                                          )
                                        );
                                      },
                                      itemCount: queryData.length
                                    );
                                  }
                                )
                              ]
                            )
                          );
                          _bottomSheetBuilder?.show(context, content, bottom: CustomFillButton(
                            label: 'Confirm',
                            onPressed: (){
                              final queryData = _getQuery(sliderIndex: _slidingNotifier!.value);
                              final selectedTimeIndex = _selectedTimeIndexNotifier!.value;
                              Navigator.of(context, rootNavigator: true).pop();
                              if(selectedTimeIndex > -1){
                                _selectedTimeValueNotifier!.value = queryData.elementAt(selectedTimeIndex);
                              }
                            }
                          ));
                        },
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black12),
                            borderRadius: BorderRadius.circular(10.0)
                          ),
                          child: ValueListenableBuilder<String?>(
                            valueListenable: _selectedTimeValueNotifier!,
                            child: const Text('---', maxLines: 1, overflow: TextOverflow.ellipsis),
                            builder: (ctx, value, child){
                              if(value != null){
                                return Text(value);
                              }else{
                                return child!;
                              }
                            }
                          ),
                        )
                      )
                    )
                  ]
                ).build(context)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              sliver: SliverToBoxAdapter(
                child: ColumnComposite(
                  iComponent: [
                    Component(
                      child: const Text('Reason', style: TextStyle(color: TextColor.secondaryColor)),
                    ),
                    Component(
                      child: SizedBox(
                        height: 200.0,
                        child: CustomTextField(
                          expands: true, maxLines: null,
                          controller: _reasonController
                        )
                      )
                    )
                  ]
                ).build(context)
              )
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              sliver: SliverToBoxAdapter(
                child: CustomFillButton(
                  label: 'Submit',
                  onPressed: () async{
                    showLoading(context);
                    await Future.delayed(const Duration(seconds: 1), () => Navigator.of(context, rootNavigator: true).pop());
                    Navigator.of(context, rootNavigator: true).push(
                      PageRouteBuilder(
                        pageBuilder: (ctx, anim1, anim2){
                          return const SubmitScreen(
                            title: 'Request Successful',
                            subTitle: 'Your request has been sent.',
                            statusCode: 200
                          );
                        },
                        transitionsBuilder: (ctx, anim1, anim2, child){
                          return FadeTransition(
                            opacity: anim1,
                            child: child
                          );
                        }
                      )
                    );
                  }
                )
              )
            )
          ]
        )
      )
    );
  }
}

class CustomCalendar extends StatefulWidget {
  final DateTime? currentValue;
  final ValueChanged<DateTime> onDateSelect;
  const CustomCalendar({Key? key, required this.onDateSelect, this.currentValue}) : super(key: key);

  @override
  _CustomCalendarState createState() => _CustomCalendarState();
}

class _CustomCalendarState extends State<CustomCalendar> {

  final DateTime _todayDate = DateTime.now();

  FactoryCustomDayCreator? _creator;
  ValueNotifier<DateTime?>? _selectedDateTimeNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _creator = const FactoryCustomDayCreator();
    _selectedDateTimeNotifier = ValueNotifier<DateTime?>(widget.currentValue);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _selectedDateTimeNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: List.generate(DateTime.monthsPerYear, (month){
          final date = DateTime(DateTime.now().year, month + 1, DateTime.monday);
          final lastMonday = date.subtract(Duration(days: date.weekday - DateTime.monday));
          final inDaysLastMonday = date.difference(lastMonday).inDays + 1;
          final lastSunday = date.subtract(Duration(days: inDaysLastMonday));
          final inDays = date.difference(lastSunday).inDays;
          final titleHeader = DateFormat('MMMM yyyy').format(date);
          return Column(
            children: [
              const SizedBox(height: 16.0),
              Text(titleHeader, style: const TextStyle(
                color: CustomMaterialColor.primary, fontSize: 17.0, fontWeight: FontWeight.bold
              )),
              SizedBox(
                height: 40,
                child: GridView(
                  shrinkWrap: true, physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: DateTime.daysPerWeek
                  ),
                  children: [
                    for(int i = -inDays; i < DateTime.daysPerWeek - inDays; i++)
                      Center(
                        child: Text(DateFormat('EEE').format(date.add(Duration(days: i))), style: const TextStyle(
                          color: TextColor.secondaryColor
                        ))
                      )
                  ]
                )
              ),
              GridView(
                shrinkWrap: true, physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: DateTime.daysPerWeek
                ),
                children: [
                  for(int i = -inDays; date.month == 1 ? i < 42 - inDays : i < 35 - inDays; i++)
                    Center(
                      child: IconButton(
                        onPressed: (){
                          _selectedDateTimeNotifier!.value = date.add(Duration(days: i));
                          widget.onDateSelect(_selectedDateTimeNotifier!.value!);
                        },
                        icon: Builder(
                          builder: (ctx){
                            final fullDate = date.add(Duration(days: i));
                            final day = int.tryParse(DateFormat('d').format(fullDate));
                            final dayString = DateFormat.EEEE().format(fullDate);
                            final month = int.tryParse(DateFormat('M').format(fullDate));
                            if(month! < date.month || dayString == 'Sunday' || i < 0){
                              return Text('$day', style: const TextStyle(color: Colors.black38));
                            }
                            else if(day == _todayDate.day
                                && month == _todayDate.month
                                && date.month == _todayDate.month){
                              return _creator!.factoryMethod(type: CustomDayType.today, day: day!)!.build(ctx);
                            }else{
                              return ValueListenableBuilder<DateTime?>(
                                valueListenable: _selectedDateTimeNotifier!,
                                child: Text('$day'),
                                builder: (ctx, value, child){
                                  if(value == fullDate && value!.month == date.month){
                                    return Container(
                                      width: 35, height: 35,
                                      decoration: BoxDecoration(
                                        color: CustomMaterialColor.primary,
                                        borderRadius: BorderRadius.circular(10.0)
                                      ),
                                      child: Center(
                                        child: Text('$day', style: const TextStyle(color: Colors.white))
                                      )
                                    );
                                  }else{
                                    return child!;
                                  }
                                }
                              );
                            }
                          }
                      )
                    )
                  )
                ]
              )
            ]
          );
        })
      )
    );
  }
}


// abstract class FactoryTime{
//   Widget build(final BuildContext context, Function(String? value) onSelected);
// }
//
// class FactoryMorningTime extends FactoryTime{
//
//   final _initData = const [
//     '7:00', '7:15', '7:30'
//   ];
//
//   final ValueNotifier<int> _selectedIndexNotifier = ValueNotifier<int>(-1);
//
//   @override
//   Widget build(BuildContext context, Function(String? value) onSelected) {
//     // TODO: implement build
//     return GridView.builder(
//       shrinkWrap: true,
//       gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//         crossAxisCount: 4, mainAxisSpacing: 26.0, crossAxisSpacing: 16.0,
//         childAspectRatio: 2.0
//       ),
//       itemBuilder: (ctx, index){
//         return GestureDetector(
//           onTap: (){
//             onSelected.call(_initData.elementAt(index));
//             if(_selectedIndexNotifier.value == index){
//               _selectedIndexNotifier.value = -1;
//               return;
//             }
//             _selectedIndexNotifier.value = index;
//           },
//           child: ValueListenableBuilder<int>(
//             valueListenable: _selectedIndexNotifier,
//             builder: (ctx, value, child){
//               return Container(
//                 decoration: BoxDecoration(
//                   color: Colors.white,
//                   borderRadius: BorderRadius.circular(10.0),
//                   border: Border.all(color: index == value ? CustomMaterialColor.primary : Colors.black12)
//                 ),
//                 child: Center(
//                   child: Text('${_initData.elementAt(index)}\tAM')
//                 )
//               );
//             }
//           )
//         );
//       },
//       itemCount: _initData.length
//     );
//   }
// }
//
// class FactoryEveningTime extends FactoryTime{
//
//   final _initData = const [
//     '1:30', '2:00', '2:15'
//   ];
//
//   @override
//   Widget build(BuildContext context, Function(String? value) onSelected) {
//     // TODO: implement build
//     return GridView.builder(
//       shrinkWrap: true,
//       gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//         crossAxisCount: 4, mainAxisSpacing: 26.0, crossAxisSpacing: 16.0,
//         childAspectRatio: 2.0
//       ),
//       itemBuilder: (ctx, index){
//         return GestureDetector(
//           onTap: (){
//             onSelected.call(_initData.elementAt(index));
//           },
//           child: Container(
//             decoration: BoxDecoration(
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(10.0),
//               border: Border.all(color: Colors.black12)
//             ),
//             child: Center(
//               child: Text('${_initData.elementAt(index)}\tPM')
//             )
//           )
//         );
//       },
//       itemCount: _initData.length
//     );
//   }
// }
//
// class FactoryTimeCreator{
//   const FactoryTimeCreator();
//
//   FactoryTime factoryMethod(final String type){
//     if(type == 'pm'){
//       return FactoryEveningTime();
//     }
//     return FactoryMorningTime();
//   }
// }
