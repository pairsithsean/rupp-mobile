import 'package:flutter/material.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

void showLoading(final BuildContext context) async{
  showDialog(
    context: context,
    builder: (ctx){
      return LoadingWidget.instance;
    }
  );
}