import 'package:flutter/material.dart';

abstract class CustomNavigator{
  static void push(final BuildContext context, final Widget to){
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (_, anim1, anim2){
          return to;
        },
        transitionsBuilder: (_, anim1, anim2, child){
          return FadeTransition(
            opacity: anim1,
            child: child
          );
        }
      )
    );
  }
}