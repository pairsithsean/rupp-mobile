import 'package:flutter/material.dart';

class DraggableBottomSheetModel{
  double minChildSize;
  double maxChildSize;
  double initialChildSize;

  DraggableBottomSheetModel({this.maxChildSize = .95, this.minChildSize = .5,
    this.initialChildSize = .5});
}

class DraggableBottomSheetBuilder{
  DraggableBottomSheetModel? sheet;

  DraggableBottomSheetBuilder(){
    sheet = DraggableBottomSheetModel();
  }

  DraggableBottomSheetBuilder setInitialChildSize(double initialChildSize){
    sheet!.initialChildSize = initialChildSize;
    return this;
  }

  DraggableBottomSheetBuilder setMinChildSize(double midChildSize){
    sheet!.minChildSize = midChildSize;
    return this;
  }

  // DraggableBottomSheetBuilder build(){
  //   return this;
  // }

  void show(final BuildContext context, final Widget content, {final Widget? bottom}){
    showModalBottomSheet(
      useRootNavigator: true,
      context: context, isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)
        )
      ),
      builder: (ctx){
        return SafeArea(
          bottom: false,
          child: DraggableScrollableSheet(
            snap: true,
            expand: false, minChildSize: sheet!.minChildSize, maxChildSize: sheet!.maxChildSize,
            initialChildSize: sheet!.initialChildSize,
            builder: (ctx, controller){
              return Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      controller: controller,
                      child: Column(
                        children: [
                          const SizedBox(height: 16.0),
                          Center(
                            child: Container(
                              width: 50, height: 5,
                              decoration: BoxDecoration(
                                color: const Color(0xFF8F90A6),
                                borderRadius: BorderRadius.circular(50.0)
                              )
                            )
                          ),
                          content
                        ]
                      )
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: bottom ?? const SizedBox()
                  )
                ]
              );
            }
          )
        );
      }
    );
  }
}


// void showCustomModalBottomSheet({required final BuildContext context,
//   required final Widget content}){
//   showModalBottomSheet(
//     context: context, isScrollControlled: true,
//     shape: const RoundedRectangleBorder(
//       borderRadius: BorderRadius.only(
//         topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)
//       )
//     ),
//     builder: (ctx){
//       return DraggableScrollableSheet(
//         expand: false, minChildSize: .5, maxChildSize: .95, initialChildSize: .5,
//         builder: (ctx, controller){
//           return SingleChildScrollView(
//             controller: controller,
//             child: Column(
//               children: [
//                 const SizedBox(height: 16.0),
//                 Center(
//                   child: Container(
//                     width: 50, height: 5,
//                     decoration: BoxDecoration(
//                       color: const Color(0xFF8F90A6),
//                       borderRadius: BorderRadius.circular(50.0)
//                     )
//                   )
//                 ),
//                 const SizedBox(height: 56.0),
//                 content
//               ]
//             )
//           );
//         }
//       );
//     }
//   );
// }