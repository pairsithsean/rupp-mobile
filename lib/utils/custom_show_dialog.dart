import 'package:flutter/material.dart';

void showCustomDialog(final BuildContext context, {final String title = ''}){
  showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: '',
      pageBuilder: (ctx, anim1, anim2){
        return Dialog(
          elevation: 0.0,
          insetAnimationCurve: Curves.ease,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: SizedBox(
            height: 200.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Flexible(
                  child: CircleAvatar(
                    radius: 25.0,
                    child: Icon(Icons.check_rounded),
                    backgroundColor: Color(0xFF10CF7C),
                    foregroundColor: Colors.white
                  )
                ),
                const SizedBox(height: 16.0),
                Flexible(
                  child: Text(title, style: const TextStyle(fontWeight: FontWeight.bold))
                )
              ]
            )
          )
        );
      },
      transitionBuilder: (ctx, anim1, anim2, child){
        return FadeTransition(
          opacity: anim1,
          child: child
        );
      }
  );
}