import 'package:flutter/material.dart';

import 'avatar_widget.dart';

class ProfileWidget extends StatelessWidget {
  final String name;
  final Widget subTitle;
  const ProfileWidget({Key? key, required this.name, required this.subTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        children: [
          const AvatarWidget(url: 'https://images.pexels.com/photos/10718776/pexels-photo-10718776.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
          const SizedBox(width: 16.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(name, style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600)),
              const SizedBox(height: 80.0 / 4.0),
              subTitle
            ]
          )
        ]
      )
    );
  }
}
