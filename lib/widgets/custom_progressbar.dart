import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/text_color.dart';

class CustomProgressBar extends StatelessWidget {
  final DurationState state;
  final ValueChanged<Duration>? onSeek;
  final VoidCallback? onDragEnd;
  const CustomProgressBar({Key? key, required this.state, this.onSeek, this.onDragEnd}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProgressBar(
      progress: state.progress,
      buffered: state.buffered,
      total: state.total,
      timeLabelLocation: TimeLabelLocation.below,
      baseBarColor: const Color(0xFFC7C9D9),
      timeLabelTextStyle: const TextStyle(color: Colors.white),
      timeLabelPadding: 8.0,
      onSeek: onSeek,
      onDragEnd: onDragEnd
    );
  }
}

class CustomAudioProgressBar extends StatelessWidget {
  final DurationState state;
  final ValueChanged<Duration>? onSeek;
  final VoidCallback? onDragEnd;
  final ValueChanged<ThumbDragDetails>? onDragUpdate;
  const CustomAudioProgressBar({Key? key, required this.state, this.onSeek, this.onDragEnd, this.onDragUpdate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProgressBar(
      progress: state.progress,
      buffered: state.buffered,
      total: state.total,
      timeLabelLocation: TimeLabelLocation.below,
      baseBarColor: const Color(0xFFC7C9D9),
      timeLabelTextStyle: const TextStyle(color: TextColor.secondaryColor, fontSize: 12.0),
      timeLabelPadding: 8.0,
      onSeek: onSeek,
      thumbRadius: 6.0,
      onDragEnd: onDragEnd,
      onDragUpdate: onDragUpdate
    );
  }
}

class DurationState {
  final Duration progress;
  final Duration buffered;
  final Duration total;
  const DurationState({this.progress = Duration.zero,
    this.buffered = Duration.zero, this.total = Duration.zero});
}
