import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/state_model.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'form_error_message.dart';

class CustomForm extends StatefulWidget {
  final StateContext state;
  final List<Widget> items;
  final String errorMessage;
  const CustomForm({Key? key, required this.items, required this.state, this.errorMessage = ''}) : super(key: key);

  @override
  _CustomFormState createState() => _CustomFormState();
}

class _CustomFormState extends State<CustomForm> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: widget.state.state != null ? widget.state.state is ErrorState ? true : false : false,
              child: FormErrorMessage(
                message: widget.errorMessage
              )
            ),
            const SizedBox(height: 8.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.items
            )
          ]
        )
      )
    );
  }
}

class FormItem extends StatelessWidget{
  final String label;
  final Widget child;
  const FormItem({Key? key, required this.label, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if(label.isEmpty){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, style: const TextStyle(color: TextColor.secondaryColor)),
          child
        ]
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label, style: const TextStyle(color: TextColor.secondaryColor)),
        const SizedBox(height: 8.0),
        child
      ]
    );
  }
}
