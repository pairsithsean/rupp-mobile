import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/text_color.dart';

class CustomFilterChip extends StatelessWidget {
  final String title;
  final VoidCallback onDeleted;
  const CustomFilterChip({Key? key, required this.title, required this.onDeleted}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Chip(
      side: const BorderSide(color: Color(0xFF6B7588)),
      backgroundColor: const Color(0xFFEBEBF0),
      deleteIcon: const Icon(CupertinoIcons.clear_circled, size: 18.0,
        color: Color(0xFF6B7588), semanticLabel: 'clear_circled'),
      onDeleted: onDeleted,
      label: Text(title, style: const TextStyle(color: TextColor.secondaryColor))
    );
  }
}
