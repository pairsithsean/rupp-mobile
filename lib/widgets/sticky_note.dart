import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/sticky_note_model.dart';

import 'empty_widget.dart';

class StickyNote extends StatelessWidget {
  final StickyNoteData data;
  final Color? backgroundColor;
  final Color? textColor;
  const StickyNote({Key? key, required this.data, this.textColor = Colors.white,
    this.backgroundColor = Colors.transparent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return data.hasData() ? Padding(
      padding: const EdgeInsets.only(top: 16.0, right: 26.0),
      child: Container(
        decoration: BoxDecoration(
          color: backgroundColor
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Flexible(
                child: Text(data.time, maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: textColor))
              ),
              Flexible(
                flex: 2,
                child: Text(data.subject, maxLines: 2, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold, color: textColor))
              ),
              Flexible(
                child: Text(data.lecturer, maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: textColor))
              )
            ]
          )
        )
      )
    ) : EmptyWidget.instance;
  }
}
