import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/matrial_color.dart';

class FormErrorMessage extends StatelessWidget {
  final String? message;
  const FormErrorMessage({Key? key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          const Icon(Icons.warning_amber_outlined, color: CustomMaterialColor.primary),
          const SizedBox(width: 8.0),
          Flexible(
            child: Text('$message', style: const TextStyle(color: CustomMaterialColor.primary))
          )
        ]
      )
    );
  }
}
