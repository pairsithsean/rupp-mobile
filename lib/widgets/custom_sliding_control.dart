import 'package:flutter/cupertino.dart';
import 'package:rupp_client_app/style/matrial_color.dart';

class CustomSlidingControl extends StatefulWidget {
  final Map<int, Widget> children;
  final int groupValue;
  final ValueChanged<int?> onValueChanged;
  const CustomSlidingControl({Key? key, required this.children,
    required this.groupValue, required this.onValueChanged}) : super(key: key);

  @override
  _CustomSlidingControlState createState() => _CustomSlidingControlState();
}

class _CustomSlidingControlState extends State<CustomSlidingControl> {

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: CupertinoSlidingSegmentedControl<int>(
        groupValue: widget.groupValue,
        thumbColor: CustomMaterialColor.primary,
        onValueChanged: widget.onValueChanged,
        children: widget.children
      )
    );
  }
}
