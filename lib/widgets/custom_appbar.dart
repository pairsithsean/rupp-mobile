import 'package:flutter/material.dart';

abstract class ICustomAppbar{
  final String? title;
  const ICustomAppbar({this.title});
  Widget build(final BuildContext context);
}

class CustomAppbar extends ICustomAppbar{

  CustomAppbar({final String? title}) : super(title: title);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AppBar(
      title: Text(super.title!), elevation: 1.0,
      centerTitle: false
    );
  }

}

class CustomSliverAppbar extends StatelessWidget {
  final String title;
  const CustomSliverAppbar({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      title: Text(title), elevation: 1.0,
      centerTitle: false, pinned: false, floating: true
    );
  }
}
