import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

typedef OnSelected = void Function(int index);
class CustomDropdown extends StatefulWidget {
  final double? position;
  final List<String> items;
  final OnSelected onSelected;
  const CustomDropdown({Key? key, required this.items, required this.onSelected, this.position = 0.0}) : super(key: key);

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {

  GlobalKey<State>? _dropDownKey;
  OverlayEntry? _entry;

  ValueNotifier<String>? _currentItemNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dropDownKey = GlobalKey<State>();
    _currentItemNotifier = ValueNotifier<String>(widget.items.first);
  }

  @override
  void didUpdateWidget(covariant CustomDropdown oldWidget) {
    // TODO: implement didUpdateWidget
    if(oldWidget != widget){
      _currentItemNotifier!.value = widget.items.first;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _dropDownKey!.currentState?.dispose();
    _currentItemNotifier?.dispose();
    _entry?.remove();
    _entry = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Stack(
        children: [
          GestureDetector(
            onTap: () async{
              final currentContext = _dropDownKey!.currentContext;
              final renderObj = currentContext?.findRenderObject();
              final viewport = RenderAbstractViewport.of(renderObj);
              final top = viewport?.getOffsetToReveal(renderObj!, 0.0);
              final topPosition = (top!.offset + top.rect.height + 4.0) + widget.position!;
              if(_entry != null){
                _entry?.remove();
                _entry = null;
                return;
              }
              _entry = OverlayEntry(
                opaque: false,
                builder: (ctx){
                  return Positioned(
                    top: 0.0, left: 0.0, right: 0.0, bottom: 0.0,
                    child: GestureDetector(
                      onTap: (){
                        if(_entry != null){
                          _entry?.remove();
                          _entry = null;
                          return;
                        }
                      },
                      child: Material(
                        color: Colors.black.withAlpha(12),
                        child: Stack(
                          children: [
                            Positioned(
                              top: topPosition, left: 16.0, right: 16.0,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(color: Colors.black12)
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: widget.items.map((e) {
                                        final index = widget.items.indexWhere((element) => element == e);
                                        return Material(
                                          type: MaterialType.transparency,
                                          child: InkWell(
                                            onTap: (){
                                              widget.onSelected.call(index);
                                              //
                                              if(_entry != null){
                                                _entry?.remove();
                                                _entry = null;
                                                _currentItemNotifier!.value = e;
                                                return;
                                              }
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 12.0),
                                                  SizedBox(
                                                    width: double.infinity,
                                                    child: Text(e)
                                                  ),
                                                  //index == widget.items.length - 1 ? const SizedBox() : const Divider()
                                                ]
                                              )
                                            )
                                          )
                                        );
                                      }).toList()
                                    )
                                  )
                                )
                              )
                            )
                          ]
                        )
                      )
                    )
                  );
                }
              );
              Overlay.of(context)?.insert(_entry!);
            },
            child: Container(
              key: _dropDownKey!,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: Colors.black12)
              ),
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Expanded(
                    child: ValueListenableBuilder<String>(
                      valueListenable: _currentItemNotifier!,
                      builder: (ctx, value, child){
                        return Text(value);
                      }
                    )
                  ),
                  const Icon(Icons.keyboard_arrow_down_outlined)
                ]
              )
            )
          )
        ]
      )
    );
  }
}
