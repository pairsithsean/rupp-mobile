import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:rupp_client_app/style/matrial_color.dart';

class CustomFlutterSwitch extends StatefulWidget {
  final String title;
  final bool? initialValue;
  final ValueChanged<bool> onToggle;
  const CustomFlutterSwitch({Key? key, required this.title, required this.onToggle, this.initialValue = false}) : super(key: key);

  @override
  _CustomFlutterSwitchState createState() => _CustomFlutterSwitchState();
}

class _CustomFlutterSwitchState extends State<CustomFlutterSwitch> {

  ValueNotifier<bool>? _isToggledNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isToggledNotifier = ValueNotifier(widget.initialValue!);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _isToggledNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        children: [
          Expanded(
            child: Text(widget.title)
          ),
          Flexible(
            flex: 0,
            child: ValueListenableBuilder<bool>(
              valueListenable: _isToggledNotifier!,
              builder: (ctx, value, child){
                return SizedBox(
                  width: 60.0, height: 25.0,
                  child: FlutterSwitch(
                    showOnOff: true, toggleSize: 18.0,
                    activeColor: CustomMaterialColor.primary,
                    valueFontSize: 12.0,
                    activeTextFontWeight: FontWeight.normal,
                    inactiveTextFontWeight: FontWeight.normal,
                    activeTextColor: Colors.white, inactiveTextColor: Colors.white,
                    activeText: 'ON', inactiveText: 'OFF',
                    inactiveColor: const Color(0xFFE2E2E5),
                    value: value,
                    onToggle: (bool value) {
                      _isToggledNotifier!.value = value;
                      widget.onToggle(value);
                    }
                  )
                );
              }
            )
          )
        ]
      )
    );
  }
}
