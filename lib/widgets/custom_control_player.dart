import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/custom_icon.dart';

class ControlPlayerWidget extends StatelessWidget {
  final bool isPlaying;
  final VoidCallback onPressed;
  final Color? iconColor;
  final VoidCallback? onSkipPreviousPressed;
  final VoidCallback? onSkipForwardPressed;
  final VoidCallback? onFastPreviousPressed;
  final VoidCallback? onFastForwardPressed;
  const ControlPlayerWidget({Key? key, required this.isPlaying, required this.onPressed,
    this.onFastForwardPressed, this.onFastPreviousPressed, this.onSkipForwardPressed, this.onSkipPreviousPressed,
    this.iconColor = const Color(0xFF6B7588)}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(CustomIcon.skipPrevious, color: iconColor),
          onPressed: onSkipPreviousPressed
        ),
        IconButton(
          icon: Icon(CustomIcon.fastPrevious, color: iconColor),
          onPressed: onFastPreviousPressed
        ),
        const SizedBox(width: 16.0),
        FloatingActionButton(
          elevation: 0.0,
          child: isPlaying ? const Icon(Icons.pause)
            : const Icon(Icons.play_arrow),
          onPressed: onPressed
        ),
        const SizedBox(width: 16.0),
        IconButton(
          icon: Icon(CustomIcon.fastForward, color: iconColor),
          onPressed: onFastForwardPressed
        ),
        IconButton(
          icon: Icon(CustomIcon.skipForward, color: iconColor),
          onPressed: onSkipForwardPressed
        )
      ]
    );
  }
}