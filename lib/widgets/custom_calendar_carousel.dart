import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';

enum CustomDayType{
  today, exam, holiday, startEndSemester
}

class CustomCalendarCarousel extends StatelessWidget {
  final double height;
  const CustomCalendarCarousel({Key? key, this.height = 0.0}) : super(key: key);

  final FactoryCustomDayCreator _creator = const FactoryCustomDayCreator();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: CalendarCarousel(
        height: height,
        isScrollable: false,
        headerTextStyle: const TextStyle(color: CustomMaterialColor.primary, fontSize: 20, fontWeight: FontWeight.bold),
        iconColor: CustomMaterialColor.primary,
        weekendTextStyle: const TextStyle(color: TextColor.secondaryColor),
        weekDayFormat: WeekdayFormat.short,
        weekdayTextStyle: const TextStyle(color: Color(0xFF6B7588)),
        todayButtonColor: Colors.transparent,
        todayBorderColor: Colors.transparent,
        customDayBuilder: (bool isSelectable, int index, bool isSelectedDay, bool isToday,
            bool isPrevMonthDay,
            TextStyle textStyle,
            bool isNextMonthDay,
            bool isThisMonthDay,
            DateTime day){
          if(isToday){
            return _creator.factoryMethod(type: CustomDayType.today, day: day.day)?.build(context);
          }else if(day.day == 1){
            return _creator.factoryMethod(type: CustomDayType.startEndSemester, day: day.day)?.build(context);
          }else if(day.day == 16 || day.day == 21){
            return _creator.factoryMethod(type: CustomDayType.exam, day: day.day)?.build(context);
          }else if(day.day == 14){
            return _creator.factoryMethod(type: CustomDayType.holiday, day: day.day)?.build(context);
          }
          else{
            return null;
          }
        }
      )
    );
  }
}

abstract class FactoryCustomDayView{
  final int day;
  const FactoryCustomDayView({this.day = 0});
  Widget build(final BuildContext context);
}

class FactoryCustomTodayView extends FactoryCustomDayView{
  const FactoryCustomTodayView({final int day = 0}) : super(day: day);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        width: 35, height: 35,
        decoration: BoxDecoration(
          color: const Color(0xFFFFCC00),
          borderRadius: BorderRadius.circular(10.0)
        ),
        child: Center(
          child: Text('$day')
        )
      )
    );
  }
}

class FactoryCustomStartEndView extends FactoryCustomDayView{
  const FactoryCustomStartEndView({final int day = 0}) : super(day: day);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        width: 35, height: 35,
        decoration: BoxDecoration(
          border: Border.all(color: const Color(0xFF10CF7C)),
          borderRadius: BorderRadius.circular(10.0)
        ),
        child: Center(
          child: Text('$day')
        )
      )
    );
  }
}

class FactoryCustomExamDayView extends FactoryCustomDayView{
  const FactoryCustomExamDayView({final int day = 0}) : super(day: day);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Text('$day'),
          Positioned(
            bottom: -8,
            child: Container(
              width: 5, height: 5,
              decoration: const BoxDecoration(
                color: Color(0xFF2278F8),
                shape: BoxShape.circle
              )
            )
          )
        ]
      )
    );
  }
}

class FactoryCustomHolidayDayView extends FactoryCustomDayView{
  const FactoryCustomHolidayDayView({final int day = 0}) : super(day: day);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Text('$day'),
          Positioned(
            bottom: -8,
            child: Container(
              width: 5, height: 5,
              decoration: const BoxDecoration(
                color: Color(0xFFEE3436),
                shape: BoxShape.circle
              )
            )
          )
        ]
      )
    );
  }
}

class FactoryCustomDayCreator{
  const FactoryCustomDayCreator();
  FactoryCustomDayView? factoryMethod({required final CustomDayType type, required final int day}){
    switch(type){
      case CustomDayType.today: return FactoryCustomTodayView(day: day);
      case CustomDayType.exam: return FactoryCustomExamDayView(day: day);
      case CustomDayType.holiday: return FactoryCustomHolidayDayView(day: day);
      case CustomDayType.startEndSemester: return FactoryCustomStartEndView(day: day);
      default: return null;
    }
  }
}
