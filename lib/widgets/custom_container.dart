import 'package:flutter/material.dart';

abstract class CustomContainer extends StatelessWidget {
  final double? width;
  final double? height;
  final Widget? child;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  const CustomContainer({Key? key, this.child, this.width = double.infinity,
    this.height = double.infinity, this.margin = EdgeInsets.zero , this.padding = EdgeInsets.zero}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width, height: height,
      margin: margin,
      child: child!
    );
  }
}

class ContainerRounded extends CustomContainer{
  const ContainerRounded({Key? key, final Widget? child, final EdgeInsets? margin, final EdgeInsets? padding,
    final double? width, final double? height}) : super(key: key, child: child, margin: margin, width: width, height: height, padding: padding);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Container(
        width: width, height: height,
        margin: margin,
        padding: padding,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: Colors.black.withAlpha(12), blurRadius: 24.0)
          ]
        ),
        child: child!
      )
    );
  }
}
