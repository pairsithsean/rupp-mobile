import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_client_app/model/filter_model.dart';
import 'package:rupp_client_app/model/view/filter_view.dart';
import 'package:rupp_client_app/widgets/fliter_chip.dart';

class FilterResultWidget extends StatefulWidget {
  final VoidCallback onTrailingTap;
  final ValueChanged<FilterData> onDeleted;
  final ValueChanged<FilterData> onResultData;
  const FilterResultWidget({Key? key, required this.onTrailingTap,
    required this.onDeleted, required this.onResultData}) : super(key: key);

  @override
  _FilterResultWidgetState createState() => _FilterResultWidgetState();
}

class _FilterResultWidgetState extends State<FilterResultWidget> {

  ValueNotifier<bool>? _isItemDeletedNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isItemDeletedNotifier = ValueNotifier<bool>(false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _isItemDeletedNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ValueListenableBuilder<bool>(
              valueListenable: _isItemDeletedNotifier!,
              builder: (ctx, value, child){
                return Wrap(
                  children: selectedFilterItem.map((e) {
                    widget.onResultData(e);
                    final index = selectedFilterItem.indexWhere((element) => element.data == e.data);
                    return Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: CustomFilterChip(
                        onDeleted: (){
                          widget.onDeleted(e);
                        },
                        title: e.data != null ? e.data!.title : e.label
                      )
                    );
                  }).toList()
                );
              }
            )
          ),
          GestureDetector(
            child: SvgPicture.asset('assets/svg/filter.svg', width: 18.0, height: 18.0),
            onTap: widget.onTrailingTap
          )
        ]
      )
    );
  }
}
