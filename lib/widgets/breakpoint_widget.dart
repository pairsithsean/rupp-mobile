import 'package:flutter/material.dart';

class BreakpointWidget extends StatelessWidget {
  //const BreakpointWidget({Key? key}) : super(key: key);

  const BreakpointWidget._();

  static BreakpointWidget? _breakpointWidget;

  static BreakpointWidget get instance{
    if(_breakpointWidget == null) {
      return _breakpointWidget = const BreakpointWidget._();
    } else{
      return _breakpointWidget!;
    }
  }
  
  static const Color color = Color(0xFFEE3436);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topLeft,
      children: [
        Container(
          width: 10.0, height: 10.0,
          decoration: const BoxDecoration(
            color: color,
            shape: BoxShape.circle
          )
        ),
        Positioned(
          top: 5.0, left: 0.0,
          child: Container(
            width: (200 * 7), height: 1.0,
            color: color.withAlpha(50)
          )
        )
      ]
    );
  }
}
