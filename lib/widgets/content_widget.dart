import 'package:flutter/material.dart';
import 'custom_fill_button.dart';

class ContentWidget extends StatelessWidget {
  final String title;
  final String subTitle;
  final Widget body;
  final Widget bottom;
  const ContentWidget({Key? key, required this.title, required this.subTitle,
    required this.body, required this.bottom}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
          const SizedBox(height: 26.0),
          Text(subTitle, style: const TextStyle(color: Color(0xFF6B7588))),
          const SizedBox(height: 56.0),
          body,
          const SizedBox(height: 16.0),
          bottom
        ]
      )
    );
  }
}
