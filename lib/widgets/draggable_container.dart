import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/custom_calendar_carousel.dart';

import 'avatar_widget.dart';

class DraggableContainer extends StatefulWidget {
  final Widget child;
  const DraggableContainer({Key? key, required this.child}) : super(key: key);

  @override
  _DraggableContainerState createState() => _DraggableContainerState();
}

class _DraggableContainerState extends State<DraggableContainer> {

  StreamController<double>? _dragController;
  double _dragValue = 0.0;
  static const double dragMinValue = 330.0;
  static const double dragMaxValue = 600.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dragController = StreamController<double>.broadcast();
    _dragValue = dragMinValue;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _dragController?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomMaterialColor.backgroundColor,
      child: Stack(
        children: [
          Positioned(
            top: dragMinValue, left: 0, right: 0, bottom: 0,
            child: widget.child
          ),
          StreamBuilder<double>(
            stream: _dragController!.stream, initialData: dragMinValue,
            builder: (ctx, snapshot){
              final fadeOut = ((dragMaxValue - snapshot.data!) / dragMaxValue);
              final fadeIn = (snapshot.data! / dragMaxValue);
              return Positioned(
                top: 0, left: 0, right: 0,
                child: Container(
                  height: snapshot.data!,
                  padding: const EdgeInsets.only(top: 56.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0)
                    ),
                    boxShadow: [
                      BoxShadow(color: Colors.black.withAlpha(50), blurRadius: 24.0)
                    ]
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: SizedBox(
                                width: 38.0, height: 38.0,
                                child: Image.asset('assets/png/LOGO-min.png')
                              )
                            ),
                            const Flexible(
                              child: SizedBox(
                                width: 50.0, height: 50.0,
                                child: AvatarWidget(url: 'https://images.pexels.com/photos/10718776/pexels-photo-10718776.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500')
                              )
                            )
                          ]
                        )
                      ),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text('Name', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600)),
                                  Text('October 2021', style: TextStyle(fontSize: 12.0))
                                ]
                              )
                            ),
                            // Flexible(
                            //   flex: 0,
                            //   child: snapshot.data! > dragMinValue ? EmptyWidget.instance : Visibility(
                            //     visible: snapshot.data! > dragMinValue ? false : true,
                            //     child: SingleChildScrollView(
                            //       scrollDirection: Axis.horizontal,
                            //       child: Row(
                            //         mainAxisAlignment: MainAxisAlignment.center,
                            //         children: List.generate(5, (index) {
                            //           return Container(
                            //              width: 65.0, height: 85.0,
                            //               margin: const EdgeInsets.all(12.0),
                            //               decoration: BoxDecoration(
                            //                 color: Colors.transparent,
                            //                 border: Border.all(color: Colors.black.withAlpha(12), width: 2.5),
                            //                 borderRadius: BorderRadius.circular(10.0)
                            //               ),
                            //               child: Padding(
                            //                 padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            //                 child: Column(
                            //                   mainAxisAlignment: MainAxisAlignment.center,
                            //                   children: [
                            //                     Text('27', maxLines: 1, overflow: TextOverflow.ellipsis,
                            //                         style: GoogleFonts.publicSans(fontSize: 20.0, color: TextColor.secondaryColor, fontWeight: FontWeight.bold)),
                            //                     const SizedBox(height: 8.0),
                            //                     Text('Wed', maxLines: 1, overflow: TextOverflow.ellipsis, style: GoogleFonts.publicSans(color: TextColor.secondaryColor))
                            //                   ]
                            //                 )
                            //               )
                            //           );
                            //         }).toList()
                            //       )
                            //     )
                            //   )
                            // ),
                            // Flexible(
                            //   child: Visibility(
                            //     visible: snapshot.data! > dragMinValue ? true : false,
                            //     child: CustomCalendarCarousel(
                            //       height: snapshot.data!
                            //     )
                            //   )
                            // )
                            Flexible(
                              child: Stack(
                                children: [
                                  Opacity(
                                    opacity: snapshot.data! == dragMinValue ? 1.0 : fadeOut,
                                    child: IgnorePointer(
                                      ignoring: fadeOut == 0.0 ? true : false,
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: List.generate(5, (index) {
                                            return Container(
                                              width: 65.0, height: 85.0,
                                              margin: const EdgeInsets.all(12.0),
                                              decoration: BoxDecoration(
                                                color: Colors.transparent,
                                                border: Border.all(color: Colors.black.withAlpha(12), width: 2.5),
                                                borderRadius: BorderRadius.circular(10.0)
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Text('27', maxLines: 1, overflow: TextOverflow.ellipsis,
                                                        style: GoogleFonts.publicSans(fontSize: 20.0, color: TextColor.secondaryColor, fontWeight: FontWeight.bold)),
                                                    const SizedBox(height: 8.0),
                                                    Text('Wed', maxLines: 1, overflow: TextOverflow.ellipsis, style: GoogleFonts.publicSans(color: TextColor.secondaryColor))
                                                  ]
                                                )
                                              )
                                            );
                                          }).toList()
                                        )
                                      )
                                    )
                                  ),
                                  Opacity(
                                    opacity: snapshot.data! == dragMinValue ? 0.0 : fadeIn,
                                    child: CustomCalendarCarousel(
                                      height: snapshot.data!
                                    )
                                  )
                                ]
                              )
                            )
                          ]
                        )
                      ),
                      Flexible(
                        flex: 0,
                        // child: Align(
                        //   alignment: Alignment.bottomCenter,
                        //   child: GestureDetector(
                        //     onVerticalDragUpdate: (details){
                        //       final current = _dragValue += details.delta.dy;
                        //       if(current <= dragMinValue){
                        //         if(snapshot.data! == dragMinValue) return;
                        //         _dragController!.sink.add(dragMinValue);
                        //         return;
                        //       }
                        //       else if(current >= dragMaxValue){
                        //         if(snapshot.data! == dragMaxValue) return;
                        //         _dragController!.sink.add(dragMaxValue);
                        //         return;
                        //       }
                        //       _dragController!.sink.add(current);
                        //       debugPrint('$current');
                        //     },
                        //     child: Container(
                        //       width: 50, height: 5,
                        //       margin: const EdgeInsets.all(16.0),
                        //       decoration: BoxDecoration(
                        //         color: const Color(0xFF8F90A6),
                        //         borderRadius: BorderRadius.circular(50.0)
                        //       )
                        //     )
                        //   )
                        // )
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: GestureDetector(
                            onVerticalDragUpdate: (details){
                              final current = _dragValue += details.delta.dy;
                              if(current <= dragMinValue){
                                if(snapshot.data! == dragMinValue) return;
                                _dragController!.sink.add(dragMinValue);
                                return;
                              }
                              else if(current >= dragMaxValue){
                                if(snapshot.data! == dragMaxValue) return;
                                _dragController!.sink.add(dragMaxValue);
                                return;
                              }
                              _dragController!.sink.add(current);
                              debugPrint('$current');
                            },
                            child: Container(
                              width: double.infinity, height: 35.0,
                              color: Colors.transparent,
                              child: Center(
                                child: Container(
                                  width: 50, height: 5,
                                  decoration: BoxDecoration(
                                    color: const Color(0xFF8F90A6),
                                    borderRadius: BorderRadius.circular(50.0)
                                  )
                                )
                              )
                            )
                          )
                        )
                      )
                    ]
                  )
                )
              );
            }
          )
        ]
      )
    );
  }
}
