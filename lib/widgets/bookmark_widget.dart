import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rupp_client_app/model/bookmark_model.dart';

class BookmarkWidget extends StatefulWidget {
  final BookmarkContext context;
  final ValueChanged<BookmarkContext>? onChanged;
  const BookmarkWidget({Key? key, required this.context, this.onChanged}) : super(key: key);

  @override
  _BookmarkWidgetState createState() => _BookmarkWidgetState();
}

class _BookmarkWidgetState extends State<BookmarkWidget> {

  static const double _svgSize = 20.0;
  ValueNotifier<BookmarkContext>? _selectedBookmarkNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedBookmarkNotifier = ValueNotifier<BookmarkContext>(widget.context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _selectedBookmarkNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(_selectedBookmarkNotifier!.value.state is BookmarkSaveState){
          _selectedBookmarkNotifier!.value = const BookmarkContext(state: BookmarkUnSaveState());
          widget.onChanged?.call(_selectedBookmarkNotifier!.value);
          return;
        }
        _selectedBookmarkNotifier!.value = const BookmarkContext(state: BookmarkSaveState());
        widget.onChanged?.call(_selectedBookmarkNotifier!.value);
      },
      child: ValueListenableBuilder<BookmarkContext>(
        valueListenable: _selectedBookmarkNotifier!,
        child: SvgPicture.asset('assets/svg/bookmark_outlined.svg', width: _svgSize, height: _svgSize),
        builder: (ctx, value, child){
          if(value.state is BookmarkSaveState){
            return SvgPicture.asset('assets/svg/bookmark.svg', width: _svgSize, height: _svgSize);
          }else{
            return child!;
          }
        }
      )
    );
  }
}

