import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/menu_item.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';

// class MenuItemWidget extends StatelessWidget {
//   final MenuItemModel item;
//   final VoidCallback? onPressed;
//   const MenuItemWidget({Key? key, required this.item, this.onPressed}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: onPressed,
//       child: Container(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               item.icon,
//               const SizedBox(height: 4.0),
//               Flexible(
//                 child: Text(item.label, style: const TextStyle(fontWeight: FontWeight.w600))
//               )
//             ]
//           )
//         ),
//         decoration: BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.circular(20.0),
//           boxShadow: [
//             BoxShadow(color: Colors.black.withAlpha(25), blurRadius: 8.0)
//           ]
//         )
//       )
//     );
//   }
// }

typedef OnPressed = void Function(BuildContext context);
abstract class MenuItemWidget{
  final MenuItemModel item;
  final OnPressed? onPressed;

  const MenuItemWidget({required this.item, this.onPressed});

  Widget build(final BuildContext context);
  static const TextStyle labelStyle = TextStyle(fontWeight: FontWeight.w600);
}

class ColumnMenuItemWidget extends MenuItemWidget{
  ColumnMenuItemWidget({required final MenuItemModel item, final OnPressed? onPressed}) : super(item: item, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ContainerRounded(
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: () => onPressed!(context),
          borderRadius: BorderRadius.circular(10.0),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                item.icon,
                const SizedBox(height: 4.0),
                Flexible(
                  child: Text(item.label, maxLines: 1, overflow: TextOverflow.ellipsis, style: MenuItemWidget.labelStyle)
                )
              ]
            )
          )
        )
      )
    );
  }
}

class RowMenuItemWidget extends MenuItemWidget{
  RowMenuItemWidget({required final MenuItemModel item, final OnPressed? onPressed}) : super(item: item, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ContainerRounded(
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: () => onPressed!(context),
          borderRadius: BorderRadius.circular(10.0),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: Row(
              children: [
                item.icon,
                const SizedBox(width: 16.0),
                Flexible(
                  child: Text(item.label, style: MenuItemWidget.labelStyle)
                )
              ]
            )
          )
        )
      )
    );
  }
}

