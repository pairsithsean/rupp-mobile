import 'package:flutter/material.dart';
import 'package:rupp_client_app/style/matrial_color.dart';

class CustomFillButton extends StatelessWidget {
  final String label;
  final VoidCallback? onPressed;
  const CustomFillButton({Key? key, required this.label, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity, height: 48.0,
      child: TextButton(
        child: Text(label),
        style: TextButton.styleFrom(
          backgroundColor: onPressed == null ? const Color(0xFFC7C9D9) : CustomMaterialColor.primary,
          primary: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))
        ),
        onPressed: onPressed
      )
    );
  }
}
