import 'package:flutter/material.dart';

class CustomCheckboxListTile extends StatefulWidget {
  final String title;
  final bool? initialValue;
  final ValueChanged<bool?> onChanged;
  const CustomCheckboxListTile({Key? key, required this.title, required this.onChanged, this.initialValue = false}) : super(key: key);

  @override
  _CustomCheckboxListTileState createState() => _CustomCheckboxListTileState();
}

class _CustomCheckboxListTileState extends State<CustomCheckboxListTile> {

  ValueNotifier<bool>? _isCheckedNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isCheckedNotifier = ValueNotifier(widget.initialValue!);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _isCheckedNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _isCheckedNotifier!,
      builder: (ctx, value, child){
        return CheckboxListTile(
          title: Text(widget.title),
          controlAffinity: ListTileControlAffinity.trailing,
          value: value,
          dense: true,
          onChanged: (v){
            _isCheckedNotifier!.value = v!;
            widget.onChanged(v);
          }
        );
      }
    );
  }
}

