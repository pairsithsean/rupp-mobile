import 'package:flutter/material.dart';

class CustomChip extends StatelessWidget {
  final String label;
  final Color? color;
  final Color? textColor;
  const CustomChip({Key? key, required this.label, this.color = Colors.transparent, this.textColor = Colors.black}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Text(label, style: TextStyle(fontSize: 12.0, color: textColor)),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(8.0)
      )
    );
  }
}
