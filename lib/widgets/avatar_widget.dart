import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

class AvatarWidget extends StatelessWidget {
  final String url;
  const AvatarWidget({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: SizedBox(
        width: 80.0, height: 80.0,
        child: CachedNetworkImage(
          imageUrl: url, fit: BoxFit.cover,
          errorWidget: (ctx, a, b) => Container(color: Colors.black.withAlpha(12)),
          placeholder: (ctx, url) => LoadingWidget.instance
        )
      )
    );
  }
}
