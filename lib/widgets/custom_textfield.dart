import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';

typedef OnFieldSubmitted = void Function(String? value);
typedef OnChanged = void Function(String? value);
class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final TextInputType? textInputType;
  final String? hintText;
  final OnFieldSubmitted? onFieldSubmitted;
  final OnChanged? onChanged;
  final FocusNode? focusNode;
  final int? maxLines;
  final bool expands;
  final bool errorState;
  const CustomTextField({Key? key, this.controller, this.onFieldSubmitted, this.onChanged, this.focusNode, this.maxLines = 1,
    this.expands = false, this.errorState = false,
    this.hintText = '', this.textInputType = TextInputType.text}) : super(key: key);


  static InputBorder enabledDefaultBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: const BorderSide(color: Colors.black12)
  );

  static InputBorder focusedDefaultBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: const BorderSide(color: CustomMaterialColor.primary)
  );

  static TextStyle defaultHintStyle = const TextStyle(color: TextColor.secondaryColor, fontSize: 14.0,
      fontWeight: FontWeight.normal);

  static InputBorder customErrorBorder =  OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: const BorderSide(color: CustomMaterialColor.primary)
  );

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: textInputType,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      maxLines: maxLines, expands: expands,
      textAlignVertical: TextAlignVertical.top,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: defaultHintStyle,
        enabledBorder: errorState ? customErrorBorder : enabledDefaultBorder,
        focusedBorder: enabledDefaultBorder
      )
    );
  }
}

class OTPField extends CustomTextField{

  final bool isNotMatched;
  static const int maxLength = 1;

  const OTPField({Key? key, this.isNotMatched = false, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: TextInputType.number,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      maxLines: maxLines, maxLength: maxLength,
      style: const TextStyle(fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
      cursorColor: const Color.fromRGBO(77, 131, 241, 1.0),
      decoration: InputDecoration(
        hintText: hintText,
        counterText: '',
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color.fromRGBO(77, 131, 241, 1.0)),
          borderRadius: BorderRadius.circular(10.0)
        ),
        enabledBorder: isNotMatched ? OutlineInputBorder(
          borderSide: const BorderSide(color: CustomMaterialColor.primary),
          borderRadius: BorderRadius.circular(10.0)
        ) : null,
        border: CustomTextField.enabledDefaultBorder
      )
    );
  }
}

class PasswordField extends CustomTextField{

  final bool? isPasswordMatched;
  static const bool obscureText = true;

  const PasswordField({Key? key, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged, final bool errorState = false, this.isPasswordMatched = false}) : super(key: key,
    onFieldSubmitted: onFieldSubmitted, onChanged: onChanged, errorState: errorState,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: TextInputType.text,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      style: const TextStyle(fontWeight: FontWeight.bold),
      obscureText: obscureText,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: CustomTextField.defaultHintStyle,
        counterText: '',
        enabledBorder: isPasswordMatched! ? PasswordMatchedOutlineBorder.instance.border : errorState ? CustomTextField.customErrorBorder
            : CustomTextField.enabledDefaultBorder,
        focusedBorder: isPasswordMatched! ? PasswordMatchedOutlineBorder.instance.border : CustomTextField.enabledDefaultBorder,
        border: CustomTextField.enabledDefaultBorder
      )
    );
  }
}


class PasswordMatchedOutlineBorder{

  PasswordMatchedOutlineBorder._();

  static PasswordMatchedOutlineBorder? _border;
  static PasswordMatchedOutlineBorder get instance{
    if(_border == null) {
      return _border = PasswordMatchedOutlineBorder._();
    } else {
      return _border!;
    }
  }

  InputBorder get border{
    return OutlineInputBorder(
      borderSide: const BorderSide(color: Color.fromRGBO(109, 208, 168, 1.0)),
      borderRadius: BorderRadius.circular(10.0)
    );
  }
}

class SearchField extends CustomTextField{
  final VoidCallback? onSuffixIconTap;
  const SearchField({Key? key, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode, this.onSuffixIconTap,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: [
        TextFormField(
          controller: controller,
          focusNode: focusNode,
          keyboardType: TextInputType.text,
          onFieldSubmitted: onFieldSubmitted,
          onChanged: onChanged,
          //style: const TextStyle(fontWeight: FontWeight.bold),
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: const TextStyle(fontSize: 14.0, color: TextColor.secondaryColor),
            fillColor: Colors.white, filled: true,
            counterText: '',
            contentPadding: const EdgeInsets.symmetric(horizontal: 46.0),
            enabledBorder: CustomTextField.enabledDefaultBorder,
            focusedBorder: CustomTextField.enabledDefaultBorder
          )
        ),
        Positioned(
          top: 0, bottom: 0, left: 16.0,
          child: SvgPicture.asset('assets/svg/search.svg', width: 18.0, height: 18.0)
        ),
        Positioned(
          top: 0, bottom: 0, right: 16.0,
          child: GestureDetector(
            child: SvgPicture.asset('assets/svg/filter.svg', width: 18.0, height: 18.0),
            onTap: onSuffixIconTap
          )
        )
      ]
    );
  }
}

class PhoneNumberField extends CustomTextField{

  const PhoneNumberField({Key? key, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: TextInputType.phone,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      inputFormatters: [
        MaskedInputFormatter('###-###-####')
      ],
      decoration: InputDecoration(
        hintText: hintText,
        counterText: '',
        enabledBorder: CustomTextField.enabledDefaultBorder,
        focusedBorder: CustomTextField.enabledDefaultBorder
      )
    );
  }
}

class CreditCardField extends CustomTextField{

  const CreditCardField({Key? key, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: [
        TextFormField(
          controller: controller,
          focusNode: focusNode,
          keyboardType: TextInputType.number,
          onFieldSubmitted: onFieldSubmitted,
          onChanged: onChanged,
          inputFormatters: [
            MaskedInputFormatter('#### #### #### ####')
          ],
          decoration: InputDecoration(
            hintText: hintText,
            counterText: '',
            contentPadding: const EdgeInsets.only(right: 90.0, left: 16.0, top: 16.0, bottom: 16.0),
            enabledBorder: CustomTextField.enabledDefaultBorder,
            focusedBorder: CustomTextField.enabledDefaultBorder
          )
        ),
        Positioned(
          top: 0.0, bottom: 0.0, right: 16.0,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: 70, height: 35,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.black.withAlpha(12))
                )
              ),
              SvgPicture.asset('assets/svg/visa.svg', width: 12.0, height: 12.0)
            ]
          )
        )
      ]
    );
  }
}

class CCVField extends CustomTextField{

  const CCVField({Key? key, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: TextInputType.number,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      inputFormatters: [
        MaskedInputFormatter('###'),
        LengthLimitingTextInputFormatter(3)
      ],
      decoration: InputDecoration(
        hintText: hintText,
        counterText: '',
        enabledBorder: CustomTextField.enabledDefaultBorder,
        focusedBorder: CustomTextField.enabledDefaultBorder
      )
    );
  }
}

class ExpDateField extends CustomTextField{

  const ExpDateField({Key? key, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: TextInputType.number,
      onFieldSubmitted: onFieldSubmitted,
      onChanged: onChanged,
      inputFormatters: [
        MaskedInputFormatter('##/##'),
        LengthLimitingTextInputFormatter(5)
      ],
      decoration: InputDecoration(
        hintText: hintText,
        counterText: '',
        enabledBorder: CustomTextField.enabledDefaultBorder,
        focusedBorder: CustomTextField.enabledDefaultBorder
      )
    );
  }
}