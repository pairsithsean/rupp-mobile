import 'package:flutter/widgets.dart';

class CustomIcon{
  CustomIcon._();

  static const _kFontFam = 'BottomBarFont';
  static const _kFontControlFam = 'ControlPlayerFont';
  static const String? _kFontPkg = null;

  static const IconData home = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bell = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData more = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData result = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);

  static const IconData fastPrevious = IconData(0xe800, fontFamily: _kFontControlFam, fontPackage: _kFontPkg);
  static const IconData skipForward = IconData(0xe801, fontFamily: _kFontControlFam, fontPackage: _kFontPkg);
  static const IconData skipPrevious = IconData(0xe802, fontFamily: _kFontControlFam, fontPackage: _kFontPkg);
  static const IconData fastForward = IconData(0xe803, fontFamily: _kFontControlFam, fontPackage: _kFontPkg);
}