import 'package:flutter/material.dart';

abstract class CustomMaterialColor{
  static const MaterialColor primary = MaterialColor(_primaryPrimaryValue, <int, Color>{
    50: Color(0xFFFDE7E7),
    100: Color(0xFFF9C2C4),
    200: Color(0xFFF6999C),
    300: Color(0xFFF27074),
    400: Color(0xFFEF5257),
    500: Color(_primaryPrimaryValue),
    600: Color(0xFFEA2E33),
    700: Color(0xFFE7272C),
    800: Color(0xFFE42024),
    900: Color(0xFFDF1417),
  });
  static const int _primaryPrimaryValue = 0xFFEC3339;

  static const MaterialColor primaryAccent = MaterialColor(_primaryAccentValue, <int, Color>{
    100: Color(0xFFFFFFFF),
    200: Color(_primaryAccentValue),
    400: Color(0xFFFFA8A9),
    700: Color(0xFFFF8F90),
  });
  static const int _primaryAccentValue = 0xFFFFDBDC;

  static const Color backgroundColor = Color.fromRGBO(250, 250, 252, 1.0);
}