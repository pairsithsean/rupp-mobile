import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rupp_client_app/screen/login.dart';
import 'package:rupp_client_app/screen/splash.dart';
import 'package:rupp_client_app/style/matrial_color.dart';

// void main() {
//   WidgetsFlutterBinding.ensureInitialized();
//   SystemChrome.setPreferredOrientations([
//     DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
//   ]);
//   runApp(const MyApp());
// }

class MyApp extends StatefulWidget {
  final String title;
  final bool isOnBoardingAlreadyViewed;
  const MyApp({Key? key, this.title = '', this.isOnBoardingAlreadyViewed = false}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: widget.title,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: CustomMaterialColor.primary,
        textTheme: GoogleFonts.manropeTextTheme(),
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          titleTextStyle: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.w600)
        ),
        textSelectionTheme: const TextSelectionThemeData(
          cursorColor: Colors.black
        ),
        checkboxTheme: CheckboxThemeData(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
          materialTapTargetSize: MaterialTapTargetSize.padded
        )
      ),
      home: widget.isOnBoardingAlreadyViewed ? const LoginScreen() : const SplashScreen()
    );
  }
}


// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: CustomMaterialColor.primary,
//         textTheme: GoogleFonts.manropeTextTheme(),
//         appBarTheme: const AppBarTheme(
//           backgroundColor: Colors.white,
//           iconTheme: IconThemeData(color: Colors.black),
//           titleTextStyle: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.w600)
//         ),
//         textSelectionTheme: const TextSelectionThemeData(
//           cursorColor: Colors.black
//         ),
//         checkboxTheme: CheckboxThemeData(
//           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
//           materialTapTargetSize: MaterialTapTargetSize.padded
//         )
//       ),
//       home: const LoginScreen()
//     );
//   }
// }
