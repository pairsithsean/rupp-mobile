class StickyNoteModel{
  final StickyNoteData data;
  const StickyNoteModel({required this.data});
}

class StickyNoteData{
  final String time;
  final String subject;
  final String lecturer;
  const StickyNoteData({required this.time, required this.subject, required this.lecturer});

  bool hasData(){
    return time.isNotEmpty && subject.isNotEmpty && lecturer.isNotEmpty;
  }
}