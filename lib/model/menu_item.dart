import 'package:flutter/cupertino.dart';

class MenuItemModel{
  final String label;
  final Widget icon;

  const MenuItemModel({required this.label, required this.icon});
}