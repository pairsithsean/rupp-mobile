import 'package:intl/intl.dart';
import 'package:rupp_client_app/model/view/sticky_note_view.dart';

class TimetableModel{

  // final TimetableData? data;
  // final TimetableDate? date;
  // const TimetableModel({this.data , this.date});

  static const List<String> times = [
    '7 am', '8 am', '9 am', '10 am', '11 am',
    '12 pm', '1 pm', '2 pm', '3 pm'
  ];

  // static const List<TimetableDate> dates = [
  //   TimetableDate(date: '22.11.2021', day: 'Monday'),
  //   TimetableDate(date: '23.11.2021', day: 'Tuesday'),
  //   TimetableDate(date: '24.11.2021', day: 'Wednesday'),
  //   TimetableDate(date: '25.11.2021', day: 'Thursday'),
  //   TimetableDate(date: '26.11.2021', day: 'Friday'),
  //   TimetableDate(date: '27.11.2021', day: 'Saturday'),
  //   TimetableDate(date: '28.11.2021', day: 'Sunday')
  // ];

  final List<TimetableDate> dates = [];

  void loadDate(){
    final todayDate = DateTime.now();
    final lastMonday = todayDate.subtract(Duration(days: todayDate.weekday - DateTime.monday));
    final inDays = todayDate.difference(lastMonday).inDays;
    for(int i = -inDays; i < DateTime.daysPerWeek - inDays; i++){
      final day = DateFormat.EEEE().format(todayDate.add(Duration(days: i)));
      final date = DateFormat('dd.MM.yyyy').format(todayDate.add(Duration(days: i)));
      final timetableDate = TimetableDate(date: date, day: day);
      dates.add(timetableDate);
    }
  }

  final List<Map<String, List<FactoryStickyNoteView?>>> _data = [];

  List<Map<String, List<FactoryStickyNoteView?>>> getData(){
    return _data;
  }

  void addData(final Map<String, List<FactoryStickyNoteView?>> data){
    _data.add(data);
  }
}

class TimetableDate{
  final String day;
  final String date;

  const TimetableDate({required this.date, required this.day});
}