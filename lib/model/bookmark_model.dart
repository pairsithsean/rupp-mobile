import 'package:rupp_client_app/model/online_learning_model.dart';

class BookmarkModel extends OnlineLearningData{
  BookmarkModel({required final String title, required final String date,
    required final OnlineLearningMaterial material,
    required final BookmarkContext bookmarkContext, final String? time})
      : super(title: title, date: date, time: time, context: bookmarkContext, material: material);
  // final String? title;
  // final BookmarkContext context;
  //
  // const BookmarkModel({this.title, required this.context});
}

abstract class BookmarkState{
  const BookmarkState();
  bool handle();
}

class BookmarkSaveState extends BookmarkState{
  const BookmarkSaveState() : super();
  @override
  bool handle() {
    // TODO: implement handle
    return true;
  }
}

class BookmarkUnSaveState extends BookmarkState{
  const BookmarkUnSaveState() : super();
  @override
  bool handle() {
    // TODO: implement handle
    return false;
  }
}

class BookmarkContext{
  final BookmarkState? state;
  const BookmarkContext({this.state});
}
