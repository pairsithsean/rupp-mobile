import 'package:rupp_client_app/model/state_model.dart';

class LoginModel{
  final String phoneNumber;
  final String password;
  final IState state;

  const LoginModel({required this.phoneNumber,
    required this.password, required this.state});
}

// abstract class LoginState extends IState{
//   String handle();
// }

// class LoginSuccessState extends IState{
//   @override
//   bool handle() {
//     // TODO: implement handle
//     return true;
//   }
//
//   @override
//   String message() {
//     // TODO: implement message
//     throw UnimplementedError();
//   }
// }
//
// class LoginErrorState extends IState{
//   @override
//   bool handle() {
//     // TODO: implement handle
//     return true;
//   }
//
//   @override
//   String message() {
//     // TODO: implement message
//     throw UnimplementedError();
//   }
// }