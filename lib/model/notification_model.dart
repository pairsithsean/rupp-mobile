class NotificationModel{
  final String title;
  final List<NotificationData> data;

  const NotificationModel({required this.title, required this.data});
}


class NotificationData{
  final String title;
  final String subTitle;
  final String time;

  const NotificationData({required this.title, required this.subTitle, required this.time});
}