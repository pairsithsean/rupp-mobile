class AttendanceHistoryModel{
  final String date;
  List<AttendanceHistoryData> data = [];
  AttendanceHistoryModel({required this.date, required this.data});
}

class AttendanceHistoryData{
  final String? title;
  final String? date;
  final String? reason;
  final String? status;

  const AttendanceHistoryData({this.title, this.date, this.reason, this.status});
}