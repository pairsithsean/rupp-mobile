abstract class IState{
  bool handle();
  String message();
}

class ErrorState extends IState{
  @override
  bool handle() {
    // TODO: implement handle
    return true;
  }

  @override
  String message() {
    // TODO: implement message
    throw UnimplementedError();
  }
}

class SuccessState extends IState{
  @override
  bool handle() {
    // TODO: implement handle
    return true;
  }

  @override
  String message() {
    // TODO: implement message
    throw UnimplementedError();
  }

}

class StateContext{
  final IState? state;
  const StateContext({this.state});
}