import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/bookmark_model.dart';
import 'package:rupp_client_app/model/view/online_learning_view.dart';

class BookmarkView extends StatefulWidget {
  final BookmarkModel data;
  const BookmarkView({Key? key, required this.data}) : super(key: key);

  @override
  _BookmarkViewState createState() => _BookmarkViewState();
}

class _BookmarkViewState extends State<BookmarkView> {

  ValueNotifier<BookmarkContext>? _bookmarkContextNotifier;
  FactoryOnlineLearningViewCreator? _creator;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bookmarkContextNotifier =  ValueNotifier<BookmarkContext>(const BookmarkContext(state: BookmarkSaveState()));
    // _creator = FactoryOnlineLearningViewCreator(data: OnlineLearningData(
    //   title: 'aa',
    //   date: 'ggg',
    //   context: const BookmarkContext(state: BookmarkSaveState())
    // ), bookmarkContext: _bookmarkContextNotifier!);
    _creator = FactoryOnlineLearningViewCreator(data: widget.data);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _bookmarkContextNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // return ContainerRounded(
    //   margin: const EdgeInsets.symmetric(vertical: 12.0),
    //   child: Center(
    //     child: ListTile(
    //       title: Text(widget.data.title!,
    //         maxLines: 2, overflow: TextOverflow.ellipsis, style: const TextStyle(fontWeight: FontWeight.w600)),
    //       leading: const CircleAvatar(
    //         radius: 15,
    //         child: Icon(Icons.contact_mail, size: 15.0)
    //       ),
    //       onTap: (){},
    //       trailing: BookmarkWidget(context: widget.data.context)
    //     )
    //   )
    // );
    return _creator!.factoryMethod(viewType: OnlineLearningViewType.column).build(context);
  }
}

