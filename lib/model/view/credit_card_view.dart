import 'package:flutter/material.dart';
import 'package:rupp_client_app/widgets/custom_form.dart';
import 'package:rupp_client_app/widgets/custom_textfield.dart';

class CreditCardView extends StatefulWidget {
  final TextEditingController cardHolderController;
  final TextEditingController cardNumberController;
  final TextEditingController expDateController;
  final TextEditingController ccvController;
  const CreditCardView({Key? key, required this.cardHolderController,
    required this.cardNumberController, required this.expDateController,
    required this.ccvController}) : super(key: key);

  @override
  _CreditCardViewState createState() => _CreditCardViewState();
}

class _CreditCardViewState extends State<CreditCardView> {

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          FormItem(
            label: 'Cardholder Name',
            child: CustomTextField(
              controller: widget.cardHolderController
            )
          ),
          const SizedBox(height: 16.0),
          FormItem(
            label: 'Card Number',
            child: CreditCardField(
              controller: widget.cardNumberController
            )
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                child: FormItem(
                  label: 'Exp.Date',
                  child: ExpDateField(
                    controller: widget.expDateController
                  )
                )
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: FormItem(
                  label: 'CCV',
                  child: CCVField(
                    controller: widget.ccvController
                  )
                )
              )
            ]
          )
        ]
      )
    );
  }
}
