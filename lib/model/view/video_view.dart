import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/bookmark_model.dart';
import 'package:rupp_client_app/model/online_learning_model.dart';
import 'package:rupp_client_app/model/view/video_player_view.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/bookmark_widget.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';
import 'package:video_player/video_player.dart';

class VideoView extends StatefulWidget {
  final OnlineLearningData data;
  const VideoView({Key? key, required this.data}) : super(key: key);

  @override
  _VideoViewState createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {

  VideoPlayerController? _playerController;
  OverlayEntry? _entry;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    const url = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';
    _playerController = VideoPlayerController.network(url, formatHint: VideoFormat.ss);
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async{
      try{
        await _playerController?.initialize();
        //await _playerController?.play();
        setState(() {
        });
      }catch(e){
        debugPrint('$e');
        setState(() {

        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _playerController?.removeListener(() { });
    _playerController?.dispose();
    _entry?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 2,
                child: Text(widget.data.title, style: const TextStyle(
                  fontSize: 20.0, fontWeight: FontWeight.bold
                ))
              ),
              const Flexible(
                child: BookmarkWidget(context: BookmarkContext(state: BookmarkUnSaveState()))
              )
            ]
          ),
          const SizedBox(height: 16.0),
          Row(
            children: const [
              Expanded(
                flex: 0,
                child: CircleAvatar(
                  radius: 10.0
                )
              ),
              SizedBox(width: 8.0),
              Expanded(
                child: Text('Name', style: TextStyle(
                  fontSize: 12.0, color: TextColor.secondaryColor
                ))
              ),
              Flexible(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text('Dec 10, 2021', style: TextStyle(
                    fontSize: 12.0, color: TextColor.secondaryColor
                  ))
                )
              )
            ]
          ),
          const SizedBox(height: 16.0),
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: SizedBox(
              height: 150.0,
              // child: _playerController!.value.isInitialized
              //     ? Stack(
              //   alignment: Alignment.center,
              //   children: [
              //     Hero(
              //       tag: 'video',
              //       child: VideoPlayer(_playerController!)
              //     ),
              //     GestureDetector(
              //       onTap: (){
              //         if(_playerController!.value.isInitialized){
              //           // _playerController?.play();
              //           // setState(() {
              //           // });
              //           _entry = OverlayEntry(
              //             builder: (ctx){
              //               return VideoPlayerView(
              //                 controller: _playerController!,
              //                 onBackPressed: (){
              //                   _entry?.remove();
              //                   _entry = null;
              //                 }
              //               );
              //             }
              //           );
              //           Overlay.of(context, rootOverlay: true)?.insert(_entry!);
              //         }
              //       },
              //       child: Offstage(
              //         offstage: _playerController!.value.isPlaying ? true : false,
              //         child: const CircleAvatar(
              //           child: Icon(Icons.play_arrow)
              //         )
              //       )
              //     )
              //   ]
              // ) : const CircularProgressIndicator.adaptive()
              child: Builder(
                builder: (ctx){
                  if(_playerController!.value.isInitialized){
                    return Stack(
                      alignment: Alignment.center,
                      children: [
                        Hero(
                          tag: 'video',
                          child: VideoPlayer(_playerController!)
                        ),
                        GestureDetector(
                          onTap: (){
                            if(_playerController!.value.isInitialized){
                              _entry = OverlayEntry(
                                builder: (ctx){
                                  return VideoPlayerView(
                                    controller: _playerController!,
                                    onBackPressed: (){
                                      _entry?.remove();
                                      _entry = null;
                                    }
                                  );
                                }
                              );
                              Overlay.of(context, rootOverlay: true)?.insert(_entry!);
                            }
                          },
                          child: Offstage(
                            offstage: _playerController!.value.isPlaying ? true : false,
                            child: const CircleAvatar(
                              child: Icon(Icons.play_arrow)
                            )
                          )
                        )
                      ]
                    );
                  }else if(_playerController!.value.hasError){
                    return const Text('Video load failed');
                  }else{
                    return LoadingWidget.instance;
                  }
                }
              )
            )
          )
        ]
      )
    );
  }
}

