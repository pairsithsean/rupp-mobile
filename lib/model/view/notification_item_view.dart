import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/notification_model.dart';
import 'package:rupp_client_app/style/text_color.dart';

class NotificationItemView extends StatelessWidget {
  final String title;
  final NotificationData data;
  const NotificationItemView({Key? key, required this.title, required this.data}) : super(key: key);

  final FactoryNotificationCreator _creator = const FactoryNotificationCreator();

  @override
  Widget build(BuildContext context) {
    return _creator.factoryMethod(title, data).build(context);
  }
}

abstract class FactoryNotification {
  Widget build(final BuildContext context);
}

class FactoryLatestNotification extends FactoryNotification{

  final NotificationData data;
  FactoryLatestNotification({required this.data});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(16.0),
      child: ListTile(
        leading: Container(
          width: 45.0, height: 45.0,
          decoration: BoxDecoration(
            color: const Color.fromRGBO(253, 245, 252, 1.0),
            borderRadius: BorderRadius.circular(10.0)
          ),
          child: const Icon(Icons.ac_unit_rounded)
        ),
        title: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Text(data.title, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(fontWeight: FontWeight.w600))
              )
            ),
            Text(data.time, style: const TextStyle(fontSize: 12.0))
          ]
        ),
        subtitle: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: Text(data.subTitle, maxLines: 2, overflow: TextOverflow.ellipsis, style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w300)),
                )
              ),
              Container(
                width: 10.0, height: 10.0,
                decoration: BoxDecoration(
                  color: const Color(0xFF2278F8),
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(color: const Color(0xFF2278F8).withAlpha(100), blurRadius: 24.0)
                  ]
                )
              )
            ]
          )
        ),
        onTap: (){}
      )
    );
  }

}

class FactoryOlderNotification extends FactoryNotification{

  final NotificationData data;
  FactoryOlderNotification({required this.data});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(16.0),
      child: ListTile(
        leading: Container(
          width: 45.0, height: 45.0,
          decoration: BoxDecoration(
            color: const Color.fromRGBO(253, 245, 252, 1.0),
            borderRadius: BorderRadius.circular(10.0)
          )
        ),
        title: Row(
          children: [
            Expanded(
              child: Text(data.title, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(color: TextColor.secondaryColor, fontWeight: FontWeight.w600))
            ),
            Text(data.time, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
          ]
        ),
        subtitle: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(data.subTitle, maxLines: 2, overflow: TextOverflow.ellipsis, style: const TextStyle(fontWeight: FontWeight.w300))
        ),
        onTap: (){}
      )
    );
  }
}

class FactoryNotificationCreator{

  const FactoryNotificationCreator();

  FactoryNotification factoryMethod(final String title, final NotificationData data){
    if(title == 'Latest'){
      return FactoryLatestNotification(data: data);
    }else{
      return FactoryOlderNotification(data: data);
    }
  }
}
