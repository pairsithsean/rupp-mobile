import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/attendance_history_model.dart';
import 'package:rupp_client_app/screen/attendance_history.dart';
import 'package:rupp_client_app/style/text_color.dart';

class AttendanceHistoryView extends StatelessWidget {
  final AttendanceHistoryData data;
  const AttendanceHistoryView({Key? key, required this.data}) : super(key: key);

  final FactoryStatusCreator _creator = const FactoryStatusCreator();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      margin: const EdgeInsets.symmetric(vertical: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(color: Colors.black26)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(data.title!, style: const TextStyle(color: TextColor.secondaryColor, fontSize: 12.0))
              ),
              Flexible(
                child: _creator.factoryMethod(data).build(context)
              )
            ]
          ),
          const SizedBox(height: 16.0),
          Text(data.date!, style: const TextStyle(
            fontWeight: FontWeight.bold, fontSize: 17.0
          )),
          const SizedBox(height: 16.0),
          Text(data.reason!)
        ]
      )
    );
  }
}
