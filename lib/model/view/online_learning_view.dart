import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/online_learning_model.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/widgets/bookmark_widget.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_chip.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';

// import '../bookmark_model.dart';

enum OnlineLearningViewType{
  row, column
}

class OnlineLearningView extends StatefulWidget {
  // final OnlineLearningModel model;
  // final List<OnlineLearningData> data;
  // const OnlineLearningView({Key? key, required this.model, required this.data}) : super(key: key);

  final OnlineLearningViewType viewType;
  final OnlineLearningData data;
  final VoidCallback? onTap;
  const OnlineLearningView({Key? key, required this.viewType, required this.data, this.onTap}) : super(key: key);

  @override
  _OnlineLearningViewState createState() => _OnlineLearningViewState();
}

class _OnlineLearningViewState extends State<OnlineLearningView> {

  //FactoryOnlineLearningViewCreator? _creator;
  //ValueNotifier<BookmarkContext>? _selectedBookmarkNotifier;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
     //_selectedBookmarkNotifier = ValueNotifier<BookmarkContext>(widget.data.context!);
     //_creator = FactoryOnlineLearningViewCreator(data: widget.data);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    //_selectedBookmarkNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: FactoryOnlineLearningViewCreator(data: widget.data)
          .factoryMethod(viewType: widget.viewType).build(context),
    );
    //return _creator!.factoryMethod(viewType: widget.viewType).build(context);
  }
}

abstract class FactoryOnlineLearningView{
  final OnlineLearningData data;
  //final ValueNotifier<BookmarkContext>? bookmarkContextNotifier;
  //const FactoryOnlineLearningView({Key? key, required this.data, required this.bookmarkContextNotifier});
  const FactoryOnlineLearningView({Key? key, required this.data});
  Widget build(final BuildContext context);
}

class FactoryOnlineLearningRowView extends FactoryOnlineLearningView{
  // FactoryOnlineLearningRowView({required final OnlineLearningData data, required final ValueNotifier<BookmarkContext> bookmarkContext})
  //     : super(data: data, bookmarkContextNotifier: bookmarkContext);

  final FactoryMaterialViewTypeCreator _materialViewTypeCreator = const FactoryMaterialViewTypeCreator();
  FactoryOnlineLearningRowView({required final OnlineLearningData data})
      : super(data: data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: ContainerRounded(
        width: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 0,
              child: Stack(
                children: [
                  Positioned(
                    child: CachedNetworkImage(
                      imageUrl: 'https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                      fit: BoxFit.fill, height: 250.0
                    )
                  ),
                  Positioned(
                    bottom: 16.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Wrap(
                        direction: Axis.vertical,
                        children: [
                          const CustomChip(label: 'Seminar Room 1', color: Colors.white),
                          const SizedBox(height: 8.0),
                          const CustomChip(label: 'Physic', color: Colors.white),
                          const SizedBox(height: 8.0),
                          _materialViewTypeCreator.factoryMethod(label: data.material.title)!.build(context)
                        ]
                      )
                    )
                  )
                ]
              )
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(data.title, style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                        const SizedBox(height: 8.0),
                        Text('Posted:\t${data.date}', style: const TextStyle(fontSize: 12.0)),
                        const SizedBox(height: 8.0),
                        Text(data.time!, style: const TextStyle(color: CustomMaterialColor.primary, fontSize: 12.0))
                      ]
                    )
                  ),
                  // GestureDetector(
                  //   onTap: (){
                  //     if(data.context!.state is BookmarkSaveState){
                  //      data.context = const BookmarkContext(state: BookmarkUnSaveState());
                  //      _bookmarkContextNotifier!.value = data.context!;
                  //       return;
                  //     }
                  //     data.context = const BookmarkContext(state: BookmarkSaveState());
                  //     _bookmarkContextNotifier!.value = data.context!;
                  //
                  //   },
                  //   child: ValueListenableBuilder<BookmarkContext>(
                  //     valueListenable: _bookmarkContextNotifier!,
                  //     child: SvgPicture.asset('assets/svg/bookmark_outlined.svg', width: 20, height: 20),
                  //     builder: (ctx, value, child){
                  //       if(data.context!.state is BookmarkSaveState){
                  //         return SvgPicture.asset('assets/svg/bookmark.svg', width: 20, height: 20);
                  //       }else{
                  //         return child!;
                  //       }
                  //     }
                  //   )
                  // )
                  BookmarkWidget(
                    context: data.context!,
                    onChanged: (ctx){
                      data.context = ctx;
                    }
                  )
                ]
              )
            )
          ]
        )
      )
    );
  }
}

class FactoryOnlineLearningColumnView extends FactoryOnlineLearningView{
  final FactoryMaterialViewTypeCreator _materialViewTypeCreator = const FactoryMaterialViewTypeCreator();
  FactoryOnlineLearningColumnView({required final OnlineLearningData data})
      : super(data: data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ColumnComposite(
      iComponent: [
        Component(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: ContainerRounded(
              height: 150.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: ContainerRounded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 120,
                        child: Stack(
                          children: [
                            Positioned.fill(
                              child: CachedNetworkImage(
                                imageUrl: 'https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                                fit: BoxFit.fill, height: 250.0
                              )
                            ),
                            Positioned(
                              top: 0, left: 0, 
                              child: Container(
                                width: 65.0, height: 35.0,
                                child: Center(
                                  child: Text('1 File', style: TextStyle(fontSize: 10.0, color:
                                  _materialViewTypeCreator.factoryMethod(label: data.material.title)!.textColor,
                                      fontWeight: FontWeight.w600))
                                ),
                                decoration: BoxDecoration(
                                  color: data.material.title == 'Image' ? const Color(0xFF2278F8) :
                                    _materialViewTypeCreator.factoryMethod(label: data.material.title)!.backgroundColor,
                                  borderRadius: const BorderRadius.only(
                                    bottomRight: Radius.circular(20.0)
                                  )
                                )
                              )
                            )
                          ]
                        )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(data.title, style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                              const SizedBox(height: 8.0),
                              Text('Start:\t${data.date}', style: const TextStyle(fontSize: 12.0)),
                              const SizedBox(height: 8.0),
                              Flexible(
                                child:  Wrap(
                                  direction: Axis.vertical,
                                  runSpacing: 8.0, spacing: 8.0,
                                  children: [
                                    CustomChip(label: 'Seminar Room 1', color: Colors.black.withAlpha(12)),
                                    CustomChip(label: 'Physic', color: Colors.black.withAlpha(12)),
                                    _materialViewTypeCreator.factoryMethod(label: data.material.title)!.build(context)
                                  ]
                                )
                              )
                            ]
                          )
                        )
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: BookmarkWidget(
                          context: data.context!,
                          onChanged: (ctx){
                            data.context = ctx;
                          }
                        )
                        // child: GestureDetector(
                        //   onTap: (){
                        //     if(data.context!.state is BookmarkSaveState){
                        //       data.context = const BookmarkContext(state: BookmarkUnSaveState());
                        //       _bookmarkContextNotifier!.value = data.context!;
                        //       return;
                        //     }
                        //     data.context = const BookmarkContext(state: BookmarkSaveState());
                        //     _bookmarkContextNotifier!.value = data.context!;
                        //
                        //   },
                        //   child: ValueListenableBuilder<BookmarkContext>(
                        //     valueListenable: _bookmarkContextNotifier!,
                        //     child: SvgPicture.asset('assets/svg/bookmark_outlined.svg', width: 20, height: 20),
                        //     builder: (ctx, value, child){
                        //       if(data.context!.state is BookmarkSaveState){
                        //         return SvgPicture.asset('assets/svg/bookmark.svg', width: 20, height: 20);
                        //       }else{
                        //         return child!;
                        //       }
                        //     }
                        //   )
                        // )
                      )
                    ]
                  )
                )
              )
            )
          )
        )
      ]
    ).build(context);
  }
}

class FactoryOnlineLearningViewCreator{
  final OnlineLearningData data;
  const FactoryOnlineLearningViewCreator({required this.data});
  FactoryOnlineLearningView factoryMethod({final OnlineLearningViewType? viewType}){
    switch(viewType){
      case OnlineLearningViewType.row: return FactoryOnlineLearningRowView(data: data);
      default: return FactoryOnlineLearningColumnView(data: data);
    }
  }
}


/// CREATE MATERIAL VIEW TYPE
abstract class FactoryMaterialViewType{
  final String label;
  final Color backgroundColor;
  final Color textColor;
  const FactoryMaterialViewType({required this.label, this.backgroundColor = Colors.white, this.textColor = Colors.white});
  Widget build(final BuildContext context);
}

class FactoryMaterialMediaType extends FactoryMaterialViewType{
  FactoryMaterialMediaType({required String label}) : super(label: label);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomChip(label: label, color: backgroundColor, textColor: textColor);
  }

  @override
  // TODO: implement backgroundColor
  Color get backgroundColor => const Color(0xFFFFCC00);

  @override
  // TODO: implement textColor
  Color get textColor => Colors.black;
}

class FactoryMaterialDefaultType extends FactoryMaterialViewType{
  FactoryMaterialDefaultType({required String label}) : super(label: label);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomChip(label: label, color: backgroundColor);
  }

  @override
  // TODO: implement backgroundColor
  Color get backgroundColor => Colors.black.withAlpha(12);
}

class FactoryMaterialPDFType extends FactoryMaterialViewType{
  FactoryMaterialPDFType({required String label}) : super(label: label);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomChip(label: label, color: backgroundColor, textColor: textColor);
  }

  @override
  // TODO: implement backgroundColor
  Color get backgroundColor => const Color(0xFFEE3436);
}

class FactoryMaterialQuizType extends FactoryMaterialViewType{
  FactoryMaterialQuizType({required String label}) : super(label: label);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomChip(label: label, color: backgroundColor, textColor: textColor);
  }

  @override
  // TODO: implement backgroundColor
  Color get backgroundColor => const Color(0xFF10CF7C);
}

class FactoryMaterialWordType extends FactoryMaterialViewType{
  FactoryMaterialWordType({required String label}) : super(label: label);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomChip(label: label, color: backgroundColor, textColor: textColor);
  }

  @override
  // TODO: implement backgroundColor
  Color get backgroundColor => const Color(0xFF2278F8);
}

class FactoryMaterialViewTypeCreator{
  const FactoryMaterialViewTypeCreator();

  FactoryMaterialViewType? factoryMethod({required final String label}){
    switch(label){
      case 'Video': return FactoryMaterialMediaType(label: label);
      case 'Podcast': return FactoryMaterialMediaType(label: label);
      case 'PDF': return FactoryMaterialPDFType(label: label);
      case 'Word': return FactoryMaterialWordType(label: label);
      default: return FactoryMaterialDefaultType(label: label);
    }
  }
}