import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rupp_client_app/utils/show_loading_indicator.dart';
import 'package:video_player/video_player.dart';

import '../../widgets/custom_control_player.dart';
import '../../widgets/custom_progressbar.dart';
import '../../widgets/empty_widget.dart';

class VideoPlayerView extends StatefulWidget{
  final VideoPlayerController controller;
  final VoidCallback onBackPressed;
  const VideoPlayerView({Key? key, required this.controller, required this.onBackPressed}) : super(key: key);

  @override
  _VideoPlayerViewState createState() => _VideoPlayerViewState();
}

class _VideoPlayerViewState extends State<VideoPlayerView> with SingleTickerProviderStateMixin{

  StreamController<DurationState>? _progressBarController;
  AnimationController? _animationController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _progressBarController = StreamController<DurationState>.broadcast();
    //
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 500));
    _animationController?.animateTo(1.0, curve: Curves.ease);
    //
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async{
      await widget.controller.play();
      if(mounted){
        setState(() {
        });
      }
    });
    //
    widget.controller.addListener(() async{
      if(!widget.controller.value.isPlaying){
        if(mounted){
          setState(() {
          });
        }
      }
      final progress = await widget.controller.position;
      final buffered = await widget.controller.position;
      final total = widget.controller.value.duration;
      if(mounted){
        try{
          if(_progressBarController != null){
            if(!_progressBarController!.isClosed){
              _progressBarController!.sink.add(DurationState(
                progress: progress!, buffered: buffered!, total: total
              ));
            }
          }
        }catch(e){
          debugPrint('$e');
        }
      }
    });
  }

  void _onPlayOrPausePressed() async{
    if(!widget.controller.value.isPlaying){
      await widget.controller.play();
    }else{
      await widget.controller.pause();
    }
    if(mounted){
      setState(() {
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _progressBarController?.close();
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    return Material(
      color: Colors.black,
      child: SafeArea(
        top: orientation == Orientation.portrait ? true : false,
        bottom: orientation == Orientation.portrait ? true : false,
        left: false, right: false,
        child: OrientationBuilder(
          builder: (ctx, _orientation){
            if(_orientation == Orientation.portrait){
              // return VideoPlayerPortraitView(
              //   controller: widget.controller,
              //   onPlayPressed: _onPlayOrPausePressed,
              //   animationController: _animationController!,
              //   progressBarController: _progressBarController!,
              //   onBackPressed: widget.onBackPressed
              // );
              return VideoPlayerPortraitViewMode(
                controller: widget.controller,
                onPlayPressed: _onPlayOrPausePressed,
                animationController: _animationController!,
                progressBarController: _progressBarController!,
                onBackPressed: widget.onBackPressed
              ).build(context);
            }else{
              // return VideoPlayerLandscapeView(
              //   controller: widget.controller,
              //   animationController: _animationController!,
              //   onPlayPressed: _onPlayOrPausePressed,
              //   progressBarController: _progressBarController!,
              //   onBackPressed: (){
              //     setState(() {
              //       SystemChrome.setPreferredOrientations([
              //         DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
              //       ]);
              //     });
              //     widget.onBackPressed();
              //   }
              // );
              return VideoPlayerLandscapeViewMode(
                controller: widget.controller,
                animationController: _animationController!,
                onPlayPressed: _onPlayOrPausePressed,
                progressBarController: _progressBarController!,
                onBackPressed: (){
                  setState(() {
                    SystemChrome.setPreferredOrientations([
                      DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
                    ]);
                  });
                  widget.onBackPressed();
                }
              ).build(context);
            }
          }
        )
      )
    );
  }
}

abstract class VideoPlayerViewMode {
  final VideoPlayerController controller;
  final VoidCallback onBackPressed;
  final VoidCallback onPlayPressed;
  final StreamController<DurationState> progressBarController;
  final AnimationController animationController;
  const VideoPlayerViewMode({required this.controller, required this.onPlayPressed,
    required this.onBackPressed, required this.progressBarController, required this.animationController});

  Widget build(final BuildContext context);
}

class VideoPlayerLandscapeViewMode extends VideoPlayerViewMode{
  VideoPlayerLandscapeViewMode({required VideoPlayerController controller,
    required VoidCallback onPlayPressed, required VoidCallback onBackPressed,
    required StreamController<DurationState> progressBarController,
    required AnimationController animationController})
      : super(controller: controller,
      onPlayPressed: onPlayPressed,
      onBackPressed: onBackPressed, progressBarController: progressBarController, animationController: animationController);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final orientation = MediaQuery.of(context).orientation;
    return Stack(
      children: [
        GestureDetector(
          child: VideoPlayer(controller),
          onTap: (){
            if(animationController.isCompleted){
              animationController.reverse();
            }else{
              animationController.animateTo(1.0, curve: Curves.ease);
            }
          }
        ),
        Positioned(
          top: 16.0, left: 16.0,
          child: AnimatedBuilder(
            animation: animationController,
            child: BackButton(
              color: Colors.white,
              onPressed: () async{
                if(controller.value.isPlaying){
                  await controller.pause();
                }
                onBackPressed.call();
              }
            ),
            builder: (ctx, child){
              return FadeTransition(
                opacity: Tween(begin: 1.0, end: 0.0).animate(animationController),
                child: IgnorePointer(
                  ignoring: animationController.value == 1.0 ? true : false,
                  child: child
                )
              );
            }
          )
        ),
        Positioned(
          bottom: 16.0, left: 56.0, right: 56.0,
          child: AnimatedBuilder(
            animation: animationController,
            child: Column(
              children: [
                Flexible(
                  flex: 0,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: _OrientationButton(
                      onPressed: (){
                        if(orientation == Orientation.landscape){
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
                          ]);
                          return;
                        }
                        SystemChrome.setPreferredOrientations([
                          DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight
                        ]);
                      }
                    )
                  )
                ),
                const SizedBox(height: 8.0),
                Flexible(
                  flex: 0,
                  child: StreamBuilder<DurationState>(
                    stream: progressBarController.stream,
                    builder: (ctx, snapshot){
                      if(snapshot.hasData){
                        final durationState = snapshot.data;
                        return CustomProgressBar(
                          state: durationState!,
                          onSeek: (duration) async{
                            try{
                              await controller.seekTo(duration);
                            }catch(e){
                              debugPrint('$e');
                            }
                          }
                        );
                      }else{
                        return EmptyWidget.instance;
                      }
                    }
                  )
                ),
                Flexible(
                  flex: 0,
                  child: ControlPlayerWidget(
                    isPlaying: controller.value.isPlaying,
                    onPressed: onPlayPressed,
                    iconColor: Colors.white,
                    onSkipPreviousPressed: (){},
                    onSkipForwardPressed: (){},
                    onFastPreviousPressed: (){},
                    onFastForwardPressed: (){}
                  )
                )
              ]
            ),
            builder: (ctx, child){
              return FadeTransition(
                opacity: Tween(begin: 1.0, end: 0.0).animate(animationController),
                child: IgnorePointer(
                  ignoring: animationController.value == 1.0 ? true : false,
                  child: child
                )
              );
            }
          )
        )
      ]
    );
  }
}

class VideoPlayerPortraitViewMode extends VideoPlayerViewMode{
  VideoPlayerPortraitViewMode({
    required VideoPlayerController controller, 
    required VoidCallback onPlayPressed, required VoidCallback onBackPressed, required StreamController<DurationState> progressBarController, 
    required AnimationController animationController}) : super(controller: controller, onPlayPressed: onPlayPressed, onBackPressed: onBackPressed, 
      progressBarController: progressBarController, animationController: animationController);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final orientation = MediaQuery.of(context).orientation;
    return Column(
      children: [
        AnimatedBuilder(
          animation: animationController,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: BackButton(
                  color: Colors.white,
                  onPressed: () async{
                    if(controller.value.isPlaying){
                      await controller.pause();
                    }
                    onBackPressed.call();
                  }
                )
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: _OrientationButton(
                    onPressed: (){
                      if(orientation == Orientation.landscape){
                        SystemChrome.setPreferredOrientations([
                          DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
                        ]);
                        return;
                      }
                      SystemChrome.setPreferredOrientations([
                        DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight
                      ]);
                    }
                  )
                )
              )
            ]
          ),
          builder: (ctx, child){
            return FadeTransition(
              opacity: Tween(begin: 1.0, end: 0.0).animate(animationController),
              child: IgnorePointer(
                ignoring: animationController.value == 1.0 ? true : false,
                child: child
              )
            );
          }
        ),
        Expanded(
          child: Center(
            child: AspectRatio(
              aspectRatio: 1.0,
              child: Hero(
                tag: 'video',
                child: GestureDetector(
                  child: VideoPlayer(controller),
                  onTap: (){
                    if(animationController.isCompleted){
                      animationController.reverse();
                    }else{
                      animationController.animateTo(1.0, curve: Curves.ease);
                    }
                  }
                )
              )
            )
          )
        ),
        AnimatedBuilder(
          animation: animationController,
          child: Column(
            children: [
              Flexible(
                flex: 0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: StreamBuilder<DurationState>(
                    stream: progressBarController.stream,
                    builder: (ctx, snapshot){
                      if(snapshot.hasData){
                        final durationState = snapshot.data;
                        return CustomProgressBar(
                          state: durationState!,
                          onSeek: (duration) async{
                            try{
                              await controller.seekTo(duration);
                            }catch(e){
                              debugPrint('$e');
                            }
                          }
                        );
                      }else{
                        return EmptyWidget.instance;
                      }
                    }
                  )
                )
              ),
              Flexible(
                flex: 0,
                child: ControlPlayerWidget(
                  isPlaying: controller.value.isPlaying,
                  onPressed: onPlayPressed,
                  iconColor: Colors.white,
                  onSkipPreviousPressed: (){},
                  onSkipForwardPressed: (){},
                  onFastPreviousPressed: (){},
                  onFastForwardPressed: (){}
                )
              )
            ]
          ),
          builder: (ctx, child){
            return FadeTransition(
              opacity: Tween(begin: 1.0, end: 0.0).animate(animationController),
              child: IgnorePointer(
                ignoring: animationController.value == 1.0 ? true : false,
                child: child
              )
            );
          }
        )
      ]
    );
  }
}

// class VideoPlayerLandscapeView extends StatefulWidget {
//   final VideoPlayerController controller;
//   final VoidCallback onBackPressed;
//   final VoidCallback onPlayPressed;
//   final StreamController<DurationState> progressBarController;
//   final AnimationController animationController;
//   const VideoPlayerLandscapeView({Key? key, required this.controller, required this.onPlayPressed,
//     required this.onBackPressed, required this.progressBarController, required this.animationController}) : super(key: key);
//
//   @override
//   _VideoPlayerLandscapeViewState createState() => _VideoPlayerLandscapeViewState();
// }
//
// class _VideoPlayerLandscapeViewState extends State<VideoPlayerLandscapeView> with SingleTickerProviderStateMixin {
//
//   @override
//   Widget build(BuildContext context) {
//     final orientation = MediaQuery.of(context).orientation;
//     return Stack(
//       children: [
//         GestureDetector(
//           child: VideoPlayer(widget.controller),
//           onTap: (){
//             if(widget.animationController.isCompleted){
//               widget.animationController.reverse();
//             }else{
//               widget.animationController.animateTo(1.0, curve: Curves.ease);
//             }
//           }
//         ),
//         Positioned(
//           top: 16.0, left: 16.0,
//           child: AnimatedBuilder(
//             animation: widget.animationController,
//             child: BackButton(
//               color: Colors.white,
//               onPressed: () async{
//                 if(widget.controller.value.isPlaying){
//                   await widget.controller.pause();
//                 }
//                 widget.onBackPressed.call();
//               }
//             ),
//             builder: (ctx, child){
//               return FadeTransition(
//                 opacity: Tween(begin: 1.0, end: 0.0).animate(widget.animationController),
//                 child: IgnorePointer(
//                   ignoring: widget.animationController.value == 1.0 ? true : false,
//                   child: child
//                 )
//               );
//             }
//           )
//         ),
//         Positioned(
//           bottom: 16.0, left: 56.0, right: 56.0,
//           child: AnimatedBuilder(
//             animation: widget.animationController,
//             child: Column(
//               children: [
//                 Flexible(
//                   flex: 0,
//                   child: Align(
//                     alignment: Alignment.centerRight,
//                     child: _OrientationButton(
//                       onPressed: (){
//                         if(orientation == Orientation.landscape){
//                           setState(() {
//                             SystemChrome.setPreferredOrientations([
//                               DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
//                             ]);
//                           });
//                           return;
//                         }
//                         setState(() {
//                           SystemChrome.setPreferredOrientations([
//                             DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight
//                           ]);
//                         });
//                       }
//                     )
//                   )
//                 ),
//                 const SizedBox(height: 8.0),
//                 Flexible(
//                   flex: 0,
//                   child: StreamBuilder<DurationState>(
//                     stream: widget.progressBarController.stream,
//                     builder: (ctx, snapshot){
//                       if(snapshot.hasData){
//                         final durationState = snapshot.data;
//                         return CustomProgressBar(
//                           state: durationState!,
//                           onSeek: (duration) async{
//                             if(mounted){
//                               try{
//                                 await widget.controller.seekTo(duration);
//                               }catch(e){
//                                 debugPrint('$e');
//                               }
//                             }
//                           }
//                         );
//                       }else{
//                         return EmptyWidget.instance;
//                       }
//                     }
//                   )
//                 ),
//                 Flexible(
//                   flex: 0,
//                   child: ControlPlayerWidget(
//                     isPlaying: widget.controller.value.isPlaying,
//                     onPressed: widget.onPlayPressed
//                   )
//                 )
//               ]
//             ),
//             builder: (ctx, child){
//               return FadeTransition(
//                 opacity: Tween(begin: 1.0, end: 0.0).animate(widget.animationController),
//                 child: IgnorePointer(
//                   ignoring: widget.animationController.value == 1.0 ? true : false,
//                   child: child
//                 )
//               );
//             }
//           )
//         )
//       ]
//     );
//   }
// }

// class VideoPlayerPortraitView extends StatefulWidget {
//   final VideoPlayerController controller;
//   final VoidCallback onBackPressed;
//   final VoidCallback onPlayPressed;
//   final StreamController<DurationState> progressBarController;
//   final AnimationController animationController;
//   const VideoPlayerPortraitView({Key? key, required this.controller, required this.onPlayPressed,
//     required this.onBackPressed, required this.progressBarController, required this.animationController}) : super(key: key);
//
//   @override
//   _VideoPlayerPortraitViewState createState() => _VideoPlayerPortraitViewState();
// }
//
// class _VideoPlayerPortraitViewState extends State<VideoPlayerPortraitView> {
//   @override
//   Widget build(BuildContext context) {
//     final orientation = MediaQuery.of(context).orientation;
//     return Column(
//         children: [
//           AnimatedBuilder(
//               animation: widget.animationController,
//               child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Flexible(
//                         child: BackButton(
//                             color: Colors.white,
//                             onPressed: () async{
//                               if(widget.controller.value.isPlaying){
//                                 await widget.controller.pause();
//                               }
//                               widget.onBackPressed.call();
//                             }
//                         )
//                     ),
//                     Flexible(
//                         child: Padding(
//                             padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                             child: _OrientationButton(
//                                 onPressed: (){
//                                   if(orientation == Orientation.landscape){
//                                     setState(() {
//                                       SystemChrome.setPreferredOrientations([
//                                         DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
//                                       ]);
//                                     });
//                                     return;
//                                   }
//                                   setState(() {
//                                     SystemChrome.setPreferredOrientations([
//                                       DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight
//                                     ]);
//                                   });
//                                 }
//                             )
//                         )
//                     )
//                   ]
//               ),
//               builder: (ctx, child){
//                 return FadeTransition(
//                     opacity: Tween(begin: 1.0, end: 0.0).animate(widget.animationController),
//                     child: IgnorePointer(
//                         ignoring: widget.animationController.value == 1.0 ? true : false,
//                         child: child
//                     )
//                 );
//               }
//           ),
//           Expanded(
//               child: Center(
//                   child: AspectRatio(
//                       aspectRatio: 1.0,
//                       child: Hero(
//                           tag: 'video',
//                           child: GestureDetector(
//                               child: VideoPlayer(widget.controller),
//                               onTap: (){
//                                 if(widget.animationController.isCompleted){
//                                   widget.animationController.reverse();
//                                 }else{
//                                   widget.animationController.animateTo(1.0, curve: Curves.ease);
//                                 }
//                               }
//                           )
//                       )
//                   )
//               )
//           ),
//           AnimatedBuilder(
//               animation: widget.animationController,
//               child: Column(
//                   children: [
//                     Flexible(
//                         flex: 0,
//                         child: Padding(
//                             padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                             child: StreamBuilder<DurationState>(
//                                 stream: widget.progressBarController.stream,
//                                 builder: (ctx, snapshot){
//                                   if(snapshot.hasData){
//                                     final durationState = snapshot.data;
//                                     return CustomProgressBar(
//                                         state: durationState!,
//                                         onSeek: (duration) async{
//                                           if(mounted){
//                                             try{
//                                               await widget.controller.seekTo(duration);
//                                             }catch(e){
//                                               debugPrint('$e');
//                                             }
//                                           }
//                                         }
//                                     );
//                                   }else{
//                                     return EmptyWidget.instance;
//                                   }
//                                 }
//                             )
//                         )
//                     ),
//                     Flexible(
//                         flex: 0,
//                         child: ControlPlayerWidget(
//                           isPlaying: widget.controller.value.isPlaying,
//                           onPressed: widget.onPlayPressed,
//                           iconColor: Colors.white
//                         )
//                     )
//                   ]
//               ),
//               builder: (ctx, child){
//                 return FadeTransition(
//                     opacity: Tween(begin: 1.0, end: 0.0).animate(widget.animationController),
//                     child: IgnorePointer(
//                         ignoring: widget.animationController.value == 1.0 ? true : false,
//                         child: child
//                     )
//                 );
//               }
//           )
//         ]
//     );
//   }
// }


class _OrientationButton extends StatelessWidget{
  final VoidCallback onPressed;
  const _OrientationButton({required this.onPressed});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      child: SvgPicture.asset('assets/svg/orientation.svg', width: 16.0, height: 16.0),
      onTap: onPressed
    );
  }
}


