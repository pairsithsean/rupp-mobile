import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/onboarding_model.dart';
import 'package:rupp_client_app/style/text_color.dart';

class OnBoardingView extends StatelessWidget {
  final OnBoardingModel data;
  const OnBoardingView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity, height: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Image.asset(data.image)
          ),
          Flexible(
            child: Text(data.title, style: const TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20.0
            ))
          ),
          const SizedBox(height: 16.0),
          Flexible(
            child: Text(data.subTitle, style: const TextStyle(
              fontWeight: FontWeight.w600, color: TextColor.secondaryColor
            ))
          )
        ]
      )
    );
  }
}
