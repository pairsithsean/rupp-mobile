import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/online_learning_model.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';

import '../../widgets/bookmark_widget.dart';
import '../bookmark_model.dart';

class PdfView extends StatefulWidget {
  final OnlineLearningData data;
  const PdfView({Key? key, required this.data}) : super(key: key);

  @override
  _PdfViewState createState() => _PdfViewState();
}

class _PdfViewState extends State<PdfView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ContainerRounded(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const Align(
              alignment: Alignment.centerRight,
              child: BookmarkWidget(context: BookmarkContext(state: BookmarkUnSaveState()))
            ),
            Text(widget.data.title, style: const TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.bold
            ))
          ]
        )
      )
    );
  }
}
