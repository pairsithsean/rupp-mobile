import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/payment_history_model.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';
import 'package:rupp_client_app/widgets/custom_fill_button.dart';

class PaymentHistoryView extends StatelessWidget {
  final PaymentHistoryModel data;
  const PaymentHistoryView({Key? key, required this.data}) : super(key: key);

  final FactoryPaymentHistoryCreator _creator = const FactoryPaymentHistoryCreator();

  @override
  Widget build(BuildContext context) {
    return _creator.factoryMethod(data.isPaid, data).build(context);
  }
}

abstract class FactoryPaymentHistory{
  final PaymentHistoryModel data;
  const FactoryPaymentHistory({required this.data});
  Widget build(final BuildContext context);
}

class FactoryDuePayment extends FactoryPaymentHistory{
  FactoryDuePayment({required final PaymentHistoryModel data}) : super(data: data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ContainerRounded(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
              left: BorderSide(color: CustomMaterialColor.primary, width: 5)
            )
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(data.date, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                    const SizedBox(height: 8.0),
                    Text('Due: ${data.date1}', style: const TextStyle(fontSize: 12.0, color: CustomMaterialColor.primary)),
                    const SizedBox(height: 8.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(data.majorName, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                        ),
                        Flexible(
                          child: Text(data.semesterTitle, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
                        )
                      ]
                    )
                  ]
                ),
                const SizedBox(height: 8.0),
                const Divider(),
                const SizedBox(height: 8.0),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('Total Payment', style: TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                          const SizedBox(height: 4.0),
                          Text('\u{0024}${data.pay}', style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
                        ]
                      )
                    ),
                    SizedBox(
                      width: 100, height: 40.0,
                      child: CustomFillButton(
                        label: 'Pay now',
                        onPressed: (){
                          // Navigator.of(context).push(
                          //   PageRouteBuilder(
                          //     pageBuilder: (ctx, anim1, anim2){
                          //       return const PaymentScreen(title: 'Payment');
                          //     }
                          //   )
                          // );
                          Navigator.of(context).pushNamed('payment');
                        }
                      )
                    )
                  ]
                )
              ]
            )
          )
        )
      )
    );
  }
}

class FactoryPaidPayment extends FactoryPaymentHistory{
  FactoryPaidPayment({required final PaymentHistoryModel data}) : super(data: data);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ContainerRounded(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
              left: BorderSide(color: Color.fromRGBO(37, 206, 127, 1.0), width: 5)
            )
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(data.date, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                    const SizedBox(height: 8.0),
                    Text('Paid: ${data.date1}', style: const TextStyle(fontSize: 12.0, color: Color.fromRGBO(37, 206, 127, 1.0))),
                    const SizedBox(height: 8.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(data.majorName, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                        ),
                        Flexible(
                          child: Text(data.semesterTitle, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
                        )
                      ]
                    )
                  ]
                ),
                const SizedBox(height: 8.0),
                const Divider(),
                const SizedBox(height: 8.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Total Payment', style: TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
                    const SizedBox(height: 4.0),
                    Text('\u{0024}${data.pay}', style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
                  ]
                )
              ]
            )
          )
        )
      )
    );
  }
}

class FactoryPaymentHistoryCreator{

  const FactoryPaymentHistoryCreator();

  FactoryPaymentHistory factoryMethod(final bool isPaid, final PaymentHistoryModel data){
    if(isPaid){
      return FactoryPaidPayment(data: data);
    }else{
      return FactoryDuePayment(data: data);
    }
  }
}

// class _PaymentHistoryWidget extends StatelessWidget{
//   final PaymentHistoryModel data;
//   final Color? borderColor;
//   const _PaymentHistoryWidget({Key? key, this.borderColor = Colors.transparent, required this.data}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Container(
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(10),
//         boxShadow: [
//           BoxShadow(color: Colors.black.withAlpha(12), blurRadius: 24.0)
//         ]
//       ),
//       child: ClipRRect(
//           borderRadius: BorderRadius.circular(10),
//           child: Container(
//               decoration: BoxDecoration(
//                   color: Colors.white,
//                   border: Border(
//                       left: BorderSide(color: borderColor!, width: 5)
//                   )
//               ),
//               child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
//                   child: Column(
//                       children: [
//                         Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Text(data.date, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
//                               const SizedBox(height: 8.0),
//                               Text('Due: ${data.date1}', style: TextStyle(fontSize: 12.0, color: borderColor!)),
//                               const SizedBox(height: 8.0),
//                               Row(
//                                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                   children: [
//                                     Expanded(
//                                       child: Text(data.subjectName, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
//                                     ),
//                                     Flexible(
//                                         child: Text(data.semesterTitle, style: const TextStyle(fontSize: 12.0, color: TextColor.secondaryColor))
//                                     )
//                                   ]
//                               )
//                             ]
//                         ),
//                         const SizedBox(height: 8.0),
//                         const Divider(),
//                         const SizedBox(height: 8.0),
//                         Row(
//                             children: [
//                               Expanded(
//                                   child: Column(
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: [
//                                         const Text('Total Payment', style: TextStyle(fontSize: 12.0, color: TextColor.secondaryColor)),
//                                         const SizedBox(height: 4.0),
//                                         Text('\u{0024}${data.pay}', style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0))
//                                       ]
//                                   )
//                               ),
//                               SizedBox(
//                                   width: 100, height: 40.0,
//                                   child: CustomFillButton(
//                                       label: 'Pay now',
//                                       onPressed: (){}
//                                   )
//                               )
//                             ]
//                         )
//                       ]
//                   )
//               )
//           )
//       )
//     );
//   }
// }
