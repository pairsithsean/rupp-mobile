import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:io';
import '../../style/matrial_color.dart';

class ScanView extends StatefulWidget {
  final ValueChanged<Barcode> onDetect;
  const ScanView({Key? key, required this.onDetect}) : super(key: key);

  @override
  _ScanViewState createState() => _ScanViewState();
}

class _ScanViewState extends State<ScanView> {

  static const double _cameraWidth = 350.0;
  static const double _cameraHeight = 350.0;

  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? _qrViewController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _requestCameraPermission();
  }

  void _requestCameraPermission() async{
    try{
      if(!await Permission.camera.isGranted){
        final status = await Permission.camera.request();
        if(status == PermissionStatus.granted){

        }else if(status == PermissionStatus.permanentlyDenied){
          await openAppSettings();
        }else{

        }
      }else{

      }
    }catch(e){
      debugPrint('$e');
    }
  }

  void _onQRViewCreated(QRViewController controller) {
    _qrViewController = controller;
    controller.scannedDataStream.listen((scanData) {
      widget.onDetect(scanData);
    });
  }

  @override
  void reassemble() {
    // TODO: implement reassemble
    super.reassemble();
    if (Platform.isAndroid) {
      _qrViewController?.pauseCamera();
    } else if (Platform.isIOS) {
      _qrViewController?.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _cameraWidth,
      height: _cameraHeight,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned.fill(
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated
            )
          ),
          Positioned(
            top: -5, left: -5,
            child: Container(
              width: 50.0,
              height: 50.0,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(color: CustomMaterialColor.primary, width: 2.0),
                  left: BorderSide(color: CustomMaterialColor.primary, width: 2.0)
                )
              )
            )
          ),
          Positioned(
            top: -5, right: -5,
            child: Container(
              width: 50.0,
              height: 50.0,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(color: CustomMaterialColor.primary, width: 2.0),
                  right: BorderSide(color: CustomMaterialColor.primary, width: 2.0)
                )
              )
            )
          ),
          Positioned(
            bottom: -5, right: -5,
            child: Container(
              width: 50.0,
              height: 50.0,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: CustomMaterialColor.primary, width: 2.0),
                  right: BorderSide(color: CustomMaterialColor.primary, width: 2.0)
                )
              )
            )
          ),
          Positioned(
            bottom: -5, left: -5,
            child: Container(
              width: 50.0,
              height: 50.0,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: CustomMaterialColor.primary, width: 2.0),
                  left: BorderSide(color: CustomMaterialColor.primary, width: 2.0)
                )
              )
            )
          )
        ]
      )
    );
  }
}
