import 'package:flutter/material.dart';
import 'package:rupp_client_app/model/sticky_note_model.dart';
import 'package:rupp_client_app/widgets/sticky_note.dart';
//
// class StickyNoteView extends StatelessWidget {
//   final String note;
//   const StickyNoteView({Key? key, required this.note}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }

enum StickyNoteType{
  classes, exam, presentation
}

abstract class FactoryStickyNoteView{
  final StickyNoteModel model;
  const FactoryStickyNoteView({required this.model});
  Widget build(final BuildContext context);
}

class FactoryStickyNoteClassesView extends FactoryStickyNoteView{
  FactoryStickyNoteClassesView({required final StickyNoteModel model}) : super(model: model);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StickyNote(data: model.data, backgroundColor: const Color(0xFF5B8DEF));
  }
}

class FactoryStickyNoteExamView extends FactoryStickyNoteView{
  FactoryStickyNoteExamView({required final StickyNoteModel model}) : super(model: model);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StickyNote(data: model.data, backgroundColor: const Color(0xFFFF7172));
  }
}

class FactoryStickyNotePresentationView extends FactoryStickyNoteView{
  FactoryStickyNotePresentationView({required final StickyNoteModel model}) : super(model: model);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StickyNote(data: model.data, backgroundColor: const Color(0xFFFDDD48), textColor: Colors.black);
  }
}

class StickyNoteViewCreator{
  const StickyNoteViewCreator();

  FactoryStickyNoteView factoryMethod(final StickyNoteType type, final StickyNoteModel model){
    switch(type){
      case StickyNoteType.exam: return FactoryStickyNoteExamView(model: model);
      case StickyNoteType.classes: return FactoryStickyNoteClassesView(model: model);
      default: return FactoryStickyNotePresentationView(model: model);
    }
  }
}
