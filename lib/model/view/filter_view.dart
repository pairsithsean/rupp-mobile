import 'package:flutter/material.dart';
import 'package:rupp_client_app/repository/material_repository.dart';
import 'package:rupp_client_app/style/matrial_color.dart';
import 'package:rupp_client_app/style/text_color.dart';
import 'package:rupp_client_app/widgets/composite_widget.dart';
import 'package:rupp_client_app/widgets/custom_dropdown.dart';

import '../filter_model.dart';
import '../online_learning_model.dart';

final List<FilterData> selectedFilterItem = [];
class FilterView extends StatefulWidget {
  final List<OnlineLearningModel> data;
  const FilterView({Key? key, required this.data}) : super(key: key);

  @override
  _FilterViewState createState() => _FilterViewState();
}

class _FilterViewState extends State<FilterView> {

  final List<FilterData> _filterData = [];
  final List<FilterData> _filterMaterialData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //debugPrint('${selectedFilterItem.length} ${widget.data.length}');
    /// INSERT DATA TO FILTER LIST
    for(OnlineLearningModel model in widget.data){
      for(OnlineLearningData data in model.data){
        _filterData.add(FilterData(data: data, label: data.title));
      }
    }

    for(OnlineLearningMaterial material in MaterialRepository.all()) {
      _filterMaterialData.add(FilterData(data: null, label: material.title));
    }

    for(int i = 0; i < selectedFilterItem.length; i++){
      final index = _filterData.indexWhere((element) => element.label == selectedFilterItem[i].label);
      if(index > -1){
        _filterData[index] = selectedFilterItem.elementAt(i);
      }else{
        final index = _filterMaterialData.indexWhere((element) => element.label == selectedFilterItem[i].label);
        _filterMaterialData[index] = selectedFilterItem.elementAt(i);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Row(
            children: [
              TextButton(
                child: const Text('Cancel'),
                onPressed: (){
                  Navigator.of(context, rootNavigator: true).pop();
                  //selectedFilterItem.clear();
                }
              ),
              const Expanded(
                child: Center(
                  child: Text('Filter')
                )
              ),
              TextButton(
                child: const Text('Reset'),
                onPressed: (){
                  for(int i = 0; i < selectedFilterItem.length; i++){
                    final index = _filterData.indexWhere((element) => element.label == selectedFilterItem[i].label);
                    if(index > -1){
                      _filterData[index].isSelected = false;
                    }else{
                      final index = _filterMaterialData.indexWhere((element) => element.label == selectedFilterItem[i].label);
                      _filterMaterialData[index].isSelected = false;
                    }
                  }
                  setState(() {
                    selectedFilterItem.clear();
                  });
                }
              )
            ]
          ),
          // Padding(
          //   padding: const EdgeInsets.all(16.0),
          //   child: FilterDropDownView(
          //     model: FilterModel(
          //       title: 'Select Course Type',
          //       data: [
          //         FilterData(
          //           data: OnlineLearningData(title: 'aaa', date: '', material: MaterialRepository.all().first)
          //         ),
          //         FilterData(
          //           data: OnlineLearningData(title: 'bbb', date: '', material: MaterialRepository.all().elementAt(2))
          //         )
          //       ]
          //     )
          //   )
          // ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: FilterWrapView(
              model: FilterModel(
                title: 'Select Subject type',
                data: _filterData
              )
            )
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: FilterWrapView(
              model: FilterModel(
                title: 'Materials',
                data: _filterMaterialData
              )
            )
          )
        ]
      )
    );
  }
}

class FilterWrapView extends StatefulWidget {
  final FilterModel model;
  const FilterWrapView({Key? key, required this.model}) : super(key: key);

  @override
  _FilterWrapViewState createState() => _FilterWrapViewState();
}

class _FilterWrapViewState<T> extends State<FilterWrapView> {

  // ValueNotifier<FilterData?>? _selectedItemNotifier;
  //
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _selectedItemNotifier = ValueNotifier<FilterData?>(widget.model.data.first);
  // }
  //
  // @override
  // void dispose() {
  //   // TODO: implement dispose
  //   _selectedItemNotifier?.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ColumnComposite(
        iComponent: [
          Component(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Center(
                child: Text(widget.model.title, style: const TextStyle(fontWeight: FontWeight.w600))
              )
            )
          ),
          WrapComposite(
            iComponent: widget.model.data.map((data){
              return Component(
                child: GestureDetector(
                  onTap: (){
                    if(data.isSelected){
                      setState(() {
                        data.isSelected = false;
                      });
                      final queryItem = selectedFilterItem.where((element) => element.isSelected == true).toList();
                      selectedFilterItem.clear();
                      for(FilterData data in queryItem){
                        selectedFilterItem.add(data);
                      }
                      return;
                    }
                    setState(() {
                      data.isSelected = true;
                    });
                    //
                    if(data.isSelected){
                      selectedFilterItem.add(data);
                    }
                  },
                  // child: ValueListenableBuilder<FilterData?>(
                  //   valueListenable: _selectedItemNotifier!,
                  //   builder: (ctx, value, child){
                  //     return Container(
                  //       padding: const EdgeInsets.all(16.0),
                  //       child: Text(data.name, style: TextStyle(color: data.isSelected ? Colors.white : TextColor.secondaryColor)),
                  //       decoration: BoxDecoration(
                  //         color: data.isSelected ? CustomMaterialColor.primary : Colors.black.withAlpha(25),
                  //         borderRadius: BorderRadius.circular(10.0)
                  //       )
                  //     );
                  //   }
                  // )
                  child: Container(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(data.label, style: TextStyle(color: data.isSelected ? Colors.white : TextColor.secondaryColor)),
                    decoration: BoxDecoration(
                      color: data.isSelected ? CustomMaterialColor.primary : const Color(0xFFEBEBF0),
                      borderRadius: BorderRadius.circular(10.0)
                    )
                  )
                )
              );
            }).toList()
          )
        ]
      ).build(context)
    );
  }
}

class FilterDropDownView extends StatelessWidget{
  final FilterModel model;
  const FilterDropDownView({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SizedBox(
      child: ColumnComposite(
        iComponent: [
          Component(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Center(
                child: Text(model.title, style: const TextStyle(fontWeight: FontWeight.w600))
              )
            )
          ),
          Component(
            child: CustomDropdown(
              position: 45.0,
              items: model.data.map((e){
                return e.data!.title;
              }).toList(),
              onSelected: (index){
              }
            )
          )
        ]
      ).build(context)
    );
  }
}

