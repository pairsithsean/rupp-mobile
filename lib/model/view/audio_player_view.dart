import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rupp_client_app/widgets/custom_container.dart';
import 'package:rupp_client_app/widgets/custom_progressbar.dart';
import 'package:rupp_client_app/widgets/loading_widget.dart';

import '../../widgets/bookmark_widget.dart';
import '../../widgets/custom_control_player.dart';
import '../bookmark_model.dart';
import '../online_learning_model.dart';

class AudioPlayerView extends StatefulWidget {
  final OnlineLearningData data;
  const AudioPlayerView({Key? key, required this.data}) : super(key: key);

  @override
  _AudioPlayerViewState createState() => _AudioPlayerViewState();
}

class _AudioPlayerViewState extends State<AudioPlayerView> {

  AudioPlayer? _audioPlayer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    //loadAudio();
  }

  // void loadAudio() async{
  //   try{
  //     await _audioPlayer?.play('https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3');
  //   }catch(e){
  //     debugPrint('$e');
  //   }
  // }

  @override
  void dispose() {
    // TODO: implement dispose
    _audioPlayer?.stop();
    _audioPlayer?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          ContainerRounded(
            width: 300.0, height: 300.0,
            child: CachedNetworkImage(
              imageUrl: 'https://media.istockphoto.com/photos/vintage-vinyl-record-album-cover-mockup-flat-concept-picture-id1127565686?b=1&k=20&m=1127565686&s=170667a&w=0&h=OBvTbZEFPOwXQLGWAKODUXwX8VaiEbQvPWrNzfl5GUI=',
              fit: BoxFit.cover
            )
          ),
          const SizedBox(height: 16.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Flexible(
                child: Text('Title', style: TextStyle(
                  fontSize: 20.0, fontWeight: FontWeight.bold
                ))
              ),
              Flexible(
                child: BookmarkWidget(context: BookmarkContext(state: BookmarkUnSaveState()))
              )
            ]
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Text('Subtitle')
          ),
          const SizedBox(height: 16.0),
          FutureBuilder<int>(
            future: _audioPlayer?.play('https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', isLocal: false,
                duckAudio: false, stayAwake: true), initialData: null,
            builder: (ctx, snapshot){
              if(snapshot.hasData){
                if(snapshot.data == 1){
                  return StreamBuilder<Duration>(
                    stream: _audioPlayer?.onDurationChanged, initialData: Duration.zero,
                    builder: (ctx, snapshotDuration){
                      return StreamBuilder<Duration>(
                        stream: _audioPlayer?.onAudioPositionChanged, initialData: Duration.zero,
                        builder: (ctx, snapshotPosition){
                          return CustomAudioProgressBar(
                            state: DurationState(
                              progress: snapshotPosition.data!, buffered: snapshotPosition.data!,
                              total: snapshotDuration.data!
                            ),
                            onSeek: (duration) async{
                              if(mounted){
                                await _audioPlayer?.seek(duration);
                                if(snapshotPosition.data! == snapshotDuration.data!) return;
                                await _audioPlayer?.resume();
                              }
                            },
                            onDragEnd: () async{
                              if(mounted){
                                await _audioPlayer?.stop();
                              }
                            }
                          );
                        }
                      );
                    }
                  );
                }else{
                  return LoadingWidget.instance;
                }
              }else{
                return LoadingWidget.instance;
              }
            }
          ),
          StreamBuilder<PlayerState>(
            stream: _audioPlayer?.onPlayerStateChanged, initialData: PlayerState.PAUSED,
            builder: (ctx, snapshotPlayerState){
              return ControlPlayerWidget(
                isPlaying: snapshotPlayerState.data! == PlayerState.PLAYING ? true : false,
                onPressed: () async{
                  if(mounted){
                    switch(snapshotPlayerState.data!){
                      case PlayerState.PLAYING: await _audioPlayer?.pause(); break;
                      case PlayerState.COMPLETED: await _audioPlayer?.release(); await _audioPlayer?.resume(); break;
                      case PlayerState.STOPPED: await _audioPlayer?.release(); await _audioPlayer?.resume(); break;
                      default: await _audioPlayer?.resume();
                    }
                  }
                },
                onFastForwardPressed: () async{

                },
                onFastPreviousPressed: () async{},
                onSkipForwardPressed: () async{},
                onSkipPreviousPressed: () async{}
              );
            }
          )
        ]
      )
    );
  }
}
