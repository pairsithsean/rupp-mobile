class LearningResultModel{
  final String title;
  final double? score;

  const LearningResultModel({required this.title, this.score = 0.0});
}