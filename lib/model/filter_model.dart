import 'package:rupp_client_app/model/online_learning_model.dart';

class FilterModel{
  final String title;
  final List<FilterData> data;
  const FilterModel({required this.title, required this.data});
}

class FilterData {
  OnlineLearningData? data;
  final String label;
  bool isSelected;

  FilterData({required this.data, required this.label, this.isSelected = false});
}