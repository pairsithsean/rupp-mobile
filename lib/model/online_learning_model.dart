import 'bookmark_model.dart';

class OnlineLearningModel{
  final String title;
  final List<OnlineLearningData> data;
  const OnlineLearningModel({required this.title, required this.data});
}

class OnlineLearningData{
  final String title;
  final String date;
  final String? time;
  final OnlineLearningMaterial material;
  BookmarkContext? context;

  OnlineLearningData({required this.material, required this.title, required this.date, this.context =
    const BookmarkContext(state: BookmarkUnSaveState()), this.time = ''});
}

class OnlineLearningMaterial{
  final String title;

  const OnlineLearningMaterial({required this.title});
}