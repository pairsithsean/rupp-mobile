class PaymentHistoryModel{
  final String date;
  final String majorName;
  final String date1;
  final String semesterTitle;
  final double pay;
  final bool isPaid;

  const PaymentHistoryModel({required this.date, required this.date1,
    required this.semesterTitle, required this.majorName, required this.pay, required this.isPaid});
}